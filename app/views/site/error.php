<?php
use yii\bootstrap\Html;
$this->title = 'Произошла ошибка';
?>
<div class="content-sm container">
    <div class="row">
        <div class="col-sm-4 margin-b-30">
            <h3><?=nl2br(Html::encode($message));?></h3>
        </div>
    </div>
</div>