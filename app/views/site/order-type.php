<?php
/** @var $category string */

/** @var $form_model \app\models\forms\CallbackForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Как вам удобнее сделать заказ?';
?>
<div class="bg-color-sky-dark-blue">
    <div class="content-sm container">
        <div class="row row-space-1">
            <div class="col-sm-4 sm-margin-b-2">
                <div class="pricing">
                    <div class="margin-b-30">
                        <i class="pricing-icon fa fa-phone" aria-hidden="true"></i>
                        <h2>По телефону</h2>
                    </div>
                    <ul class="list-unstyled pricing-list margin-b-50">
                        <li class="pricing-list-item"><b>Перезвоним Вам за 8 секунд</b></li>
                    </ul>
                    <?php $form = ActiveForm::begin([
                        'action' => ['/call'],
                        'fieldConfig' => [
                            'options' => ['class' => 'input-parent'],
                            'template' => '{input}{error}',
                            'inputOptions' => [
                                'class' => 'form-control content-input margin-b-20',
                            ]
                        ],
                    ]); ?>
                    <?= $form->field($form_model, 'phone')
                        ->widget(\yii\widgets\MaskedInput::className(), [
                            'mask' => '+7 (999) 999 99 99',
                            'options' => [
                                'autocomplete' => 'off',
                                'class' => 'form-control content-input margin-b-20',
                                'placeholder' => $form_model->getAttributeLabel('phone')
                            ],
                        ]); ?>

                    <?= Html::submitButton(
                        'Жду звонка',
                        [
                            'name' => 'OrderTypeForm[order_type]',
                            'value' => 'call',
                            'class' => 'btn-theme btn-theme-sm btn-base-bg text-uppercase',
                        ]
                    ); ?>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
            <div class="col-sm-4 sm-margin-b-2">
                <div class="pricing pricing-active">
                    <div class="margin-b-30">
                        <i class="pricing-icon fa fa-shopping-basket" aria-hidden="true"></i>
                        <h2>Онлайн заказ</h2>
                    </div>
                    <ul class="list-unstyled pricing-list margin-b-80">
                        <li class="pricing-list-item">Быстро</li>
                        <li class="pricing-list-item">Просто</li>
                        <li class="pricing-list-item">Удобно</li>
                        <li class="pricing-list-item"><b>Со скидкой 10%</b></li>
                    </ul>
                    <?= Html::a(
                        'Перейти к оформлению',
                        ['/catalog/cat', 'slug' => $category],
                        [
                            'class' => 'btn-theme btn-theme-sm btn-base-bg text-uppercase'
                        ]
                    ); ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="pricing">
                    <div class="margin-b-30">
                        <i class="pricing-icon icon-note"></i>
                        <h2>Другие способы</h2>
                    </div>
                    <ul class="list-unstyled pricing-list margin-b-50">
                        <li class="pricing-list-item">
                            <a href="mailto:site@fortex-rostov.ru">
                                <span><i class="mail fa fa-envelope-o" aria-hidden="true"></i></span>
                                Email:<br/>site@fortex-rostov.ru
                            </a>
                        </li>
                        <li class="pricing-list-item">
                            <a href="#">
                                <span><i class="whatsapp fa fa-whatsapp" aria-hidden="true"></i></span>
                                WhatsApp
                            </a>
                        </li>
                        <li class="pricing-list-item">
                            <a href="#">
                                <span><i class="viber icon-call-out"></i></span>
                                Viber
                            </a>
                        </li>
                        <li class="pricing-list-item">
                            <a href="#">
                                <span><i class="telegram fa fa-telegram" aria-hidden="true"></i></span>
                                Telegram
                            </a>
                        </li>
                    </ul>
                    <ul>
                </div>
            </div>
        </div>
    </div>
</div>

<?=$this->render('//layouts/chunks/_popular_slider');?>