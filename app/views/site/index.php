<?php
/* @var $cats \yii\easyii\modules\catalog\api\CategoryObject[] */
/* @var $pechati \yii\easyii\modules\catalog\api\CategoryObject[] */
/* @var $tool_cats \yii\easyii\modules\catalog\api\CategoryObject[] */
/* @var $stampy \yii\easyii\modules\catalog\api\CategoryObject */
/* @var $by_scetch \yii\easyii\modules\catalog\api\CategoryObject */
/* @var $faximile \yii\easyii\modules\catalog\api\CategoryObject */
/* @var $ekslibrisy \yii\easyii\modules\catalog\api\CategoryObject */
/* @var $uslugi \yii\easyii\modules\gallery\api\PhotoObject[] */
/* @var $partners \yii\easyii\modules\gallery\api\PhotoObject[] */
/* @var $callback_model \app\models\forms\CallbackForm */

$asset = \app\assets\AppAsset::register($this);
use yii\helpers\Url;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use app\helpers\CatalogHelper;

?>
<?=$this->render('chunks/_offer', ['callback_model' => $callback_model]);?>


<div class="container container-first">
    <a class="anchor" id="catalog"></a>
    <h2 class="text-center">Каталог продукции</h2>
    <div class="row">
    <?php foreach($cats as $cat): ?>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <a class="item" href="<?=Url::to(['/catalog/cat', 'slug' => $cat->slug]);?>">
                <div class="item-img-wrapper">
                    <?=Html::img($cat->thumb(null, 400), ['alt' => $cat->title]);?>
                </div>
                <div class="item-title"><?=$cat->title;?></div>
            </a>
        </div>
    <?php endforeach; ?>
    </div>
</div>


<div class="container">
    <a class="anchor" id="prints"></a>
    <h2 class="text-center">Варианты печатей и штампов</h2>
    <div class="row auto-clear margin-b-20">
        <?php foreach($pechati as $pechat): ?>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
                <a class="item" href="<?=Url::toRoute(['catalog/subcat', 'cat' => $pechat->model->parent,  'slug' => $pechat->slug]);?>">
                    <div class="item-img-wrapper">
                        <?=Html::img($pechat->thumb(null, 400), ['alt' => $pechat->title]);?>
                    </div>
                    <div class="item-title"><?=$pechat->title;?></div>
                </a>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="row auto-clear">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
            <a class="item" href="<?=Url::toRoute(['catalog/cat', 'slug' => $by_scetch->slug]);?>">
                <div class="item-img-wrapper">
                    <?=Html::img($by_scetch->thumb(null, 400), ['alt' => $by_scetch->title]);?>
                </div>
                <div class="item-title"><?=$by_scetch->title;?></div>
            </a>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
            <a class="item" href="<?=Url::toRoute(['catalog/stampy']);?>">
                <div class="item-img-wrapper">
                    <?=Html::img($stampy->thumb(null, 400), ['alt' => $stampy->title]);?>
                </div>
                <div class="item-title"><?=$stampy->title;?></div>
            </a>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
            <a class="item" href="<?=Url::toRoute(['catalog/cat', 'slug' => $faximile->slug]);?>">
                <div class="item-img-wrapper">
                    <?=Html::img($faximile->thumb(null, 400), ['alt' => $faximile->title]);?>
                </div>
                <div class="item-title"><?=$faximile->title;?></div>
            </a>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
            <a class="item" href="<?=Url::toRoute(['catalog/ekslibrisy']);?>">
                <div class="item-img-wrapper">
                    <?=Html::img($ekslibrisy->thumb(null, 400), ['alt' => $ekslibrisy->title]);?>
                </div>
                <div class="item-title"><?=$ekslibrisy->title;?></div>
            </a>
        </div>
    </div>
    
</div>


<div class="container">
    <a class="anchor" id="equipment"></a>
    <h2 class="text-center">Варианты оснасток</h2>
    <div class="row">
        <?php $i = 0; ?>
        <?php foreach($tool_cats as $tool_cat): ?>
            <?php
                $variant = $i + 1;
                $a = Yii::$app->request->get('a');
            ?>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <a class="item" href="<?=Url::toRoute(['catalog/tools-examples', 'slug' => $tool_cat->slug, 'a' => 'pecati', 'b' => 'choose']);?>">
                    <div class="item-img-wrapper">
                        <?=Html::img($tool_cat->thumb(null, 400), ['alt' => $tool_cat->title]);?>
                    </div>
                    <div class="item-title"><?=$tool_cat->title;?></div>
                    <div class="item-flying-info">
                        <div class="item-flying-info-inner">
                            <span class="item-flying-info-bottom">
                                от <?=CatalogHelper::getMinPrice($tool_cat);?> руб.
                            </span>
                        </div>
                    </div>
                </a>
            </div>
            
            <?php $i++;?>
        <?php endforeach; ?>
    </div>
</div>

<div class="container text-center">
    <h2>
        О гарантии миллион оттисков или 10 лет
        <br class="hidden-xs">и почему это важно?
    </h2>
    <p style="font-size:19px;">
        Важно потому, что в результате длительного использования клише печати
        практически не подвергаются износу, потому что мы изготавливаем его
        из высококачественной резины производства США,
        специально предназначенной для такого большого количества оттисков.
    </p>
</div>


<a class="anchor" id="reviews"></a>
<div class="container">
    <h2 class="text-center">Отзывы наших клиентов</h2>
    <div class="swiper-container swiper" data-slides-per-view="4">
        <div class="swiper-wrapper">
            <?php foreach($comments as $comment): ?>
                <?=$this->render('//site/chunks/_carousel_comment', ['comment' => $comment]);?>
            <?php endforeach; ?>
        </div>
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
    </div>
</div>


<div class="container">
    <a class="anchor" id="partners"></a>
    <h2 class="text-center">Партнеры</h2>
    <div class="swiper-container" data-slides-per-view="4">
        <div class="swiper-wrapper">
            <?php foreach($partners as $partner): ?>
                <?=$this->render('//site/chunks/_gallery_partner', ['partner' => $partner]);?>
            <?php endforeach; ?>
        </div>
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
    </div>
</div>

<div class="container container-last">
    <a class="anchor" id="service"></a>
    <h2 class="text-center">Другие услуги</h2>
    <div class="swiper-container swiper-other-services" data-slides-per-view="3">
        <div class="swiper-wrapper">
            <?php foreach($uslugi as $slide): ?>
                <?=$this->render('//site/chunks/_service_slide', ['slide' => $slide]);?>
            <?php endforeach; ?>
        </div>
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
    </div>
</div>


<?php Modal::begin([
    'id' => 'reviews-image-modal',
    'header' => '<h4 class="margin-b-0 text-center" id="reviews-image-title"></h4>',
    'size' => Modal::SIZE_DEFAULT,
]);?>

<div class="row">
    <div class="col-xs-12">
        <img id="reviews-image" class="img-thumbnail center-block">
    </div>
</div>

<?php Modal::end();?>

<script>
    function reviewsImageModal(image_src, title){
        document.getElementById('reviews-image').src = image_src;
        document.getElementById('reviews-image-title').innerHTML = title;
    };
</script>
<?php
$this->registerJs(<<<JS
    jQuery('#pecati-phone, #pecati-i-stampy-phone, #metal-phone').inputmask('+7 (999) 999-99-99');
JS
);
?>