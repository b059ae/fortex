<?php
use yii\bootstrap\Html;
?>
<div class="container text-center">
    <a class="anchor" id="catalog"></a>
    <h2><?=$title;?></h2>
    <h3 class="margin-b-40"><?=$message;?></h3>
    <p><?=Html::a('Перейти на Главную', ['/'], ['class' => 'btn btn-info btn-lg']);?></p>
</div>