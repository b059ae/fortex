<?php
$asset = \app\assets\AppAsset::register($this);
use yii\bootstrap\Html;
?>
<div class="<?=!empty($classes) ? $classes : '';?>">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 offer-left">
                <h1 class="text-primary">Изготовление печатей<br>врача</h1>
                <p class="text-center">
                    <span class="text-info offer-label-1">от 1 часа</span>
                    <?=Html::a(
                        'Купить <span class="smaller">за</span> 390 руб.',
                        ['/catalog/tools', 'a' => 'pecati/doctor', 'b' => 'choose'],
                        ['class' => 'btn btn-2 btn-info btn-lg offer-button']
                    );?>
                </p>
                <p class="offer-text">
                    Доставим заказ по Ростову-на-Дону
                    <br>в течение 24-х часов
                </p>
                <h2>
                    Гарантия <span class="text-danger">1 миллион</span> оттисков
                    <br>или <span class="text-danger">10 лет</span>
                </h2>
                <p class="visible-xs">
                    <?=Html::img($asset->baseUrl . '/img/offers/doctor.png', [
                        'class' => 'img-responsive center-block'
                    ]);?>
                </p>

                <p class="text-center">
                    <?=Html::a(
                        'Купить <span class="smaller">за</span> 390 руб.',
                        ['/catalog/tools', 'a' => 'pecati/doctor', 'b' => 'choose'],
                        ['class' => 'btn btn-2 btn-info btn-lg offer-button']
                    );?>
                </p>


            </div>
            <div class="col-xs-12 col-sm-6 hidden-xs">
                <?=Html::img($asset->baseUrl . '/img/offers/doctor.png', [
                    'class' => 'img-responsive center-block'
                ]);?>
            </div>
        </div>
    </div>
</div>