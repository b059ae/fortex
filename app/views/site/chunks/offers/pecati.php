<?php
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\MaskedInput;
$asset = \app\assets\AppAsset::register($this);
?>
<div class="<?=!empty($classes) ? $classes : '';?> offer-2">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 offer-left">
                <h1 class="text-primary">Срочное изготовление печатей</h1>
                <p class="text-center">
                    <span class="text-info offer-label-1">от 1 часа</span>
                    <?=Html::a(
                        'цена 390 руб.',
                        ['/catalog/tools', 'a' => 'pecati/ip', 'b' => 'choose'],
                        ['class' => 'btn btn-2 btn-info btn-lg offer-button']
                    );?>
                </p>
                <p class="offer-text">
                    Доставим заказ по Ростову-на-Дону
                    <br>в течение 24-х часов
                </p>
                <h2>
                    Гарантия <span class="text-danger">1 миллион</span> оттисков
                    <br>или <span class="text-danger">10 лет</span>
                </h2>
                <p class="visible-xs">
                    <?=Html::img($asset->baseUrl . '/img/offers/pecati.png', [
                        'class' => 'img-responsive center-block'
                    ]);?>
                </p>
                <p class="offer-text">
                    Закажите печать прямо сейчас и менеджер перезвонит Вам через <span class="text-info">8 секунд</span> и уточнит заказ.
                </p>
                
                <?php $form = ActiveForm::begin([
                    'id' => 'pecati-form',
                    'action' => '/call',
                    'options' => ['class' => 'row text-center-xs']
                ]); ?>
                
                <div class="row">
                    <div class="col-xs-12 col-sm-offset-1 col-sm-6 col-lg-6 col-lg-offset-1 text-center">
                        <?=$form->field($callback_model, 'phone')
                            ->textInput()
                            ->label(false)
                            ->widget(MaskedInput::className(), [
                                'mask' => '+7 (999) 999-99-99',
                                'options' => [
                                    'id' => 'pecati-phone',
                                    'class' => 'form-control form-control-2 inline',
                                    'placeholder' => '+7 (***) ***-**-**',
                                ],
                            ]);?>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-lg-4 margin-b-20 text-center">
                        <?=Html::submitButton(
                            'Жду звонка',
                            ['class' => 'btn btn-2 btn-lg btn-danger']
                        );?>
                    </div>
                </div>
                
                <?php ActiveForm::end();?>

            </div>
            <div class="col-xs-12 col-sm-6 hidden-xs">
                <?=Html::img($asset->baseUrl . '/img/offers/pecati.png', [
                    'class' => 'img-responsive center-block'
                ]);?>
            </div>
        </div>
    </div>
</div>