<?php
use yii\bootstrap\Html;

$field_id = 'count-' . $good->id;

$image = $good->usesTool() ? $good->getToolThumb(null, 100) : $good->item()->thumb(null, 100);

?>
<div class="row good" id="good-<?=$good->id;?>">
    <div class="col-xs-6 col-sm-6 col-md-2">
        <img src="<?=$image;?>" alt="<?=$good->item->title;?>" class="img-thumbnail" style="margin-top:20px;">
    </div>
    <div class="col-xs-6 col-sm-6 col-md-3">
        <?php if($good->usesConfig()): ?>
            <?=Html::a(
                str_replace("\n", '<br>', $good->getTitle(true)),
                $good->getMasterUrl()
            );?>
        <?php else: ?>
            <?=$good->title;?>
        <?php endif; ?>
    </div>
    
    <div class="clearfix visible-sm visible-xs"></div>

    <div class="col-xs-6 col-sm-6 col-md-2">
        <strong class="shopcart-label visible-sm-inline visible-xs-inline">За единицу:<br></strong>
        <strong><?=$good->getPriceForOne(true);?> руб.</strong>
    </div>

    <div class="col-xs-6 col-sm-6 col-md-2">
        <div class="product_count">
            <div class="string_left" onclick="productCount('<?=$field_id;?>', '-'); countPrices();">
                <i class="fa fa-minus" aria-hidden="true"></i>
            </div>
            <?=Html::textInput('count['.$good->id.']', $good->count, [
                'id' => $field_id,
                'class' => 'product_count_input',
                'data-price' => $good->getPriceForOne(true),
                'data-time-to-work-price-0' => $good->getTimeToWorkPrice(0),
                'data-time-to-work-price-1' => $good->getTimeToWorkPrice(1),
                'data-time-to-work-price-2' => $good->getTimeToWorkPrice(2),
                'data-id' => $good->id,
            ]);?>
            <div class="string_right" onclick="productCount('<?=$field_id;?>', '+'); countPrices();">
                <i class="fa fa-plus" aria-hidden="true"></i>
            </div>
        </div>
    </div>
    <div class="clearfix visible-sm visible-xs"></div>

    <div class="col-xs-6 col-sm-6 col-md-2">
        <strong class="shopcart-label visible-sm-inline visible-xs-inline">Всего:<br></strong>
        <strong class="price_pos" id="good-total-price-<?=$good->id;?>">
            <?=$good->getCost(true);?> руб.
        </strong>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-1">
        <?=Html::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', [
            'title' => 'Удалить',
            'class' => 'btn-remove',
            'onclick' => 'removeGood('.$good->id.')',
        ]);?>
    </div>
</div>