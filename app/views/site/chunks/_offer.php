<?php
/* @var $callback_model \app\models\forms\CallbackForm */

use yii\easyii\models\Setting;

$phone = Setting::get('phone');
$path = Yii::$app->request->pathInfo;
$asset = \app\assets\AppAsset::register($this);
?>
<div class="offer">

<?php if($path === ''): ?>

<div id="offer-slider" class="swiper-container">
    <div class="swiper-wrapper">
        <?=$this->render('offers/pecati', [
            'classes' => 'swiper-slide',
            'callback_model' => $callback_model,
        ]);?>
        <?=$this->render('offers/ip', ['classes' => 'swiper-slide']);?>
        <?=$this->render('offers/ooo', ['classes' => 'swiper-slide']);?>
        <?=$this->render('offers/doctor', ['classes' => 'swiper-slide']);?>
        <?=$this->render('offers/pecat-s-osnastkoy', ['classes' => 'swiper-slide']);?>
        <?=$this->render('offers/karmannaya', ['classes' => 'swiper-slide']);?>
        <?=$this->render('offers/automaticheskaya', ['classes' => 'swiper-slide']);?>
        <?=$this->render('offers/ruchnaya', ['classes' => 'swiper-slide']);?>
        <?=$this->render('offers/metallicheskaya', [
            'classes' => 'swiper-slide',
            'callback_model' => $callback_model,
        ]);?>
        <?=$this->render('offers/pecati-i-stampy', [
            'classes' => 'swiper-slide',
            'callback_model' => $callback_model,
        ]);?>
    </div>
    <div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div>
</div>

<?php elseif($path === 'pecati'): ?>

    <?=$this->render('offers/pecati', ['callback_model' => $callback_model]);?>

<?php elseif($path === 'ip'): ?>

    <?=$this->render('offers/ip');?>

<?php elseif($path === 'ooo'): ?>

    <?=$this->render('offers/ooo');?>

<?php elseif($path === 'doctor'): ?>

    <?=$this->render('offers/doctor');?>

<?php elseif($path === 'pecat-s-osnastkoy'): ?>

    <?=$this->render('offers/pecat-s-osnastkoy');?>

<?php elseif($path === 'karmannaya'): ?>

    <?=$this->render('offers/karmannaya');?>

<?php elseif($path === 'automaticheskaya'): ?>

    <?=$this->render('offers/automaticheskaya');?>

<?php elseif($path === 'ruchnaya'): ?>

    <?=$this->render('offers/ruchnaya');?>

<?php elseif($path === 'metallicheskaya'): ?>

    <?=$this->render('offers/metallicheskaya', ['callback_model' => $callback_model]);?>

<?php elseif($path === 'pecati-i-stampy'): ?>

    <?=$this->render('offers/pecati-i-stampy', ['callback_model' => $callback_model]);?>

<?php endif; ?>

</div>
