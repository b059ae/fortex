<?php
$index = isset($index) ? $index : null;
$col_lg = !empty($col_lg) ? $col_lg : 3;
$col_md = !empty($col_md) ? $col_md : 4;
$col_sm = !empty($col_sm) ? $col_sm : 6;
$col_xs = !empty($col_xs) ? $col_xs : 12;

?>
<div class="col-sm-6 col-md-4 col-lg-3 margin-b-30 wow zoomIn text-center" data-wow-duration=".3" data-wow-delay=".1s">
    <img class="margin-b-20 img-responsive center-block" src="<?=$partner->thumb(null, 77);?>" alt="<?=$partner->description;?>">
    <p><?=$partner->description;?></p>
</div>
<?=\app\helpers\CatalogHelper::getClearfix($index, [
    'col_lg' => $col_lg,
    'col_md' => $col_md,
    'col_sm' => $col_sm,
    'col_xs' => $col_xs,
]);?>