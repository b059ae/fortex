<?php
use yii\bootstrap\Html;
?>
<div class="swiper-slide">
    <div class="review">
        <a href="/#reviews-image-modal"
           data-toggle="modal"
           onclick="reviewsImageModal('<?=$comment->thumb(null, 800);?>', '<?=Html::encode($comment->title);?>')">
            <img src="<?=$comment->thumb(null, 500);?>" />
        </a>
    </div>
</div>