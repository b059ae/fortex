<?php
/* @var $slide \yii\easyii\modules\gallery\api\PhotoObject */

use yii\bootstrap\Html;

$col_lg = !empty($col_lg) ? $col_lg : '2';
$image_thumb = $slide->thumb(null, 400);
$image = $image_thumb ? $image_thumb : Yii::getAlias('@web') . '/noimage.jpg';
$description = explode("\n", $slide->description);
$title = !empty($description[0]) ? $description[0] : '';
$link = !empty($description[1]) ? $description[1] : '#';
?>
<div class="swiper-slide swiper-slide-2">
    <a class="item" href="#float-button-modal" target="_blank" data-toggle="modal">
        <div class="item-img-wrapper">
            <?=Html::img($image, ['alt' => $title]);?>
        </div>
        <div class="item-title"><?=$title;?></div>
    </a>
</div>