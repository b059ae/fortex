<?php
use yii\widgets\Breadcrumbs;
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Оформление заказа';
$this->params['breadcrumbs'][] = $this->title;

if(isset($order_info->template)):

?>
<div class="container shopcart">
    <?=Breadcrumbs::widget([
        'homeLink' => ['label' => 'Фортекс', 'url' => '/'],
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []
    ]);?>
    <h2 class="text-center" style="text-transform:none;"><?=$this->title;?></h2>
    
    <?php $form = ActiveForm::begin([
        'id' => 'shopcart-form'
    ]); ?>
    
    <?=$this->render('shopcart/' . $order_info->template, [
        'form' => $form,
        'shopcart_model' => $shopcart_model,
        'order_info' => $order_info,
        'urgency_info' => $urgency_info,
        'delivery_price' => $delivery_price,
        'min_price_free_delivery' => $min_price_free_delivery,
    ]);?>
    
    <?php ActiveForm::end(); ?>
</div>
<script>
    window.additional_price = function(){return 0;};
    window.additional_item_price = function(){return 0;};
    window.alternative_price = function(){return 0;};
</script>
<?php
$prefix = strtolower($shopcart_model->formName());
$prefix .= strlen($prefix) > 0 ? '-' : '';

$this->registerJs(<<<JS
    
    window.shopcartCost = function(){
        var alternative_price = window.alternative_price(),
            urgency_price = parseInt(jQuery('#{$prefix}urgency').find(':selected').data('urgency_price')),
            delivery_price = jQuery('#{$prefix}delivery').val() == '2' ? {$delivery_price} : 0,
            count_field = jQuery('#{$prefix}count'),
            cost = (alternative_price > 0) ? alternative_price : parseInt(count_field.data('cost')),
            count = parseInt(count_field.val()),
            total = (urgency_price + cost + window.additional_item_price()) * count;
        
        total += window.additional_price();
        total += (total < $min_price_free_delivery) ? delivery_price : 0;
        document.getElementById('shopcart-cost').innerHTML = total;
    };
    
    jQuery('#{$prefix}count').on('product-count.change', shopcartCost);
    
    jQuery('#{$prefix}delivery').on('change', function(){
        var da_field = document.getElementById('delivery_address');
        if(jQuery(this).val() == '2'){
            da_field.disabled = false;
        }
        else{
            da_field.disabled = true;
        }
        shopcartCost();
    }).trigger('change');
    
    jQuery('#{$prefix}urgency').on('change', shopcartCost);
    
    jQuery('#button-2').on('click', function(){
        var company_f = jQuery('#{$prefix}company'),
            form = $('#shopcart-form');
        if(company_f.length && !jQuery.trim(company_f.val()).length){
            form.yiiActiveForm('updateMessages', {
                'company': ['Заполните поле "Название организации"']
            }, true);
            jQuery('html, body').animate({
                scrollTop: jQuery('.has-error').first().offset().top - 100
            }, 300);
            return false;
        }
    });
    
JS
);
?>
<?php else: ?>

<div class="container text-center">
    <h2>На данный момент ваша корзина пуста</h2>
    <p>
        <?=Html::a('На главную', ['/'], ['class' => 'btn btn-info btn-lg']);?>
    </p>
</div>

<?php endif; ?>
