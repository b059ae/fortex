<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\widgets\MaskedInput;

$this->title = 'Заказ';
?>
<div class="container">
    <div class="row padding-t-30">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?php $form = ActiveForm::begin([
                'action' => ['/call?r=1'],
                'id' => 'o-callback',
            ]);?>
            <div style="height:130px;">
                <h5 class="text-center">
                    Оставьте ваш номер телефона
                </h5>
                <p class="text-center">
                    и через 8 секунд наш менеджер позвонит
                    Вам и быстро оформит заказ!
                </p>
            </div>
            
            <?=$form->field($callback_model, 'phone', ['options' => ['class' => 'text-center']])
                ->textInput()
                ->widget(MaskedInput::className(),[
                    'mask' => '+7 (999) 999-99-99'
                ]);?>
            <div class="form-group text-center">
                <?=Html::submitButton('Перезвонить мне', ['class' => 'btn btn-info']);?>
            </div>
            
            <?php ActiveForm::end();?>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?php $form = ActiveForm::begin([
                'id' => 'o-continue',
            ]);?>
            <div style="height:130px;">
                <h5 class="text-center">Оформить заказ онлайн</h5>
                <ul class="margin-b-20">
                    <li>Оплата по счету</li>
                    <li>Оплата карточкой на сайте</li>
                    <li>Оплата в офисе</li>
                </ul>
            </div>
            
            <?=$form->field($choose_model, 'phone', ['options' => ['class' => 'text-center']])
                ->textInput()
                ->widget(MaskedInput::className(),[
                    'mask' => '+7 (999) 999-99-99'
                ]);?>
            <?=Html::activeHiddenInput($choose_model, 'a');?>
            <div class="form-group text-center">
                <?=Html::submitButton('Продолжить<br>оформление заказа', ['class' => 'btn btn-info']);?>
            </div>
            
            <?php ActiveForm::end();?>
        </div>
    </div>
</div>