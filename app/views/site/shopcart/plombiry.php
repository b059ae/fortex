<?php
/* @var $form \yii\bootstrap\ActiveForm */
/* @var $shopcart_model \app\models\Shopcart */

use yii\bootstrap\Html;
use yii\widgets\MaskedInput;
use app\helpers\Product;

$good = $order_info->getGood();
$image_1 = isset($good->item->data->image) ? 'uploads/' . $good->item->data->image : '';
$image_2 = isset($good->item->data->image2) ? 'uploads/' . $good->item->data->image2 : '';
$count_prices = json_encode(Product::$plombiry_config['prices']);
?>
<div class="row">
    <div class="col-xs-12 col-sm-7 col-md-8 col-md-push-4 col-sm-push-5 margin-b-0">
        <div class="row">
            <div class="col-xs-4">
                <label class="control-label">Материал</label>
                
                <?=$form->field($shopcart_model, 'config_staff')
                    ->radioList([
                        'Латунь' => 'Латунь',
                        'Алюминий' => 'Алюминий',
                    ], ['unselect' => null])->label(false);?>
                
                <label class="control-label">Размер</label>
                
                <?=$form->field($shopcart_model, 'config_size')->radioList([
                    '20 мм' => '20 мм',
                    '22 мм' => '22 мм',
                    '24 мм' => '24 мм',
                    '25 мм (двустороний)' => '25 мм (двустороний)',
                    '29 мм' => '29 мм',
                    '40 мм' => '40 мм',
                ], ['unselect' => null])->label(false);?>
            </div>
            <div class="col-xs-8">
                <p>
                    <?=Html::img($image_1, [
                        'class' => 'img-responsive center-block',
                        'id' => 'main-image',
                    ]);?>
                </p>
            </div>
        </div>
    </div>
    
    <div class="col-xs-12 col-sm-5 col-md-4 col-md-pull-8 col-sm-pull-7 margin-b-0">
        <?=$form->field($shopcart_model, 'name')->textInput([
            'placeholder' => $shopcart_model->getAttributeLabel('name') . '*',
        ])->label(false);?>

        <?=$form->field($shopcart_model, 'phone')->textInput()
            ->label(false)->widget(MaskedInput::className(), [
                'mask' => '+7 (999) 999-99-99',
                'options' => [
                    'class' => 'form-control',
                    'placeholder' => $shopcart_model->getAttributeLabel('phone') . ' *',
                ]
            ]);?>

        <?=$form->field($shopcart_model, 'email')->textInput([
            'placeholder' => $shopcart_model->getAttributeLabel('email') . ' *',
        ])->label(false);?>

        <?=$form->field($shopcart_model, 'file')->fileInput()->label('Прикрепить образец')?>

        <?=$form->field($shopcart_model, 'config_center_text')->textarea([
            'placeholder' => 'Текст для пломбы',
        ])->label(false);?>

        <div class="row">
            <div class="col-xs-4 col-lg-6">
                <label class="control-label padding-t-10">
                    <?=$shopcart_model->getAttributeLabel('count');?>
                </label>
            </div>
            <div class="col-xs-8 col-lg-6">
                <div class="product-count pull-right">
                    <div class="product-count-minus">-</div>
                    <?=Html::activeTextInput($shopcart_model, 'count', [
                        'class' => 'product-count-input form-control',
                        'data-cost' => $order_info->getPrice(),
                    ]);?>
                    <div class="product-count-plus">+</div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-4 margin-b-0">
                <label class="control-label padding-t-10">
                    <?=$shopcart_model->getAttributeLabel('delivery');?>
                </label>
            </div>
            <div class="col-xs-8 margin-b-0">
                <?=$form->field($shopcart_model, 'delivery')->dropDownList(
                    $shopcart_model->getValueLabels('delivery'),
                    ['class' => 'chosen-select']
                )->label(false);?>
            </div>
        </div>

        <p class="text-muted small">
            Доставка по г. Ростов-на-Дону <?=$delivery_price;?>р.<br>
            При заказе стоимостью от <?=$min_price_free_delivery;?>р. доставка бесплатна.
        </p>

        <?=$form->field($shopcart_model, 'delivery_address')->textInput([
            'placeholder' => $shopcart_model->getAttributeLabel('delivery_address'),
        ])->label(false);?>

        <div class="row">
            <div class="col-xs-4 margin-b-0">
                <label class="control-label">
                    <?=$shopcart_model->getAttributeLabel('urgency');?>
                </label>
            </div>
            <div class="col-xs-8 margin-b-0">
                <?=$form->field($shopcart_model, 'urgency')->dropDownList(
                    $shopcart_model->getValueLabels('urgency'),
                    [
                        'class' => 'chosen-select',
                        'options' => $urgency_info,
                    ])->label(false);?>
            </div>
        </div>

        <div class="row text-danger">
            <div class="col-xs-6 margin-b-0">
                <label class="control-label padding-t-10">
                    К оплате, руб
                </label>
            </div>
            <div class="col-xs-6 margin-b-0">
                <strong style="font-size:35px;" id="shopcart-cost"></strong>
            </div>
        </div>
        
    </div>
    <div class="clearfix"></div>
    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 margin-b-0">
        <?=$form->field($shopcart_model, 'agree')
            ->inline(true)
            ->checkbox()
            ->label('<p class="small text-muted margin-b-20">
            Я согласен на обработку своих персональных данных
            в соответствии с положениями Федерального закона от 27.07.2006 №152-ФЗ
            "О персональных данных" и условиями Политики в отношении обработки персональных данных
        </p>');?>
    </div>
</div>

<div class="row">
<?php foreach($shopcart_model->getValueLabels('order_type') as $key => $value): ?>
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 text-center-sm text-center-xs">
        <?=Html::submitButton($value, [
            'id' => 'button-' . $key,
            'class' => 'btn btn-danger',
            'name' => 'order_type',
            'value' => $key,
        ]);?>
    </div>
<?php endforeach; ?>
</div>


<?php
$this->registerJs(<<<JS
    var count_field = document.getElementById('count'),
        count_prices = $count_prices;
    
    window.alternative_price = function(){
        var count = count_field.value;
        if(count >= 1 && count <= 10){
            return count_prices[0];
        }
        if(count >= 11 && count <= 50){
            return count_prices[1];
        }
        if(count >= 51 && count <= 100){
            return count_prices[2];
        }
        if(count >= 101 && count <= 500){
            return count_prices[3];
        }
        if(count >= 501 && count <= 1000){
            return count_prices[4];
        }
        if(count >= 1000){
            return count_prices[5];
        }
    };
    
    var image_1 = new Image(),
        image_2 = new Image(),
        staff_buttons = jQuery('[name="config_staff"]'),
        main_image = document.getElementById('main_image');
    image_1.src = '$image_1';
    image_2.src = '$image_2';
    
    staff_buttons.eq(0)
        .on('click', function(){document.getElementById('main-image').src = image_1.src;})
        .trigger('click');
        
    staff_buttons.eq(1)
        .on('click', function(){document.getElementById('main-image').src = image_2.src;});
JS
);
?>