<?php
/* @var $form \yii\bootstrap\ActiveForm */
/* @var $shopcart_model \app\models\Shopcart */

use yii\bootstrap\Html;
use yii\widgets\MaskedInput;

$cliche_subcat = $order_info->getClicheInfo('cat_slug');
?>
<div class="row">
    <div class="col-xs-12 col-sm-5 col-sm-push-7 margin-b-0">
        <p>
            <?=Html::img($order_info->getClicheInfo('item')->thumb(null, 250), [
                'class' => 'img-responsive center-block'
            ]);?>
        </p>
        <p>
            <?=Html::img($order_info->getToolInfo('item')->thumb(null, 400), [
                'class' => 'img-responsive center-block'
            ]);?>
        </p>
    </div>
    
    <div class="col-xs-12 col-sm-7 col-sm-pull-5 margin-b-0">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 margin-b-0">
                <?=$form->field($shopcart_model, 'phone')->textInput()
                    ->label(false)->widget(MaskedInput::className(), [
                        'mask' => '+7 (999) 999-99-99',
                        'options' => [
                            'class' => 'form-control',
                            'placeholder' => $shopcart_model->getAttributeLabel('phone') . ' *',
                        ]
                    ]);?>

                <?=$form->field($shopcart_model, 'email')->textInput([
                    'placeholder' => $shopcart_model->getAttributeLabel('email') . ' *',
                ])->label(false);?>

                <?php if($cliche_subcat !== 'doctor' && $cliche_subcat !=='extraordinary'): ?>
                <?=$form->field($shopcart_model, 'inn')->textInput([
                    'placeholder' => $shopcart_model->getAttributeLabel('inn') . ' *',
                ])->label(false);?>
                <?php endif; ?>

                <?=$form->field($shopcart_model, 'name')->textInput([
                    'placeholder' => $shopcart_model->getAttributeLabel('name') . '*',
                ])->label(false);?>

                <?php if($cliche_subcat !== 'doctor' && $cliche_subcat !=='extraordinary'): ?>
                    <?=$form->field($shopcart_model, 'ogrnip')->textInput([
                        'placeholder' => $shopcart_model->getAttributeLabel('ogrnip'),
                    ])->label(false);?>
                <?php endif; ?>

                <?=$form->field($shopcart_model, 'country')->textInput([
                    'placeholder' => $shopcart_model->getAttributeLabel('country'),
                ])->label(false);?>

                <?=$form->field($shopcart_model, 'address')->textInput([
                    'placeholder' => $shopcart_model->getAttributeLabel('address'),
                ])->label(false);?>


                <div class="row">
                    <div class="col-xs-4 col-lg-6">
                        <label class="control-label padding-t-10">
                            <?=$shopcart_model->getAttributeLabel('count');?>
                        </label>
                    </div>
                    <div class="col-xs-8 col-lg-6">
                        <div class="product-count pull-right">
                            <div class="product-count-minus">-</div>
                            <?=Html::activeTextInput($shopcart_model, 'count', [
                                'class' => 'product-count-input form-control',
                                'data-cost' => $order_info->getPrice(),
                            ]);?>
                            <div class="product-count-plus">+</div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-4 margin-b-0">
                        <label class="control-label padding-t-10">
                            <?=$shopcart_model->getAttributeLabel('delivery');?>
                        </label>
                    </div>
                    <div class="col-xs-8 margin-b-0">
                        <?=$form->field($shopcart_model, 'delivery')->dropDownList(
                            $shopcart_model->getValueLabels('delivery'),
                            ['class' => 'chosen-select']
                        )->label(false);?>
                    </div>
                </div>

                <p class="text-muted small">
                    Доставка по г. Ростов-на-Дону <?=$delivery_price;?>р.<br>
                    При заказе стоимостью от <?=$min_price_free_delivery;?>р. доставка бесплатна.
                </p>

                <?=$form->field($shopcart_model, 'delivery_address')->textInput([
                    'placeholder' => $shopcart_model->getAttributeLabel('delivery_address'),
                ])->label(false);?>


                <div class="row">
                    <div class="col-xs-4 margin-b-0">
                        <label class="control-label">
                            <?=$shopcart_model->getAttributeLabel('urgency');?>
                        </label>
                    </div>
                    <div class="col-xs-8 margin-b-0">
                        <?=$form->field($shopcart_model, 'urgency')->dropDownList(
                            $shopcart_model->getValueLabels('urgency'),
                            [
                                'class' => 'chosen-select',
                                'options' => $urgency_info,
                            ])->label(false);?>
                    </div>
                </div>

                <div class="row text-danger">
                    <div class="col-xs-6 margin-b-0">
                        <label class="control-label padding-t-10">
                            К оплате, руб
                        </label>
                    </div>
                    <div class="col-xs-6 margin-b-0">
                        <strong style="font-size:35px;" id="shopcart-cost"></strong>
                    </div>
                </div>
                
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 margin-b-0 ">
                <?=$form->field($shopcart_model, 'config_center_text')->textInput([
                    'placeholder' => $shopcart_model->getAttributeLabel('config_center_text'),
                ])->label(false);?>

                <?php if($cliche_subcat !== 'doctor' && $cliche_subcat !=='extraordinary'): ?>
                    <?=$form->field($shopcart_model, 'company')->textInput([
                        'placeholder' => $shopcart_model->getAttributeLabel('company'),
                    ])->label(false);?>
                <?php endif; ?>

                <?=$form->field($shopcart_model, 'config_info')->textInput([
                    'placeholder' => $shopcart_model->getAttributeLabel('config_info'),
                ])->label(false);?>

                <?=$form->field($shopcart_model, 'config_font')->dropDownList([
                    'HeliosCond' => 'HeliosCond'
                ], ['class' => 'chosen-select'])->label(false);?>

                <?=$form->field($shopcart_model, 'file')->fileInput()->label('Загрузить логотип')?>

                <?=$form->field($shopcart_model, 'config_protection')
                    ->inline(true)
                    ->checkboxList($shopcart_model->getValueLabels('config_protection'));?>

            </div>
        </div>

        <?=$form->field($shopcart_model, 'agree')
            ->inline(true)
            ->checkbox()
            ->label('<p class="small text-muted">
            Я согласен на обработку своих персональных данных
            в соответствии с положениями Федерального закона от 27.07.2006 №152-ФЗ
            "О персональных данных" и условиями Политики в отношении обработки персональных данных
        </p>');?>
        
    </div>
</div>
<div class="row">
<?php foreach($shopcart_model->getValueLabels('order_type') as $key => $value): ?>
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 text-center-xs text-center-sm">
        <?=Html::submitButton($value, [
            'id' => 'button-' . $key,
            'class' => 'btn btn-danger',
            'name' => 'order_type',
            'value' => $key,
        ]);?>
    </div>
<?php endforeach; ?>
</div>