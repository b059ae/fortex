<?php
/* @var $form \yii\bootstrap\ActiveForm */
/* @var $shopcart_model \app\models\Shopcart */

use yii\bootstrap\Html;
use yii\widgets\MaskedInput;
use app\helpers\CatalogHelper;

$cliche = $order_info->getClicheInfo('item');
$cliche_image = !empty($cliche->data->image)
    ? 'uploads/' . $cliche->data->image : $cliche->thumb(null, 250);
$tool = $order_info->getToolInfo('item');
$tool_image = !empty($tool->data->image)
    ? 'uploads/' . $tool->data->image : $tool->thumb(null, 400);
?>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-8 col-md-push-4 col-sm-push-6 margin-b-0">
        <div class="row">
            <div class="col-xs-4">
                <h5>Выбор размера</h5>

                <?=$form->field($shopcart_model, 'config_size')->radioList([
                    '10x26 мм' => '10x26 мм',
                    '14x36 мм' => '14x36 мм',
                    '24x56 мм' => '24x56 мм',
                    '30x54 мм' => '30x54 мм',
                    '43x43x43 мм (треугольная)' => '43x43x43 мм (треугольная)',
                    '46x66 мм' => '46x66 мм',
                ], ['unselect' => null])->label(false);?>

                <p class="small text-muted hidden-xs"><?=$shopcart_model->stampySizeNotice();?></p>
            </div>
            <div class="col-xs-8">
                <p><?=Html::img($cliche_image, ['class' => 'img-responsive center-block']);?></p>
                <p><?=Html::img($tool_image, ['class' => 'img-responsive center-block']);?></p>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-md-pull-8 col-sm-pull-6 margin-b-0">
        
        <?=$form->field($shopcart_model, 'name')->textInput([
            'placeholder' => $shopcart_model->getAttributeLabel('name') . '*',
        ])->label(false);?>

        <?=$form->field($shopcart_model, 'phone')->textInput()
            ->label(false)->widget(MaskedInput::className(), [
                'mask' => '+7 (999) 999-99-99',
                'options' => [
                    'class' => 'form-control',
                    'placeholder' => $shopcart_model->getAttributeLabel('phone') . ' *',
                ]
            ]);?>

        <?=$form->field($shopcart_model, 'email')->textInput([
            'placeholder' => $shopcart_model->getAttributeLabel('email') . ' *',
        ])->label(false);?>

        <?=$form->field($shopcart_model, 'file')->fileInput()->label('Прикрепить образец')?>

        <?=$form->field($shopcart_model, 'config_center_text')->textarea([
            'placeholder' => 'Текст для штампа',
        ])->label(false);?>

        <?=$form->field($shopcart_model, 'config_frame')->radioList([
            'Да' =>'Штамп в рамке',
            'Нет' =>'Штамп без рамки',
        ], ['item' => CatalogHelper::getRadioListTemplator()])->label(false);?>

        <div class="row">
            <div class="col-xs-12 col-sm-5">
                <label class="control-label padding-t-10">
                    <?=$shopcart_model->getAttributeLabel('count');?>
                </label>
            </div>
            <div class="col-xs-12 col-sm-7">
                <div class="product-count">
                    <div class="product-count-minus">-</div>
                    <?=Html::activeTextInput($shopcart_model, 'count', [
                        'class' => 'product-count-input form-control',
                        'data-cost' => $order_info->getPrice(),
                    ]);?>
                    <div class="product-count-plus">+</div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-4 margin-b-0">
                <label class="control-label padding-t-10">
                    <?=$shopcart_model->getAttributeLabel('delivery');?>
                </label>
            </div>
            <div class="col-xs-8 margin-b-0">
                <?=$form->field($shopcart_model, 'delivery')->dropDownList(
                    $shopcart_model->getValueLabels('delivery'),
                    ['class' => 'chosen-select']
                )->label(false);?>
            </div>
        </div>

        <p class="text-muted small">
            Доставка по г. Ростов-на-Дону <?=$delivery_price;?>р.<br>
            При заказе стоимостью от <?=$min_price_free_delivery;?>р. доставка бесплатна.
        </p>

        <?=$form->field($shopcart_model, 'delivery_address')->textInput([
            'placeholder' => $shopcart_model->getAttributeLabel('delivery_address'),
        ])->label(false);?>

        <div class="row">
            <div class="col-xs-4 margin-b-0">
                <label class="control-label">
                    <?=$shopcart_model->getAttributeLabel('urgency');?>
                </label>
            </div>
            <div class="col-xs-8 margin-b-0">
                <?=$form->field($shopcart_model, 'urgency')->dropDownList(
                    $shopcart_model->getValueLabels('urgency'),
                    [
                        'class' => 'chosen-select',
                        'options' => $urgency_info,
                    ])->label(false);?>
            </div>
        </div>

        <div class="row text-danger">
            <div class="col-xs-6 margin-b-0">
                <label class="control-label padding-t-10">
                    К оплате, руб
                </label>
            </div>
            <div class="col-xs-6 margin-b-0">
                <strong style="font-size:35px;" id="shopcart-cost"></strong>
            </div>
        </div>
            
        
        
        <?=$form->field($shopcart_model, 'agree')
            ->inline(true)
            ->checkbox()
            ->label('<p class="small text-muted margin-b-20">
            Я согласен на обработку своих персональных данных
            в соответствии с положениями Федерального закона от 27.07.2006 №152-ФЗ
            "О персональных данных" и условиями Политики в отношении обработки персональных данных
        </p>');?>

    </div>
</div>
<div class="row">
<?php foreach($shopcart_model->getValueLabels('order_type') as $key => $value): ?>
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 text-center-xs text-center-sm">
        <?=Html::submitButton($value, [
            'id' => 'button-' . $key,
            'class' => 'btn btn-danger',
            'name' => 'order_type',
            'value' => $key,
        ]);?>
    </div>
<?php endforeach; ?>
</div>