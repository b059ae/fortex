<?php
/* @var $form \yii\bootstrap\ActiveForm */
/* @var $shopcart_model \app\models\Shopcart */

use yii\bootstrap\Html;
use yii\widgets\MaskedInput;
use yii\easyii\helpers\Image;

$item = $order_info->getGood()->item;
$image = $item->thumb(null, 500);
?>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-7 col-md-push-5 col-sm-push-6 margin-b-0">
        <p><?=Html::img($image, ['class' => 'img-responsive center-block']);?></p>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-5 col-md-pull-7 col-sm-pull-6 margin-b-0">
        <?=$form->field($shopcart_model, 'name')->textInput([
                'placeholder' => $shopcart_model->getAttributeLabel('name') . '*',
            ])->label(false);?>
                
        <?=$form->field($shopcart_model, 'phone')->textInput()
            ->label(false)->widget(MaskedInput::className(), [
                'mask' => '+7 (999) 999-99-99',
                'options' => [
                    'class' => 'form-control',
                    'placeholder' => $shopcart_model->getAttributeLabel('phone') . ' *',
                ]
            ]);?>

        <?=$form->field($shopcart_model, 'email')->textInput([
            'placeholder' => $shopcart_model->getAttributeLabel('email') . ' *',
        ])->label(false);?>
        
        <?php if($item->cat->slug === 'datery-so-svobodnym-polem'): ?>
        
        <?=$form->field($shopcart_model, 'file')
            ->fileInput()
            ->label('Прикрепить образец');?>
        
        <?=$form->field($shopcart_model, 'config_center_text')->textarea([
            'placeholder' => 'Текст для датера',
        ])->label(false);?>
        
        <?php elseif(
            $item->slug === 'stempelnaa-poduska'
            || $item->slug === 'stempelnaa-kraska'
        ): ?>
        <?php
            $brands = [
                'Colop' => 'Colop',
                'Trodat' => 'Trodat',
                'GRM' => 'GRM',
            ];
            $colors = [
                'синий' => 'синий',
                'красный' => 'красный',
                'зеленый' => 'зеленый',
                'черный' => 'черный',
                'фиолетовый' => 'фиолетовый',
            ];
            if($item->slug === 'stempelnaa-poduska'){
                $colors['без цвета'] = 'без цвета';
            }
            
        ?>
        
        <div class="row">
            <div class="col-xs-6">
                <?=$form->field($shopcart_model, 'config_brand')
                    ->radioList($brands, ['unselect' => null])
                    ->label('Выбор производителя');?>
            </div>
            <div class="col-xs-6">
                <?=$form->field($shopcart_model, 'config_color')
                    ->radioList($colors, ['unselect' => null])
                    ->label('Выбор цвета');?>
            </div>
        </div>
        
        <?php endif; ?>

        <div class="row">
            <div class="col-xs-4 col-lg-6">
                <label class="control-label padding-t-10">
                    <?=$shopcart_model->getAttributeLabel('count');?>
                </label>
            </div>
            <div class="col-xs-8 col-lg-6">
                <div class="product-count pull-right">
                    <div class="product-count-minus">-</div>
                    <?=Html::activeTextInput($shopcart_model, 'count', [
                        'class' => 'product-count-input form-control',
                        'data-cost' => $order_info->getPrice(),
                    ]);?>
                    <div class="product-count-plus">+</div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-4 margin-b-0">
                <label class="control-label padding-t-10">
                    <?=$shopcart_model->getAttributeLabel('delivery');?>
                </label>
            </div>
            <div class="col-xs-8 margin-b-0">
                <?=$form->field($shopcart_model, 'delivery')->dropDownList(
                    $shopcart_model->getValueLabels('delivery'),
                    ['class' => 'chosen-select']
                )->label(false);?>
            </div>
        </div>

        <p class="text-muted small">
            Доставка по г. Ростов-на-Дону <?=$delivery_price;?>р.<br>
            При заказе стоимостью от <?=$min_price_free_delivery;?>р. доставка бесплатна.
        </p>

        <?=$form->field($shopcart_model, 'delivery_address')->textInput([
            'placeholder' => $shopcart_model->getAttributeLabel('delivery_address'),
        ])->label(false);?>

        <div class="row">
            <div class="col-xs-4 margin-b-0">
                <label class="control-label">
                    <?=$shopcart_model->getAttributeLabel('urgency');?>
                </label>
            </div>
            <div class="col-xs-8 margin-b-0">
                <?=$form->field($shopcart_model, 'urgency')->dropDownList(
                    $shopcart_model->getValueLabels('urgency'),
                    [
                        'class' => 'chosen-select',
                        'options' => $urgency_info,
                    ])->label(false);?>
            </div>
        </div>

        <div class="row text-danger">
            <div class="col-xs-6 margin-b-0">
                <label class="control-label padding-t-10">
                    К оплате, руб
                </label>
            </div>
            <div class="col-xs-6 margin-b-0">
                <strong style="font-size:35px;" id="shopcart-cost"></strong>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-xs-12 col-sm-6 col-md-7 margin-b-0">
        <?=$form->field($shopcart_model, 'agree')
            ->inline(true)
            ->checkbox()
            ->label('<p class="small text-muted margin-b-20">
            Я согласен на обработку своих персональных данных
            в соответствии с положениями Федерального закона от 27.07.2006 №152-ФЗ
            "О персональных данных" и условиями Политики в отношении обработки персональных данных
        </p>');?>
    </div>
</div>
<div class="row">
<?php foreach($shopcart_model->getValueLabels('order_type') as $key => $value): ?>
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 text-center-xs text-center-sm">
        <?=Html::submitButton($value, [
            'id' => 'button-' . $key,
            'class' => 'btn btn-danger',
            'name' => 'order_type',
            'value' => $key,
        ]);?>
    </div>
<?php endforeach; ?>
</div>