<?php
/* @var $form \yii\bootstrap\ActiveForm */
/* @var $shopcart_model \app\models\Shopcart */

use yii\bootstrap\Html;
use yii\widgets\MaskedInput;
use app\helpers\Product;

$images = $order_info->getImages(null, 500);
$count_prices = json_encode(Product::$shtok_config['prices']);
?>
<div class="row">
    <div class="col-xs-12 col-sm-7 col-md-8 col-md-push-4 col-sm-push-5 margin-b-0">
        <div class="row">
            <div class="col-xs-5">
                <h5>Выбор материала и размера</h5>
                
                <?=$form->field($shopcart_model, 'config_staff')->radioList([
                    'латунь d25 мм' => 'латунь d25 мм',
                    'латунь d35 мм' => 'латунь d35 мм',
                    'алюминий d25 мм' => 'алюминий d25 мм',
                ], ['unselect' => null])->label(false);?>
            </div>
            <div class="col-xs-7">
                <p>
                    <?=Html::img($images[0], [
                        'class' => 'img-responsive center-block',
                        'id' => 'main-image',
                    ]);?>
                </p>
            </div>
        </div>
    </div>
    
    <div class="col-xs-12 col-sm-5 col-md-4 col-md-pull-8 col-sm-pull-7 margin-b-0">
        <?=$form->field($shopcart_model, 'name')->textInput([
            'placeholder' => $shopcart_model->getAttributeLabel('name') . '*',
        ])->label(false);?>

        <?=$form->field($shopcart_model, 'phone')->textInput()
            ->label(false)->widget(MaskedInput::className(), [
                'mask' => '+7 (999) 999-99-99',
                'options' => [
                    'class' => 'form-control',
                    'placeholder' => $shopcart_model->getAttributeLabel('phone') . ' *',
                ]
            ]);?>

        <?=$form->field($shopcart_model, 'email')->textInput([
            'placeholder' => $shopcart_model->getAttributeLabel('email') . ' *',
        ])->label(false);?>

        <div class="row">
            <div class="col-xs-4 col-lg-6">
                <label class="control-label padding-t-10">
                    <?=$shopcart_model->getAttributeLabel('count');?>
                </label>
            </div>
            <div class="col-xs-8 col-lg-6">
                <div class="product-count pull-right">
                    <div class="product-count-minus">-</div>
                    <?=Html::activeTextInput($shopcart_model, 'count', [
                        'class' => 'product-count-input form-control',
                        'data-cost' => $order_info->getPrice(),
                    ]);?>
                    <div class="product-count-plus">+</div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-4 margin-b-0">
                <label class="control-label padding-t-10">
                    <?=$shopcart_model->getAttributeLabel('delivery');?>
                </label>
            </div>
            <div class="col-xs-8 margin-b-0">
                <?=$form->field($shopcart_model, 'delivery')->dropDownList(
                    $shopcart_model->getValueLabels('delivery'),
                    ['class' => 'chosen-select']
                )->label(false);?>
            </div>
        </div>

        <p class="text-muted small">
            Доставка по г. Ростов-на-Дону <?=$delivery_price;?>р.<br>
            При заказе стоимостью от <?=$min_price_free_delivery;?>р. доставка бесплатна.
        </p>

        <?=$form->field($shopcart_model, 'delivery_address')->textInput([
            'placeholder' => $shopcart_model->getAttributeLabel('delivery_address'),
        ])->label(false);?>

        <div class="row">
            <div class="col-xs-4 margin-b-0">
                <label class="control-label">
                    <?=$shopcart_model->getAttributeLabel('urgency');?>
                </label>
            </div>
            <div class="col-xs-8 margin-b-0">
                <?=$form->field($shopcart_model, 'urgency')->dropDownList(
                    $shopcart_model->getValueLabels('urgency'),
                    [
                        'class' => 'chosen-select',
                        'options' => $urgency_info,
                    ])->label(false);?>
            </div>
        </div>

        <div class="row text-danger">
            <div class="col-xs-6 margin-b-0">
                <label class="control-label padding-t-10">
                    К оплате, руб
                </label>
            </div>
            <div class="col-xs-6 margin-b-0">
                <strong style="font-size:35px;" id="shopcart-cost"></strong>
            </div>
        </div>
        
    </div>
    <div class="clearfix"></div>
    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 margin-b-0">
        <?=$form->field($shopcart_model, 'agree')
            ->inline(true)
            ->checkbox()
            ->label('<p class="small text-muted margin-b-20">
            Я согласен на обработку своих персональных данных
            в соответствии с положениями Федерального закона от 27.07.2006 №152-ФЗ
            "О персональных данных" и условиями Политики в отношении обработки персональных данных
        </p>');?>
    </div>
</div>

<div class="row">
<?php foreach($shopcart_model->getValueLabels('order_type') as $key => $value): ?>
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 text-center-sm text-center-xs">
        <?=Html::submitButton($value, [
            'id' => 'button-' . $key,
            'class' => 'btn btn-danger',
            'name' => 'order_type',
            'value' => $key,
        ]);?>
    </div>
<?php endforeach; ?>
</div>
<?php
$this->registerJs(<<<JS
    var count_field = document.getElementById('count'),
        count_prices = $count_prices;
    
    jQuery('[name="config_staff"]').eq(0).trigger('click');
    
    window.alternative_price = function(){
        var count = count_field.value;
        
        if(count >= 1 && count <= 10){
            return count_prices[0];
        }
        if(count >= 11 && count <= 50){
            return count_prices[1];
        }
        if(count >= 51){
            return count_prices[2];
        }
    };
JS
);
?>