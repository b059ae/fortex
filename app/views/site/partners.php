<?php
$this->title = 'Наши партнеры';
?>
<div class="container">
    <div class="row">
        <?php $index = 0; ?>
        <?php foreach($partners as $partner): ?>
            <?=$this->render('//site/chunks/_gallery_partner_full', [
                'partner' => $partner,
                'index' => $index,
            ]);?>
            <?php $index++; ?>
        <?php endforeach; ?>
    </div>
</div>