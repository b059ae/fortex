<?php
use yii\easyii\models\Setting;
use yii\bootstrap\Html;

$phone = Setting::get('phone');
$email = Setting::get('admin_email');
$controller = Yii::$app->controller;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?=Yii::$app->language ?>" class="no-js">
<head>
    <meta charset="<?=Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="keywords"/>

    <?= Html::csrfMetaTags() ?>
    <title>
        <?=Html::encode(Yii::$app->name);?>
        <?=(!empty($this->title) ? '- '.Html::encode($this->title) : ''); ?>
    </title>
    <link href= rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <?= \yii\easyii\widgets\Metrika::widget(); ?>

    <?=$this->render('chunks/_jivosite');?>

    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
    <?=$this->render('chunks/_header', [
        'phone' => $phone,
        'email' => $email,
    ]);?>
    
<div class="<?=$controller->id . '-' . $controller->action->id;?>" style="min-height:400px;">
    <?=$content;?>
</div>

    <?=$this->render('chunks/_callback_float'); ?>
    <?=$this->render('chunks/_footer', [
        'phone' => $phone,
        'email' => $email,
    ]);?>
<?php $this->endBody();?>
</body>
</html>
<?php $this->endPage();?>