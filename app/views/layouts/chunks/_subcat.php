<?php
/* @var $cat \yii\easyii\modules\catalog\api\CategoryObject */

use yii\helpers\Url;
use yii\bootstrap\Html;
?>
<div class="col-xs-12 col-sm-3 col-md-2">
    <a class="item" href="<?=Url::toRoute(['catalog/subcat', 'cat' => $cat->model->parent,  'slug' => $cat->slug]);?>">
        <div class="item-img-wrapper">
            <?=Html::img($cat->thumb(null, 400), ['alt' => $cat->title]);?>
        </div>
        <div class="item-title"><?=$cat->title;?></div>
    </a>
</div>