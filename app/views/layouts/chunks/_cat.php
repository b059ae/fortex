<?php
/* @var $cat \yii\easyii\modules\catalog\api\CategoryObject */

use yii\helpers\Url;
use yii\bootstrap\Html;
$url_data = isset($order_type) ?
    ['/order-type', 'category' => $cat->slug] : ['/catalog/cat', 'slug' => $cat->slug];
?>
<div class="col-xs-12 col-sm-6 col-md-3 col-lg-5ths">
    <a class="item" href="<?=Url::to($url_data);?>">
        <div class="item-img-wrapper">
            <?=Html::img($cat->thumb(null, 400), ['alt' => $cat->title]);?>
        </div>
        <div class="item-title"><?=$cat->title;?></div>
    </a>
</div>