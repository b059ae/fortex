<?php
/* @var $item \yii\easyii\modules\catalog\api\ItemObject */

use yii\helpers\Url;
use yii\helpers\Html;
$a = Yii::$app->request->get('a');
$b = Yii::$app->request->get('b');
$url_data = $a === 'buy' ?
    ['site/buy', 'id' => $item->id]
    : ['site/buy', 'id' => $item->id, 'a' => $a, 'b' => $b];
?>
<a class="item" href="<?=Url::toRoute($url_data);?>">
    <div class="item-img-wrapper">
        <?=Html::img($item->thumb(null, 250), ['alt' => $item->title]);?>
    </div>
    <div class="item-title"><?=$item->title;?></div>
    <div class="item-price">Цена: <?=$item->price;?> руб.</div>
</a>