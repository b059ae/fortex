<?php
/* @var $item \yii\easyii\modules\catalog\api\ItemObject */

use yii\helpers\Url;
use yii\helpers\Html;

$a = Yii::$app->request->get('a');
$url_data = $a === 'buy' ?
    ['site/buy', 'id' => $item->id]
    : ['site/buy', 'id' => $item->id, 'a' => 'tools'];
?>
<a class="item" href="<?=Url::toRoute($url_data);?>">
    <div class="item-img-wrapper">
        <?=Html::img($item->thumb(245, 245), ['alt' => $item->title]);?>
    </div>
    <div class="item-title"><?=$item->title;?></div>
</a>