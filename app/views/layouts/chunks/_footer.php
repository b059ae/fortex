<?php
use app\models\forms\MainContactForm;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
$asset = \app\assets\AppAsset::register($this);
?>
<a class="anchor" id="contact"></a>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <p class="phone">
                    <?=Html::a($phone, 'tel:' . preg_replace('/[^\+\d]/', '', $phone));?>
                </p>
                <p class="mail">
                    <?=Html::a($email, 'mailto:' . $email);?>
                </p>
                <p>344006, РФ, г. Ростов-на-Дону, ﻿пр. Кировский, 37</p>
                <p>Пн.-Пт. с 9.00 до 18:00</br>Сб.-Вс. — выходной.</br>Выдача товара осуществляется в будние дни с 9.00 до 18.00</p>
            </div>
            <div class="col-xs-12 col-sm-6">
                <h3>Оставьте сообщение</h3>
                
                <?php 
                    $form_model = new MainContactForm();
                    $form = ActiveForm::begin([
                        'action' => '/send-message',
                        'id' => 'main-contact-form',
                    ]);
                ?>
                
                <?=$form->field($form_model, 'name')
                    ->textInput(['placeholder' => $form_model->getAttributeLabel('name')])
                    ->label(false);?>
                
                <?=$form->field($form_model, 'email')
                    ->textInput(['placeholder' => $form_model->getAttributeLabel('email')])
                    ->label(false);?>

                <?=$form->field($form_model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '+7 (999) 999 99 99',
                    'options' => [
                        'autocomplete' => 'off',
                        'class' => 'form-control',
                        'placeholder' => $form_model->getAttributeLabel('phone')
                    ],
                ])->label(false);?>

                <?=$form->field($form_model, 'message')->textarea([
                    'placeholder' => $form_model->getAttributeLabel('message'),
                ])->label(false);?>
                
                <?=Html::submitButton('Отправить', [
                    'class' => 'btn btn-info btn-lg'
                ])?>
                    
                <?php ActiveForm::end(); ?>
                
            </div>
        </div>
    </div>
</footer>

<?php
$this->registerJs(<<<JS
    var main_contact_form = jQuery('#main-contact-form');
    
    main_contact_form.on('beforeSubmit.yii', function() {
        var data = main_contact_form.serialize();
        jQuery.post(main_contact_form.attr('action'), data, function(data){
            if(data.success){
                metrikaReachGoal('application');
                document.location = '/success';
            }
        });
        return false;
    });
        
        
    jQuery('form[action="/call"]').on('beforeSubmit.yii', function(){
        metrikaReachGoal('application');
    });
JS
);
?>