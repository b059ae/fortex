<?php
/* @var $cat \yii\easyii\modules\catalog\api\CategoryObject */

use yii\helpers\Url;
use yii\bootstrap\Html;
use app\helpers\CatalogHelper;
$a = Yii::$app->request->get('a');
?>
<div class="col-xs-12 col-sm-6 col-md-3">
    <a class="item" href="<?=Url::toRoute(['catalog/index', 'slug' => $cat->slug, 'a' => $a]);?>">
        <div class="item-img-wrapper">
            <?=Html::img($cat->thumb(null, 400), ['alt' => $cat->title]);?>
        </div>
        <div class="item-title"><?=$cat->title;?></div>
        <div class="item-price">
            Цена от <?=CatalogHelper::getMinPrice($cat);?> руб.
        </div>
    </a>
</div>