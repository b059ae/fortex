<?php
/* @var $cat \yii\easyii\modules\catalog\api\CategoryObject */

use yii\helpers\Url;
use yii\bootstrap\Html;
use app\helpers\CatalogHelper;
$variant = $variant ? $variant : null;
$a = Yii::$app->request->get('a');
$b = Yii::$app->request->get('b');
?>
<div class="col-xs-12 col-sm-6 col-md-3">
    <a class="item" href="<?=Url::toRoute(['catalog/tools-examples',
            'slug' => $cat->slug, 'a' => $a, 'b' => $b]);?>">
        <div class="item-img-wrapper">
            <?=Html::img($cat->thumb(null, 400), ['alt' => $cat->title]);?>
        </div>
        <div class="item-title"><?=$cat->title;?></div>
        <div class="item-flying-info">
            <div class="item-flying-info-inner">
                <span class="item-flying-info-bottom">
                    от <?=CatalogHelper::getMinPrice($cat);?> руб.
                </span>
            </div>
        </div>
    </a>
</div>