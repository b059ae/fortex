<?php
use yii\bootstrap\Html;
$asset = \app\assets\AppAsset::register($this);
?>
<div class="header" id="navbar">
    <div class="container">
        <div class="row text-center">
            <div class="col-lg-3 col-md-3 col-sm-4">
                <?=Html::a(
                    '<img src="'.$asset->baseUrl.'/img/logo.png" class="header-logo">',
                    ['/']
                );?>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4">
                <?=Html::a(
                    $phone, 'tel:' . preg_replace('/[^\+\d]/', '', $phone),
                    ['class' => 'header-phone']
                );?>
            </div>
            <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">
                <?=Html::a(
                    $email, 'mailto:' . $email,
                    ['class' => 'header-email']
                );?>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4">
                <?=Html::button('Перезвонить за наш счет', [
                    'class' => 'btn btn-2 btn-default btn-sm header-callback-btn',
                    'data-toggle' => 'modal',
                    'data-target' => '#float-button-modal',
                ]);?>
            </div>
        </div>
    </div>
</div>