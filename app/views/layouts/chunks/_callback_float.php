<?php
/* @var $this \yii\web\View */
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\widgets\MaskedInput;

Modal::begin([
    'id' => 'float-button-modal',
    'header' => '<h3 class="text-center margin-b-0">Перезвоним за 8 секунд</h3>',
]);
$form_model = new \app\models\forms\CallbackForm();
$form = ActiveForm::begin([
    'action' => ['/call'],
]);
?>
<?=$form->field($form_model, 'phone')
    ->textInput()
    ->label('Введите ваш контактный телефон')
    ->widget(MaskedInput::className(), [
        'mask' => '+7 (999) 999-99-99'
    ]);?>
<p class="text-center">
    <?=Html::submitButton('Жду звонка', ['class' => 'btn btn-info btn-lg']); ?>
</p>
                
<?php
ActiveForm::end();
Modal::end();
?>