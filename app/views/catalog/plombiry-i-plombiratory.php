<?php
/* @var $cat \yii\easyii\modules\catalog\api\CategoryObject */
/* @var $items \yii\easyii\modules\catalog\api\ItemObject[] */

use yii\widgets\Breadcrumbs;
use yii\bootstrap\Html;
use yii\helpers\Url;

$this->title = Html::encode($cat->title);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <?=Breadcrumbs::widget([
        'homeLink' => ['label' => 'Фортекс', 'url' => '/'],
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []
    ]);?>
    <h2 class="text-center"><?=$this->title;?></h2>
    <div class="row auto-clear">
        <?php foreach($items as $item): ?>
            <div class="col-xs-6 col-sm-4 col-md-5ths col-lg-5ths">
                <a class="item" href="<?=Url::toRoute(['site/buy', 'id' => $item->id]);?>">
                    <div class="item-img-wrapper">
                        <?=Html::img($item->thumb(245, 245), ['alt' => $item->title]);?>
                    </div>
                    <div class="item-title">
                        <?=$item->title;?>
                        <?php if(!empty($item->data->priceinfo)):?>
                            <br>(<?=$item->data->priceinfo;?>)
                        <?php endif; ?>
                    </div>
                </a>
            </div>
        <?php endforeach; ?>

        <?php $pagination = $cat->getPages(); ?>
        <?php if(!empty($pagination)): ?>
            <div class="clearfix margin-b-10"></div>
            <div class="col-xs-12 text-center">
                <?=$pagination;?>
            </div>
        <?php endif; ?>
    </div>
</div>
