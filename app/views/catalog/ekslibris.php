<?php
/* @var $item \yii\easyii\modules\catalog\api\ItemObject */

use yii\widgets\Breadcrumbs;
use yii\bootstrap\ActiveForm;
use yii\widgets\MaskedInput;
use yii\bootstrap\Html;

$this->title = Html::encode($item->title);
$this->params['breadcrumbs'][] = ['label' => 'Образцы экслибрисов', 'url' => ['catalog/ekslibrisy']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <?=Breadcrumbs::widget([
        'homeLink' => ['label' => 'Фортекс', 'url' => '/'],
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []
    ]);?>
    <h2 class="text-center"><?=$this->title;?></h2>
    <div class="row auto-clear">
        <div class="col-sm-6 col-sm-push-6">
            <?=Html::img($item->thumb(null, 300), ['class' => 'img-responsive center-block']);?>
        </div>
        <div class="col-sm-6 col-sm-pull-6">
            <?php $form = ActiveForm::begin();?>
            
            <?=$form->field($model, 'name')
                ->textInput(['placeholder' => $model->getAttributeLabel('name')])
                ->label(false);?>
            
            <?=$form->field($model, 'phone')
                ->textInput()
                ->label(false)
                ->widget(MaskedInput::className(), [
                    'mask' => '+7 (999) 999-99-99',
                    'options' => [
                        'placeholder' => $model->getAttributeLabel('phone'),
                        'class' => 'form-control',
                    ]
                ]);?>
            
            <?=$form->field($model, 'email')
                ->textInput(['placeholder' => $model->getAttributeLabel('email')])
                ->label(false);?>
            
            <?=$form->field($model, 'file')->fileInput();?>
            
            <?=$form->field($model, 'message')
                ->textarea(['placeholder' => $model->getAttributeLabel('message')])
                ->label(false);?>
            
            <div class="text-center">
                <?=Html::submitButton('Отправить', ['class' => 'btn btn-danger']);?>
            </div>
            
            <?php ActiveForm::end();?>
        </div>
    </div>
</div>
