<?php
/* @var $cat \yii\easyii\modules\article\api\CategoryObject */
/* @var $child_cats \yii\easyii\modules\article\api\CategoryObject[] */

use yii\helpers\Url;
use yii\helpers\Html;

$request = Yii::$app->request;
$a = $request->get('a');
$cat_slug = $request->get('slug');
$this->title = Html::encode($cat->title);
?>
<div class="container">
    <h2 class="text-center"><?=$this->title;?></h2>
    <div class="row auto-clear">
        <?php foreach($child_cats as $subcat): ?>
            <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2">
                <a class="item" href="<?=Url::toRoute(['catalog/subcat', 'slug' => $subcat->slug, 'cat' => $cat_slug, 'a' => $a]);?>">
                    <div class="item-img-wrapper">
                        <?=Html::img($subcat->thumb(null, 400), ['alt' => $subcat->title]);?>
                    </div>
                    <div class="item-title"><?=$subcat->title;?></div>
                </a>
            </div>
        <?php endforeach; ?>
    </div>
</div>
