<?php
/** @var $cat \yii\easyii\modules\catalog\api\CategoryObject */
/** @var $col_lg int */

use yii\helpers\Url;

$col_lg = !empty($col_lg) ? $col_lg : '2';
$image_thumb = $cat->thumb(null, 250);
$image = $image_thumb ? $image_thumb : Yii::getAlias('@web') . '/noimage.jpg';
$url_data = isset($order_type) ? ['/order-type', 'category' => $cat->slug] : ['/catalog/cat', 'slug' => $cat->slug];

?>
<div class="col-xs-12 col-sm-6 col-md-3 col-lg-<?=$col_lg;?> margin-b-30">
    <a class="product" href="<?=Url::to($url_data);?>">
        <div class="title-product margin-b-20"><?=$cat->title;?></div>
        <div class="margin-b-20 img-product-big">
            <div class="wow zoomIn" data-wow-duration=".3" data-wow-delay=".1s">
                <img class="img-responsive" src="<?=$image;?>" alt="<?=$cat->title;?>">
            </div>
        </div>
        <div class="knopa online-order-mini">Выбрать</div>
    </a>
</div>