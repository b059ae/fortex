<?php
/* @var $subcat \yii\easyii\modules\catalog\api\CategoryObject */
/* @var $col_lg int */

use yii\helpers\Url;
use yii\easyii\modules\catalog\api\Catalog;
use app\models\Master;

$url_data = null;
if($subcat->slug == 'faximile'){
    $url_data = Master::getUrl($subcat->items[0]->model);
}
else{
    $cat = Catalog::cat($subcat->model->parent);
    $url_data = ['/catalog/subcat', 'cat' => $cat->slug, 'slug' => $subcat->slug];
}
$col_lg = !empty($col_lg) ? $col_lg : '2';
$image_thumb = $subcat->thumb(null, 250);
$image = $image_thumb ? $image_thumb : Yii::getAlias('@web') . '/noimage.jpg';

?>
<div class="col-xs-12 col-sm-6 col-md-3 col-lg-<?=$col_lg;?> margin-b-30">
    <a class="product"
        onclick="metrikaReachGoal('online.type'); return true;"
        href="<?=Url::to($url_data);?>">
        <div class="margin-b-20 img-product">
            <img class="img-responsive" src="<?=$image;?>" alt="<?=$subcat->title;?>">
        </div>
        <div class="title-product"><?=$subcat->title;?></div>
        <p>Цена от <?=app\helpers\CatalogHelper::getMinPrice($subcat);?> руб.</p>
    </a>
</div>