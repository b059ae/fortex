<?php
/* @var $item \yii\easyii\modules\catalog\api\ItemObject */

use yii\helpers\Url;

$col_lg = !empty($col_lg) ? $col_lg : 2;
$image_thumb = $item->thumb(null, 230);
$image = $image_thumb ? $image_thumb : Yii::getAlias('@web') . '/noimage.jpg';

?>
<div class="swiper-slide">
    <div class="block-hit text-center">
        <div class="img-hit">
            <a href="<?=Url::to(['site/add-to-cart', 'id' => $item->id]);?>">
                <img class="img-responsive" src="<?=$image;?>" alt="<?=$item->title;?>">
            </a>
        </div>
        <?php /*<div class="introtext">
            <div class="text">
                <?=$item->description;?>
            </div>
            <div>
                <a class="online-order-mini"
                   href="<?=Url::to(['site/add-to-cart', 'id' => $item->id]);?>">
                    Заказать
                </a>
            </div>
        </div> */ ?>
    </div>
</div>