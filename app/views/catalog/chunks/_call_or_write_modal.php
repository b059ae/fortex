<?php
use yii\bootstrap\Modal;

Modal::begin([
    'id' => 'call-or-write',
    'header' => '<h4 class="margin-b-0">Закажите в телефонном режиме или оставьте заявку</h4>',
    'size' => Modal::SIZE_DEFAULT,
]);
?>

<div class="row">
    <div class="col-xs-12">
        <div class="form-group">
            <label for="exampleInputEmail1">E-mail</label>
            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="E-mail">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Телефон</label>
            <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Телефон">
        </div>
        <div class="form-group">
            <label for="message">Сообщение</label>
            <textarea class="form-control" rows="3" id="message"></textarea>
        </div>
        
        <button class="btn-theme btn-theme-sm btn-base-bg text-uppercase" type="button">
            Отправить
        </button>
    </div>
</div>

<?php Modal::end(); ?>