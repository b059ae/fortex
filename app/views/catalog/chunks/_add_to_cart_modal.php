<?php
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

Modal::begin([
    'id' => 'add-to-cart-modal',
    'header' => '<h4 class="margin-b-0">Добавить в корзину</h4>',
    'size' => Modal::SIZE_LARGE,
]);

$form = ActiveForm::begin([
    'id' => 'add-to-cart-form'
]);
?>

<?=Html::activeHiddenInput($form_model, 'item_id');?>
<div class="row">
    <div class="col-xs-6">
        <h2 id="add-to-cart-modal_title" class="margin-b-20"></h2>
        <img id="add-to-cart-modal_image" class="img-thumbnail center-block">
    </div>
    <div class="col-xs-6">
        <div class="product-price margin-b-20">
            <span id="add-to-cart-modal_price"></span> руб.
        </div>
        <div class="product-description" id="add-to-cart-modal_description"></div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-12 col-lg-6 text-center">
        <div class="product_count">
            <div class="string_left product_count_minus"><i class="fa fa-minus" aria-hidden="true"></i></div>
            <?=Html::activeTextInput($form_model, 'count', ['minlength' => '1', 'class' => 'product_count_input']);?>
            <div class="string_right product_count_plus"><i class="fa fa-plus" aria-hidden="true"></i></div>
        </div>
    </div>
    <div class="col-md-12 col-lg-6 text-center">
        <?=Html::submitButton('Добавить в корзину', [
            'class' => 'btn-theme btn-theme-sm btn-base-bg text-uppercase',
            'data-loading-text' => 'Добавление...'
        ]);?>
    </div>
</div>

<?php

ActiveForm::end();
Modal::end();


$this->registerJs(<<<JS
    jQuery('#add-to-cart-form').on('beforeSubmit.yii', function() {
        var \$form = $(this);
        var data = \$form.serialize();
        jQuery.post(\$form.attr('action'), data, function(data){
            refreshShopcartInfo();
            jQuery('#add-to-cart-modal').modal('hide');
            notify(false, 'Товар добавлен в корзину!', 'success');
        });
        return false;
    });
JS
);

