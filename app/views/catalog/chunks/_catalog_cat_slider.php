<?php
/** @var $cat \yii\easyii\modules\catalog\api\CategoryObject */
/** @var $image_width int */
/** @var $height int */

use yii\helpers\Url;

$col_lg = !empty($col_lg) ? $col_lg : '2';
$image_thumb = $cat->thumb(null, 250);
$image = $image_thumb ? $image_thumb : Yii::getAlias('@web') . '/noimage.jpg';


?>
<div class="swiper-slide other-service">
    <a class="product <?=(!empty($height) ? 'height-' . $height : '');?>"
       href="<?=Url::to(['/order-type', 'category' => $cat->slug]);?>">
        <div class="margin-b-20 img-product-big">
            <div class="wow zoomIn" data-wow-duration=".3" data-wow-delay=".1s">
                <img class="img-responsive" src="<?=$image;?>" alt="<?=$cat->title;?>">
            </div>
        </div>
        <div class="title-product"><?=$cat->title;?></div>
    </a>
</div>