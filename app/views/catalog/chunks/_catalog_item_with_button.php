<?php
/* @var $item \yii\easyii\modules\catalog\api\ItemObject */
/* @var $col_lg int */
/* @var $image_width int */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\easyii\modules\catalog\api\Catalog;

$url_data = ['/master/index', 'slug' => $item->slug, 'immediately' => '1'];

$modal = $item->cat->id == 4;

if(!empty($item->cat->model->parent)){
    $cat = Catalog::cat($item->cat->model->parent);
    $url_data['cat'] = $cat->slug;
    $url_data['subcat'] = $item->cat->slug;
    if($url_data['cat'] == 'pecati' && $url_data['subcat'] == 'gerbovye'){
        $modal_2 = true;
    }
    elseif($url_data['cat'] == 'osnastki'){
        $modal = true;
    }
}
else{
    $url_data['cat'] = $item->cat->slug;
}


$col_lg = !empty($col_lg) ? $col_lg : '2';
$image_thumb = $item->thumb(null, 165);
$image = $image_thumb ? $image_thumb : Yii::getAlias('@web') . '/noimage.jpg';

$big_image_thumb = $item->thumb(null, 300);
$big_image = $big_image_thumb ? $big_image_thumb : Yii::getAlias('@web') . '/noimage.jpg';

?>
<div class="col-xs-12 col-sm-6 col-md-4 col-lg-<?=$col_lg;?> margin-b-30">
    <a class="product"
       onclick="metrikaReachGoal('online.sample'); return true;"
       <?php if($modal): ?>
            data-id="<?=$item->id;?>"
            data-title="<?=Html::encode($item->title);?>"
            data-price="<?=$item->price;?>"
            data-description="<?=Html::encode($item->description);?>"
            data-image="<?=$big_image;?>"
            data-toggle="modal"
            href="/#add-to-cart-modal"
        <?php elseif(isset($modal_2)): ?>
            data-toggle="modal"
            href="/#call-or-write"
        <?php else: ?> 
            href="<?=Url::to($url_data);?>"
        <?php endif; ?>>
        <div class="margin-b-20 img-product">
            <div class="wow zoomIn" data-wow-duration=".3" data-wow-delay=".1s">
                <img class="img-responsive" src="<?=$image;?>" alt="<?=$item->title;?>">
            </div>
        </div>
        <div class="title-product"><?=$item->title;?></div>
        <p>Цена: <?=$item->price;?> руб.</p>
        <div class="knopa online-order-mini">Выбрать</div>
    </a>
</div>