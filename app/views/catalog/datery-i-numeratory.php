<?php
/* @var $cat \yii\easyii\modules\catalog\api\CategoryObject */
/* @var $subcats \yii\easyii\modules\catalog\api\CategoryObject[] */

use yii\widgets\Breadcrumbs;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\easyii\helpers\Image;

$this->title = Html::encode($cat->title);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <?=Breadcrumbs::widget([
        'homeLink' => ['label' => 'Фортекс', 'url' => '/'],
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []
    ]);?>
    <h2 class="text-center"><?=$this->title;?></h2>
    
    <div class="row auto-clear">
    <?php foreach($subcats as $subcat): ?>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-5ths">
            <a class="item" href="<?=Url::toRoute(['catalog/dn', 'slug' => $subcat->slug]);?>">
                <div class="item-img-wrapper">
                    <?=Html::img($subcat->thumb(null, 350), ['alt' => $subcat->title]);?>
                </div>
                <div class="item-title"><?=$subcat->title;?></div>
            </a>
        </div>
    <?php endforeach; ?>
    <?php foreach($items as $item): ?>
        <?php
            if($item->slug === 'smennye-poduski'){
                $url_data = ['/smennye-poduski'];
            }
            else{
                $url_data = ['site/buy', 'id' => $item->id];
            }
        ?>
        
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-5ths">
            <a class="item" href="<?=Url::toRoute($url_data);?>">
                <div class="item-img-wrapper">
                    <?=Html::img(!empty($item->data->image) ?
                        Image::thumb('../uploads/' . $item->data->image, null, 350)
                            : $item->thumb(null, 350), ['alt' => $item->title]);?>
                </div>
                <div class="item-title"><?=$item->title;?></div>
            </a>
        </div>
    <?php endforeach; ?>
    </div>
</div>
