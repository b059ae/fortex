<?php
/* @var $tool_cats \yii\easyii\modules\article\api\CategoryObject[] */

use yii\widgets\Breadcrumbs;

$this->title = 'Варианты оснасток';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <?=Breadcrumbs::widget([
        'homeLink' => ['label' => 'Фортекс', 'url' => '/'],
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []
    ]);?>
    <h2 class="text-center"><?=$this->title;?></h2>
    <div class="row auto-clear">
        <?php $i = 0; ?>
        <?php foreach($tool_cats as $tool_cat): ?>
            <?=$this->render('//layouts/chunks/_tool_cat', ['cat' => $tool_cat, 'variant' => $i + 1]);?>
            <?php $i++; ?>
        <?php endforeach; ?>
    </div>
</div>
