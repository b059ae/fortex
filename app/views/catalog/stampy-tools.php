<?php
/* @var $cat \yii\easyii\modules\catalog\api\CategoryObject */
/* @var $items \yii\easyii\modules\catalog\api\ItemObject[] */

use yii\widgets\Breadcrumbs;
use yii\bootstrap\Html;
use yii\helpers\Url;

$this->title = 'Образцы оснасток';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <?=Breadcrumbs::widget([
        'homeLink' => ['label' => 'Фортекс', 'url' => '/'],
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []
    ]);?>
    <h2 class="text-center"><?=$this->title;?></h2>
    
    <div class="row auto-clear">
        <?php foreach($items as $item): ?>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                <a class="item" href="<?=Url::toRoute(['site/buy', 'id' => $item->id]);?>">
                    <div class="item-img-wrapper">
                        <?=Html::img($item->thumb(null, 350), ['alt' => $item->title]);?>
                    </div>
                    <div class="item-title"><?=$item->title;?></div>
                </a>
            </div>
        <?php endforeach; ?>
    </div>


    <?php $pagination = $cat->getPages(); ?>
    <?php if(!empty($pagination)): ?>
        <div class="clearfix margin-b-10"></div>
        <div class="col-xs-12 text-center">
            <?=$pagination;?>
        </div>
    <?php endif; ?>
</div>
