<?php
/* @var $cat \yii\easyii\modules\catalog\api\CategoryObject */

use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\bootstrap\ActiveForm;
use yii\widgets\MaskedInput;
use yii\bootstrap\Html;

$asset = AppAsset::register($this);
$this->title = Html::encode($cat->title);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <?=Breadcrumbs::widget([
        'homeLink' => ['label' => 'Фортекс', 'url' => '/'],
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []
    ]);?>
    <h2 class="text-center"><?=$this->title;?></h2>
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 margin-b-0">
            <div class="complex-img-container">
                <?=Html::img($asset->baseUrl . '/img/' . $cat->slug . '/1.jpg', ['class' => 'complex-img']);?>
                <?=Html::img($asset->baseUrl . '/img/' . $cat->slug . '/3.jpg', ['class' => 'complex-img-arrow']);?>
                <?=Html::img($asset->baseUrl . '/img/' . $cat->slug . '/2.jpg', ['class' => 'complex-img']);?>
            </div>
            
            <div class="row">
                <div class="col-md-8 col-md-offset-2 margin-b-0">
                    <?php $form = ActiveForm::begin(); ?>

                    <?=$form->field($model, 'name')
                        ->textInput(['placeholder' => $model->getAttributeLabel('name')])->label(false);?>
                    
                    <?=$form->field($model, 'phone')
                        ->label(false)
                        ->widget(MaskedInput::className(), [
                            'mask' => '+7(999) 999-99-99',
                            'options' => [
                                'placeholder' => $model->getAttributeLabel('phone'),
                                'class' => 'form-control'
                            ],
                        ]);?>
                    
                    <?=$form->field($model, 'file')->fileInput();?>
                    
                    <?=$form->field($model, 'message')
                        ->textarea(['placeholder' => $model->getAttributeLabel('message')])->label(false);?>
                    
                    <div class="text-center">
                        <?=Html::submitButton('Отправить', ['class' => 'btn btn-danger']);?>
                    </div>
                    
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
