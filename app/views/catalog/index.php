<?php
/* @var $cats \yii\easyii\modules\catalog\api\CategoryObject[] */

$this->title = 'Каталог';
?>
<div class="container">
    <h2 class="text-center"><?=$this->title;?></h2>
    <div class="row auto-clear">
        <?php foreach($cats as $cat): ?>
            <?=$this->render('//layouts/chunks/_cat', ['cat' => $cat]);?>
        <?php endforeach; ?>
    </div>
</div>