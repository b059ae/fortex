<?php

use yii\widgets\Breadcrumbs;
use yii\bootstrap\Html;

$this->title = Html::encode($cat->title);
$this->params['breadcrumbs'][] = $this->title;

$grm_items = $grm->getItems([
    'orderBy' => ['id' => SORT_ASC],
    'pagination' => ['defaultPageSize' => 40],
]);
$colop_items = $colop->getItems([
    'orderBy' => ['id' => SORT_ASC],
    'pagination' => ['defaultPageSize' => 40],
]);
$trodat_items = $trodat->getItems([
    'orderBy' => ['id' => SORT_ASC],
    'pagination' => ['defaultPageSize' => 40],
]);

?>
<div class="container">
    <?=Breadcrumbs::widget([
        'homeLink' => ['label' => 'Фортекс', 'url' => '/'],
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []
    ]);?>
    <h2 class="text-center"><?=$this->title;?></h2>
    <?=Html::beginForm(['/smennye-poduski'], 'post', ['id' => 'cat-form']);?>
    <div class="row auto-clear row-one">
        <div class="col-xs-12 col-sm-6 col-md-2 col-lg-2">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h6>Подушки GRM</h6>
            </div>
            <?php foreach($grm_items as $key => $item): ?>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <?=Html::radioList('id', null, [$item->id => $item->title]);?>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <h6>Оснастки COLOP</h6>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <h6>Подушки COLOP</h6>
            </div>
            <?php foreach($colop_items as $key => $item): ?>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <div class="radio"><?=$item->tool;?></div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <?=Html::radioList('id', null, [$item->id => $item->title]); ?>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <h6>Оснастки TRODAT</h6>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <h6>Подушки TRODAT</h6>
            </div>
            <?php foreach($trodat_items as $key => $item): ?>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <div class="radio"><?=$item->tool;?></div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <?=Html::radioList('id', null, [$item->id => $item->title]); ?>
                </div>
            <?php endforeach; ?>
        </div>
        
        <div class="clearfix margin-b-20"></div>
        <div class="col-xs-12 margin-b-0" id="warning"></div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
            <?=Html::submitButton('Выбрать', [
                'name' => 'form',
                'value' => '1',
                'class' => 'btn btn-danger btn-lg'
            ]);?>
        </div>
    </div>
    <?=Html::endForm();?>
</div>
<?php
$this->registerJs(<<<JS
    jQuery('#cat-form').on('submit', function(){
        if(!jQuery('[name="id"]:checked').length){
            jQuery('#warning').html(
                '<div class="panel panel-danger text-center"><div class="panel-body">Выберите позицию!</div></div>'
            );
            return false;
        }
        jQuery('#warning').html('');
        return true;
    });
JS
);
?>