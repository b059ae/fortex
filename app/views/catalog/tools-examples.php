<?php
/* @var $category \yii\easyii\modules\catalog\api\CategoryObject */

use yii\widgets\Breadcrumbs;

$this->title = 'Образцы оснасток';
$this->params['breadcrumbs'][] = ['label' => 'Варианты', 'url' => '/catalog/tools'];
$this->params['breadcrumbs'][] = $this->title;
$items = $category->getItems(['orderBy' => ['id' => SORT_ASC]]);

?>
<div class="container">
    <?=Breadcrumbs::widget([
        'homeLink' => ['label' => 'Фортекс', 'url' => '/'],
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []
    ]);?>
    <h2 class="text-center"><?=$this->title;?></h2>
    <div class="row auto-clear">
        <?php if($cat_config['show_wrapper']): ?>
        <div class="col-md-10 col-md-offset-1">
            <div class="row">
        <?php endif; ?>
                
        <?php foreach($items as $item): ?>
            <div class="<?=isset($cat_config['col_class']) ? $cat_config['col_class'] : 'col-xs-6 col-sm-4 col-md-5ths';?>">
                <?=$this->render('//layouts/chunks/_tool_item', ['item' => $item]);?>
            </div>
        <?php endforeach; ?>
        
        <?php if($cat_config['show_wrapper']): ?>
            </div>
        </div>
        <?php endif; ?>

        <?php $pagination = $category->getPages(); ?>
        <?php if(!empty($pagination)): ?>
            <div class="clearfix margin-b-40"></div>
            <div class="col-xs-12 text-center">
                <?=$pagination;?>
            </div>
        <?php endif; ?>
    </div>
</div>