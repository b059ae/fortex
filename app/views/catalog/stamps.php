<?php
/* @var $cat \yii\easyii\modules\catalog\api\CategoryObject */
/* @var $stampy \yii\easyii\modules\catalog\api\CategoryObject */
/* @var $by_scetch \yii\easyii\modules\catalog\api\CategoryObject */
/* @var $faximile \yii\easyii\modules\catalog\api\CategoryObject */
/* @var $ekslibrisy \yii\easyii\modules\catalog\api\CategoryObject */

use yii\widgets\Breadcrumbs;
use yii\bootstrap\Html;
use yii\helpers\Url;

$this->title = Html::encode($cat->title);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <?=Breadcrumbs::widget([
        'homeLink' => ['label' => 'Фортекс', 'url' => '/'],
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []
    ]);?>
    <h2 class="text-center"><?=$this->title;?></h2>
    <div class="row auto-clear">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
            <a class="item" href="<?=Url::toRoute(['catalog/cat', 'slug' => $by_scetch->slug]);?>">
                <div class="item-img-wrapper">
                    <?=Html::img($by_scetch->thumb(null, 400), ['alt' => $by_scetch->title]);?>
                </div>
                <div class="item-title"><?=$by_scetch->title;?></div>
            </a>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
            <a class="item" href="<?=Url::toRoute(['catalog/stampy']);?>">
                <div class="item-img-wrapper">
                    <?=Html::img($stampy->thumb(null, 400), ['alt' => $stampy->title]);?>
                </div>
                <div class="item-title"><?=$stampy->title;?></div>
            </a>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
            <a class="item" href="<?=Url::toRoute(['catalog/cat', 'slug' => $faximile->slug]);?>">
                <div class="item-img-wrapper">
                    <?=Html::img($faximile->thumb(null, 400), ['alt' => $faximile->title]);?>
                </div>
                <div class="item-title"><?=$faximile->title;?></div>
            </a>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
            <a class="item" href="<?=Url::toRoute(['catalog/ekslibrisy']);?>">
                <div class="item-img-wrapper">
                    <?=Html::img($ekslibrisy->thumb(null, 400), ['alt' => $ekslibrisy->title]);?>
                </div>
                <div class="item-title"><?=$ekslibrisy->title;?></div>
            </a>
        </div>
    </div>
</div>
