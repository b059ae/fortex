<?php
/* @var $subcat \yii\easyii\modules\catalog\api\CategoryObject */
/* @var $items \yii\easyii\modules\catalog\api\ItemObject[] */

use yii\widgets\Breadcrumbs;
use app\helpers\CatalogHelper;
use yii\bootstrap\Html;

$this->title = Html::encode($subcat->title);
$parent = CatalogHelper::getParentCat($subcat);
$this->params['breadcrumbs'][] = ['label' => 'Варианты', 'url' => ['catalog/cat', 'slug' => $parent->slug]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <?=Breadcrumbs::widget([
        'homeLink' => ['label' => 'Фортекс', 'url' => '/'],
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []
    ]);?>
    <h2 class="text-center"><?=$this->title;?></h2>
    <div class="row auto-clear">
        <?php foreach($items as $item): ?>
            <div class="col-xs-6 col-sm-4 col-md-5ths">
                <?=$this->render('//layouts/chunks/_item',
                    ['item' => $item]);?>
            </div>
        <?php endforeach; ?>

        <?php $pagination = $subcat->getPages(); ?>
        <?php if(!empty($pagination)): ?>
            <div class="clearfix margin-b-10"></div>
            <div class="col-xs-12 text-center">
                <?=$pagination;?>
            </div>
        <?php endif; ?>
    </div>
</div>
