<?php

return [
    'rules' => [
        'buy' => 'site/buy',
        'choose' => 'site/choose',
        
        'pecati' => 'site/index',
        'ip' => 'site/index',
        'ooo' => 'site/index',
        'doctor' => 'site/index',
        'pecat-s-osnastkoy' => 'site/index',
        'karmannaya' => 'site/index',
        'automaticheskaya' => 'site/index',
        'ruchnaya' => 'site/index',
        'metallicheskaya' => 'site/index',
        'pecati-i-stampy' => 'site/index',
        
        
        
        'success' => 'site/success',
        'remove-good' => 'site/remove-good',
        'shopcart' => 'site/shopcart',
        'partners' => 'site/partners',
        'send-message' => 'site/send-message',
        'shopcart-info' => 'site/shopcart-info',
        'add-to-cart' => 'site/add-to-cart',
        'download' => 'site/download',
        'payment/<action:[\w-]+>' => 'site/payment',

        
        'smennye-poduski' => 'catalog/smennye-poduski',
        'catalog/datery-i-numeratory/smennye-poduski' => 'catalog/smennye-poduski',
        
        'catalog/stamps' => 'catalog/stamps',
        'catalog/stampy' => 'catalog/stampy',
        
        'catalog/by-scetch' => 'catalog/by-scetch',
        'catalog/faximile' => 'catalog/by-scetch',
        'catalog/plombiry-i-plombiratory' => 'catalog/plombiry-i-plombiratory',
        'catalog/stampy-tools' => 'catalog/stampy-tools',
        'catalog/ekslibrisy' => 'catalog/ekslibrisy',
        'catalog/ekslibrisy/<slug:[\w-]+>' => 'catalog/ekslibris',
        
        'catalog/tools' => 'catalog/tools',
        'catalog/tools/<slug:[\w-]+>' => 'catalog/tools-examples',
        
        'catalog/datery-i-numeratory' => 'catalog/datery-i-numeratory',
        'catalog/datery-i-numeratory/<slug:[\w-]+>' => 'catalog/dn',
        
        'catalog/<slug:[\w-]+>' => 'catalog/cat',
        'catalog/<cat:[\w-]+>/<slug:[\w-]+>' => 'catalog/subcat',
        
        'call' => 'site/call',
        
        '<controller:\w+>/view/<slug:[\w-]+>' => '<controller>/view',
        '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
        '<controller:\w+>/cat/<slug:[\w-]+>' => '<controller>/cat',
    ],
];