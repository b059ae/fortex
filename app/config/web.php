<?php

$params = require(__DIR__ . '/params.php');

$basePath =  dirname(__DIR__);
$webroot = dirname($basePath);

Yii::setAlias('@uploads', $webroot .DIRECTORY_SEPARATOR.'web'. DIRECTORY_SEPARATOR . 'uploads');

$config = [
    'id' => 'app',
    'name' => 'ФОРТЕКС',
    'basePath' => $basePath,
    'bootstrap' => ['log'],
    'language' => 'ru-RU',
    'runtimePath' => $webroot . '/runtime',
    'vendorPath' => $webroot . '/vendor',
    'components' => [
        'html2pdf' => [
            'class' => 'yii2tech\html2pdf\Manager',
            'viewPath' => '@app/pdf',
            'converter' => [
                'class' => 'yii2tech\html2pdf\converters\Dompdf',
                'defaultOptions' => [
                    'pageSize' => 'A4',
                ],
            ],
        ],
        'customer' => [// Компонент для авторизации покупателей на сайте. Не пересекается с админской учетной записью
            'class' => \yii\web\User::class,
            'identityClass' => \app\models\Customers::class,
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_c_identity', 'httpOnly' => true],
            'idParam' => '__c_id',
            'authTimeoutParam' => '__c_expire',
            'absoluteAuthTimeoutParam' => '__c_absoluteExpire',
            'returnUrlParam' => '__c_return_Url',
        ],
        'amocrm' => [
            'class' => 'yii\amocrm\Client',
            'subdomain' => 'fortexrostov',
            'login' => 'fortex.rnd@yandex.ru',
            'hash' => '585e4630161b2d1fd711f29eb9f12b4a',
            'fields' => [
                'pipeline_id' => 887266,
                'lead_custom_fields' => [
                    'order_id' => 155899,
                    'product_title' => 428899,
                    'urgency' => 392777,
                    'delivery' => 155705,
                    'message' => 394667,
                    'config_info' => 394667,
                    'config_protection' => 428913,
                    'config_font' => 428915,
                    'config_color' => 428917,
                    'config_brand' => 428919,
                    'config_center_text' => 428911,
                    'config_engraving' => 428925,
                ],
                'contact_custom_fields' => [
                    'city' => 392779,
                    'address' => 392783,
                    'phone' => 143535,
                    'email' => 143537,
                    'inn' => 428905,
                    'ogrn' => 428907,
                    'company' => 428891,
                ],
            ],
        ],
        'beeline' => [
            'class' => \app\components\Beeline::class,
            //'user' => 'MPBX_g_91381_cc_93047',
            'user' => 'MPBX_g_91381_cc_118711',
            'manager_numbers' => [
                '1711954' => '202', //Чернятина
                '1711951' => '203', //Козинцева
            ],
            'token' => '216da91d-57f2-48a2-812c-45f4840e4347',
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '9NZYyRqkEJVjOGD0oOhWWRLaTX6-0amO',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'htmlLayout' => false,
            'textLayout' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.yandex.ru',
                'username' => 'sales@fortex-rostov.ru',
                'password' => 'pass4fortex2',
                'port' => '465',
                'encryption' => 'ssl',
            ]
        ],
        'mailerb' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport'=>false,
            'htmlLayout' => false,
            'textLayout' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.yandex.com',
                'port' => '465',
                'encryption' => 'ssl',
            ]
        ],
        'urlManager' => require(__DIR__ . '/urlManager.php'),
        'formatter' => [ // Компонент для форматирования чисел, дат, строк
            'class' => \app\components\Formatter::class,
            'dateFormat' => 'php:d.m.Y',
            'timeFormat' => 'php:H:i',
            'datetimeFormat' => 'php:d.m.Y H:i',
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
            'currencyCode' => 'RUB',
        ],
        'assetManager' => [
            // uncomment the following line if you want to auto update your assets (unix hosting only)
            //'linkAssets' => true,
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js' => [YII_DEBUG ? 'jquery.js' : 'jquery.min.js'],
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [YII_DEBUG ? 'css/bootstrap.css' : 'css/bootstrap.min.css'],
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'js' => [YII_DEBUG ? 'js/bootstrap.js' : 'js/bootstrap.min.js'],
                ],
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
    
    $config['components']['db']['enableSchemaCache'] = false;
}

return array_merge_recursive($config, require($webroot . '/vendor/noumo/easyii/config/easyii.php'));