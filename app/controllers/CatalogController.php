<?php

namespace app\controllers;

use Yii;
use yii\easyii\modules\catalog\api\Catalog;
use yii\easyii\modules\catalog\models\Category;
use yii\easyii\modules\catalog\api\CategoryObject;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use app\helpers\CatalogHelper;


class CatalogController extends \yii\web\Controller
{
    public function actionIndex(){
        $cat_objects = Category::find()
            ->where(['id' => [1,2,3,4,5]])
            ->orderBy(['order_num' => SORT_ASC])
            ->all();
        $cats = ArrayHelper::map($cat_objects, 'id', function($cat){
            return new CategoryObject($cat);
        });
        
        
        return $this->render('index', [
            'cats' => $cats,
        ]);
    }
    
    
    public function actionCat($slug){
        $cat = Catalog::cat($slug);
        if(!$cat){
            throw new NotFoundHttpException('Категория не найдена.');
        }
        $child_cats = array_filter($cat->getChildren(), function($cat){
            return $cat->id != '26';
        });
        
        $items = null;
        if(!count($child_cats)){
            $items = $cat->getItems([
                'pagination' => ['defaultPageSize' => 16],
                'orderBy' => ['id' => SORT_ASC],
            ]);
        }
        
        $osnastki = null;
        if($slug == 'pecati'){
            $osnastki_objects = Category::find()
                ->where(['id' => [22,23,24,25]])
                ->orderBy(['order_num' => SORT_ASC])
                ->all();
            $osnastki = ArrayHelper::map($osnastki_objects, 'id', function($cat){
                $cat->parent = 3;
                return new CategoryObject($cat);
            });
        }
        
        // ВСЕ КАТЕГОРИИ (ДЛЯ МЕНЮ)
        $all_cats_objects = Category::find()
            ->where(['id' => [1,2,3,4,5]])
            ->select(['id', 'slug', 'title'])
            ->orderBy(['order_num' => SORT_ASC])
            ->all();
        $all_cats = ArrayHelper::map($all_cats_objects, 'id', function($cat){
            return new CategoryObject($cat);
        });
        
        $form_model = new \app\models\forms\CatalogForm();
        if(Yii::$app->request->isAjax){
            if($form_model->load(Yii::$app->request->post()) && $form_model->validate()){
                $form_model->save();
                return $this->asJson($form_model->toArray());
            }
        }
        
        return $this->render('cat', [
            'cat' => $cat,
            'child_cats' => $child_cats,
            'osnastki' => $osnastki,
            'all_cats' => $all_cats,
            'items' => $items,
            'form_model' => $form_model,
        ]);
    }
    
    
    
    
    public function actionSubcat($slug){
        $subcat = Catalog::cat($slug);
        $items = $subcat->getItems([
            'pagination' => ['defaultPageSize' => 10],
            'orderBy' => ['id' => SORT_ASC],
        ]);
        
        $form_model = new \app\models\forms\CatalogForm();
        if(Yii::$app->request->isAjax){
            if($form_model->load(Yii::$app->request->post()) && $form_model->validate()){
                $form_model->save();
                return $this->asJson($form_model->toArray());
            }
        }
        
        return $this->render('subcat', [
            'subcat' => $subcat,
            'items' => $items,
            'form_model' => $form_model,
        ]);
    }
    
    
    public function actionTools(){
        return $this->render('tools', [
            'tool_cats' => CatalogHelper::getToolCats(),
        ]);
    }
    
    public function actionToolsExamples($slug, $a = null){
        if(CatalogHelper::getToolCatConfig($slug, 'hidden') && $a !== null){
            CatalogHelper::buyHiddenItem($slug);
            if($a === 'buy'){
                $this->redirect(['/shopcart']);
            }
            else{
                $this->redirect(['/catalog/' . $a . '?a=buy']);
            }
        }
        $category = Catalog::cat($slug);
        return $this->render('tools-examples', [
            'category' => $category,
            'cat_config' => CatalogHelper::getToolCatConfig($slug),
        ]);
    }
    
    public function actionStamps(){
        return $this->render('stamps', [
            'cat' => Catalog::cat('stamps'),
            'by_scetch' => Catalog::cat('by-scetch'),
            'stampy' => Catalog::cat('stampy'),
            'faximile' => Catalog::cat('faximile'),
            'ekslibrisy' => Catalog::cat('ekslibrisy'),
        ]);
    }
    
    public function actionByScetch(){
        $path_info = explode('/', Yii::$app->request->getPathInfo());
        $slug = $path_info[count($path_info) - 1];
        $cat = Catalog::cat($slug);
        $model = new \app\models\ByScetchForm(['form_title' => $cat->title]);
        if($model->load(Yii::$app->request->post()) && $model->action()){
            return $this->redirect(['/success']);
        }
        return $this->render('by-scetch', [
            'cat' => $cat,
            'model' => $model,
        ]);
    }
    
    public function actionPlombiryIPlombiratory(){
        $cat = Catalog::cat('plombiry-i-plombiratory');
        $items = $cat->getItems([
            'pagination' => ['defaultPageSize' => 10],
            'orderBy' => ['id' => SORT_ASC],
        ]);
        return $this->render('plombiry-i-plombiratory', [
            'cat' => $cat,
            'items' => $items,
        ]);
    }
    
    public function actionStampyTools(){
        $cat = Catalog::cat('stampy-tools');
        $items = $cat->getItems([
            'pagination' => ['defaultPageSize' => 10],
            'orderBy' => ['id' => SORT_ASC],
        ]);
        return $this->render('stampy-tools', [
            'cat' => $cat,
            'items' => $items,
        ]);
    }
    
    public function actionEkslibrisy(){
        $cat = Catalog::cat('ekslibrisy');
        $items = $cat->getItems([
            'pagination' => ['defaultPageSize' => 10],
            'orderBy' => ['id' => SORT_ASC],
        ]);
        return $this->render('ekslibrisy', [
            'cat' => $cat,
            'items' => $items,
        ]);
    }
    
    public function actionEkslibris($slug){
        $item = Catalog::get($slug);
        $model = new \app\models\EkslibrisForm([
            'form_title' => 'Экслибрис (' . $item->title . ')'
        ]);
        if($model->load(Yii::$app->request->post()) && $model->action()){
            return $this->redirect(['/success']);
        }
        return $this->render('ekslibris', [
            'item' => $item,
            'model' => $model,
        ]);
    }
    
    
    public function actionStampy(){
        $cat = Catalog::cat('stampy');
        $items = $cat->getItems([
            'pagination' => ['defaultPageSize' => 12],
            'orderBy' => ['id' => SORT_ASC],
        ]);
        return $this->render('stampy', [
            'cat' => $cat,
            'items' => $items,
        ]);
    }
    public function actionDateryINumeratory(){
        $categories = Category::find()->where(['in', 'slug', [
            'stampy-samonabornye',
            'pecati-samonabornye',
            'datery',
            'numeratory',
            'datery-so-svobodnym-polem',
            'avtomaticeskie-numeratory',
            'samonabornyj-dater',
            'smennye-poduski',
        ]])->orderBy(['id' => SORT_ASC])->all();
        $subcats = array_map(function($category){
            return new CategoryObject($category);
        }, $categories);
        $different = Catalog::cat('datery-different');
        return $this->render('datery-i-numeratory', [
            'cat' => Catalog::cat('datery-i-numeratory'),
            'items' => $different->getItems(['orderBy' => ['id' => SORT_ASC]]),
            'subcats' => $subcats,
        ]);
    }
    public function actionDn($slug){
        $cat = Catalog::cat($slug);
        $items = $cat->getItems([
            'pagination' => ['defaultPageSize' => 4],
            'orderBy' => ['id' => SORT_ASC],
        ]);
        return $this->render('dn', [
            'cat' => $cat,
            'items' => $items,
        ]);
    }
    
    public function actionSmennyePoduski(){
        $request = Yii::$app->request;
        if(!empty($request->post('form')) && !empty($request->post('id'))){
            return $this->redirect(['site/buy', 'id' => $request->post('id')]);
        }
        return $this->render('smennye-poduski', [
            'cat' => Catalog::cat('smennye-poduski'),
            'grm' => Catalog::cat('sp-grm'),
            'colop' => Catalog::cat('sp-colop'),
            'trodat' => Catalog::cat('sp-trodat'),
        ]);
    }
}
