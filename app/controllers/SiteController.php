<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\easyii\modules\catalog\api\Catalog;
use yii\easyii\modules\shopcart\api\Shopcart;
use yii\easyii\modules\catalog\models\Category;
use yii\easyii\modules\catalog\api\CategoryObject;
use yii\easyii\models\Setting;
use yii\easyii\modules\shopcart\models\Order;
use app\helpers\CatalogHelper;


class SiteController extends Controller{
    
    public function beforeAction($action){
        if(in_array($action->id, ['payment'])) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }
    
    public function actions(){
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Домашняя страница сайта
     * 
     * @return \yii\web\Response
     */
    public function actionIndex(){
        $callback_model = new \app\models\forms\CallbackForm();
        $uslugi = \yii\easyii\modules\gallery\api\Gallery::cat(8)->photos;
        $comments = \yii\easyii\modules\carousel\api\Carousel::items();
        $partners = \yii\easyii\modules\gallery\api\Gallery::cat(7)->getPhotos([
            'pagination' => [
                'defaultPageSize' => '10',
            ]
        ]);
        
        return $this->render('index', [
            'cats' => CatalogHelper::getMainCats(),
            'pechati' => CatalogHelper::getPecatiCats(),
            'tool_cats' => CatalogHelper::getToolCats(),
            'by_scetch' => Catalog::cat('by-scetch'),
            'stampy' => Catalog::cat('stampy'),
            'faximile' => Catalog::cat('faximile'),
            'ekslibrisy' => Catalog::cat('ekslibrisy'),
            'uslugi' => $uslugi,
            'comments' => $comments,
            'partners' => $partners,
            'callback_model' => $callback_model,
        ]);
    }
    
    
    /**
     * Страница "Партнеры"
     * 
     * @return \yii\web\Response
     */
    public function actionPartners(){
        $partners = \yii\easyii\modules\gallery\api\Gallery::cat(7)->photos;
        
        return $this->render('partners', [
            'partners' => $partners,
        ]);
    }
    
    
    /**
     * Выбор типа заказа
     * 
     * @return \yii\web\Response
     */
    public function actionOrderType($category){
        if(empty($category)){
            throw new \yii\web\NotFoundHttpException('Страница не найдена');
        }
        return $this->render('order-type', [
            'form_model' => new \app\models\forms\CallbackForm(),
            'category' => $category,
        ]);
    }

    
    /**
     * Выполнение звонка
     * @return \yii\web\Response
     */
    public function actionCall($r = null){
        $model = new \app\models\forms\CallbackForm();
        if($model->load(Yii::$app->request->post()) && $model->action()){
            if($r == '1'){
                CatalogHelper::removeActiveGoods();
            }
            return $this->redirect(['/success']);
        }
        return $this->goBack();
    }
    
    
    /**
     * Отображение корзины | отправка заказа в amoCRM и в админку
     * 
     * @return string|\yii\web\Response
     */
    public function actionShopcart(){
        $shopcart_model = new \app\models\Shopcart();
        if(
            $shopcart_model->load(Yii::$app->request->post())
            && $redirect = $shopcart_model->action()
        ){
            return $redirect;
        }
        $urgency_info = array_map(function($price){
            return ['data-urgency_price' => $price];
        }, CatalogHelper::getUrgencyInfo());
        
        return $this->render('shopcart', [
            'shopcart_model' => $shopcart_model,
            'order_info' => CatalogHelper::getOrderInfo(),
            'urgency_info' => $urgency_info,
            'delivery_price' => CatalogHelper::getDeliveryPrice(),
            'min_price_free_delivery' => Setting::get('min_price_free_delivery'),
        ]);
    }
    
    
    /**
     * Удаление позиции из корзины
     * 
     * @return \yii\web\Response
     */
    public function actionRemoveGood(){
        $id = Yii::$app->request->post('id');
        if(!$id){
            $id = 0;
        }
        return $this->asJson(Shopcart::remove($id));
    }

    
    /**
     * Вывод сообщения пользователю
     *
     * @return string|\yii\web\Response
     */
    public function actionSuccess(){
        $title = Yii::$app->session->getFlash('title');
        $message = Yii::$app->session->getFlash('message');
        $download = Yii::$app->session->getFlash('download');
        
        if(empty($message)){
            return $this->redirect(['/']);
        }
        
        return $this->render('message', [
            'title' => $title,
            'message' => $message,
            'download' => $download,
        ]);
    }
    
    
    /**
     * Отправка заявки с нижней формы сайта в amoCRM с помощью AJAX
     * 
     * @return \yii\web\Response|void
     */
    public function actionSendMessage(){
        if(Yii::$app->request->isAjax){
            $model = new \app\models\forms\MainContactForm();
            if($model->load(Yii::$app->request->post()) && $model->action()) {
                return $this->asJson(['success' => true]);
            }
            $result = [];
            foreach ($model->getErrors() as $attribute => $errors) {
                $result[\yii\helpers\Html::getInputId($model, $attribute)] = $errors;
            }
            return $this->asJson(['validation' => $result]);
        }
    }
    
    
    /**
     * Вывод информации о текущем состоянии корзины с помощью AJAX
     * 
     * @return \yii\web\Response|void
     */
    public function actionShopcartInfo(){
        $order = Order::current();
        if(Yii::$app->request->isAjax){
            return $this->asJson([
                'count' => $order->getGoodsCount(),
                'cost' => $order->getCost(),
            ]);
        }
    }
    
    
    
    public function actionAddToCart($id){
        $form_model = new \app\models\forms\CatalogForm(['item_id' => $id]);
        if($form_model->validate()){
            $form_model->save();
            return $this->redirect(['site/shopcart']);
        }
        return $this->goHome();
    }
    
    
    public function actionDownload(){
        $file_path = Yii::$app->request->get('f');
        if(file_exists($file_path)){
            return Yii::$app->response->sendFile($file_path);
        }
    }
    
    
    public function actionPayment($action){
        $request = Yii::$app->request;
        
        $fail = function(){
            header('Content-Type: text/plain; charset=UTF-8');
            echo 'FAIL';
            exit();
        };
        $success = function(){
            header('Content-Type: text/plain; charset=UTF-8');
            echo 'SUCCESS';
            exit();
        };
        if(!empty($request->get('MNT_TRANSACTION_ID'))){
            $r_data = $request->get();
        }
        elseif(!empty($request->post('MNT_TRANSACTION_ID'))){
            $r_data = $request->post();
        }
        else{
            $fail();
        }
        
        $check_code = Setting::get('moneta_check_code');
        
        switch($action){
            case 'pay' : {
                $operation_id = !empty($r_data['MNT_OPERATION_ID']) ? $r_data['MNT_OPERATION_ID'] : '';
                $amount = !empty($r_data['MNT_AMOUNT']) ? $r_data['MNT_AMOUNT'] : '';
                $subscriber_id = !empty($r_data['MNT_SUBSCRIBER_ID']) ? $r_data['MNT_SUBSCRIBER_ID'] : '';
                $result_code = 200;
                $signature = md5(
                    $r_data['MNT_ID']
                    . $result_code
                    . $r_data['MNT_TRANSACTION_ID']
                    . $check_code
                );
                $description = 'Оплата заказа';
                
                $order = Order::findOne([
                    'transaction_id' => $r_data['MNT_TRANSACTION_ID'],
                    'status' => Order::STATUS_BLANK,
                ]);
                if(!is_object($order)){
                    $fail();
                }
                $o_data = json_decode($order->config, true);
                $inventory = str_replace(
                    ['"n"', '"p"', '"q"', '"t"'],
                    ['"name"', '"price"', '"quantity"', '"vatTag"'],
                    json_encode($o_data['inventory']['items'])
                );
                
                $response = [
                    'MNT_RESPONSE' => [
                        'MNT_ID' => $r_data['MNT_ID'],
                        'MNT_TRANSACTION_ID' => $r_data['MNT_TRANSACTION_ID'],
                        'MNT_RESULT_CODE' => $result_code,
                        'MNT_DESCRIPTION' => $description,
                        'MNT_AMOUNT' => $amount,
                        'MNT_SIGNATURE' => $signature,
                        'MNT_ATTRIBUTES' => [
                            'ATTRIBUTE' => [
                                'KEY' => 'INVENTORY',
                                'VALUE' => $inventory
                            ]
                        ]
                    ]
                ];
                
                $check_signature = md5(
                    $r_data['MNT_ID']
                    . $r_data['MNT_TRANSACTION_ID']
                    . $operation_id
                    . $amount
                    . $r_data['MNT_CURRENCY_CODE']
                    . $subscriber_id
                    . $r_data['MNT_TEST_MODE']
                    . $check_code
                );
                
                if($r_data['MNT_SIGNATURE'] === $check_signature){
                    $order->status = Order::STATUS_PENDING;
                    $order->save();
                    
                    $manager = \app\models\Manager::getActive();
                    $files = [];
                    if(!empty($o_data['file'])){
                        $files[] = ['location' => $o_data['file']];
                    }
                    $manager->sendMail('Заявка с сайта (оплата онлайн)', $order->comment, $files);
                    $manager->sendMailTo($o_data['email'], $o_data);
                    $success();
                    return $this->asXml($response);
                }
                return '';
            }
            
            case 'success' : {
                $order = Order::findOne(['transaction_id' => $r_data['MNT_TRANSACTION_ID']]);
                if(is_object($order)){
                    Yii::$app->session->setFlash('title', 'Спасибо, что выбрали нашу компанию!');
                    Yii::$app->session->setFlash(
                        'message',
                        'Оплата прошла успешно.'
                            . '<br>Номер вашего заказа: ' . $order->id . '.'
                            . '<br>На ваш e-mail отправлено сопроводительное письмо.'
                    );
                    return $this->redirect(['/success']);
                }
                else{
                    return $this->goHome();
                }
            }
            
            case 'fail' : {
                Yii::$app->session->setFlash('title', 'Ошибка!');
                Yii::$app->session->setFlash(
                    'message',
                    'Ошибка при оплате заказа. Проверьте введенные данные и попробуйте выполнить оплату заказа еще раз, либо свяжитесь с нашим менеджером.'
                );
                return $this->redirect(['/success', 'type' => 'fail']);
            }
            
            case 'check' : {
                $operation_id = !empty($r_data['MNT_OPERATION_ID']) ? $r_data['MNT_OPERATION_ID'] : '';
                $amount = !empty($r_data['MNT_AMOUNT']) ? $r_data['MNT_AMOUNT'] : '';
                $subscriber_id = !empty($r_data['MNT_SUBSCRIBER_ID']) ? $r_data['MNT_SUBSCRIBER_ID'] : '';
				
                $result_code = 402;
                $description = 'Оплата заказа';
                $signature = md5(
                    $result_code
                    . $r_data['MNT_ID']
                    . $r_data['MNT_TRANSACTION_ID']
                    . $check_code
                );
                
                $response = [
                    'MNT_RESPONSE' => [
                        'MNT_ID' => $r_data['MNT_ID'],
                        'MNT_TRANSACTION_ID' => $r_data['MNT_TRANSACTION_ID'],
                        'MNT_RESULT_CODE' => $result_code,
                        'MNT_DESCRIPTION' => $description,
                        'MNT_AMOUNT' => $amount,
                        'MNT_SIGNATURE' => $signature,
                    ]
                ];
                return $this->asXml($response);
            }
        }
        
    }
    
    
    
    
    public function actionBuy($id, $a = null, $b = null){
        $item = \yii\easyii\modules\catalog\api\Catalog::get($id);
        $goods = Shopcart::goods();
        $can_buy = true;
        foreach($goods as $good){
            if((int)$good->item->id === (int)$item->id && $good->count >= 1){
                $can_buy = false;
                break;
            }
        }
        if($can_buy){
            Shopcart::add($item->id, 1);
        }
        CatalogHelper::removePreviousTo($item->id);
        if($b === 'choose'){
            return $this->redirect(['/choose', 'a' => $a]);
        }
        if(
            $a === 'pecati/ip'
            || $a === 'pecati/ooo'
            || $a === 'pecati/doctor'
            || $a === 'pecati'
            || $a === 'tools'
            || $a === 'stampy-tools'
        ){
            return $this->redirect(['/catalog/' . $a . '?a=buy']);
        }
        return $this->redirect(['/shopcart']);
    }
    
    public function actionChoose($a){
        $choose_model = new \app\models\forms\ChooseForm(['a' => $a]);
        if($choose_model->load(Yii::$app->request->post()) && $choose_model->action()){
            return $this->redirect(['catalog/' . $a, 'a' => 'buy']);
        }
        return $this->render('choose', [
            'callback_model' => new \app\models\forms\CallbackForm(),
            'choose_model' => $choose_model,
        ]);
    }
}