jQuery(function($){
    $('.chosen-select').chosen();
    $('.product-count').productCount();
    
    if(getMobileOperatingSystem() === 'unknown'){
        var navbar = jQuery('#navbar');
        navbar.stickyNavbar()
        navbar.after('<div class="margin-b-70"></div>');
    }
    
    $('.swiper-container').each(function(){
        var self = $(this),
            prev_control = self.find('.swiper-button-prev'),
            next_control = self.find('.swiper-button-next'),
            pagination = self.find('.swiper-pagination'),
            config = $.extend({
                slidesPerView : 1,
                spaceBetween : 45,
                simulateTouch : false,
                speed : 600,
                loop : true,
                navigation : {},
                pagination : {}
            }, self.data());
        if(parseInt(config.slidesPerView) > 1){
            config.breakpoints = {
                767 : {
                    slidesPerView: 1,
                    spaceBetween: 20
                },
                991 : {
                    slidesPerView: 2,
                    spaceBetween: 45
                },
                1199 : {
                    slidesPerView: 3,
                    spaceBetween: 45
                }
            };
        }
        if(prev_control.length > 0 && next_control.length > 0){
            config.navigation.prevEl = prev_control;
            config.navigation.nextEl = next_control;
        }
        if(pagination.length > 0){
            config.pagination.el = pagination;
        }
        new Swiper(self, config);
    });
    
    var counters = $('.counter');
    if(counters.length){
        counter(counters);
    }
});
