jQuery.fn.synchWith = function(form_selector, common_prefix){
    // Generate full name for each field in appropriate form
    var generateFullFieldName = function(str, prefix){
        if(prefix === undefined){
            return str;
        }
        if(typeof prefix !== 'string'){
            return str;
        }
        var f_name = str.replace(/\[\w+\]/g, ''),
            f_subnames = f_name.length === str.length ? '' : str.substring(f_name.length);
        return prefix + '[' + f_name + ']' + f_subnames;
    };

    // Affect each field
    return jQuery(this).each(function(){
        var field = jQuery(this),
            field_name = field.attr('name'),
            field_value = field.val(),
            hidden_copies = [];
        
        // Affect each form
        jQuery(form_selector).each(function(){
            var form = jQuery(this),
                data_prefix = form.data('prefix'),
                _prefix = data_prefix !== undefined ? data_prefix : common_prefix,
                full_field_name = generateFullFieldName(field_name, _prefix),
                hidden_copy_selector = 'input[name="' + full_field_name + '"]';
            
            hidden_copy = form.find(hidden_copy_selector);
            if(!hidden_copy.length){
                hidden_copy = jQuery('<input name="' + full_field_name + '" value="' + field_value + '" type="hidden">');
                hidden_copy.appendTo(form);
            }
            hidden_copies.push(hidden_copy);
        });

        // Create "synch" event for every affected input
        field.on('synch', function(){
            var i, l = hidden_copies.length;
            for(i = 0; i < l; i++){
                hidden_copies[i].val(this.value);
            }
        }).trigger('synch');
    });
};


jQuery.fn.productCount = function(){
    jQuery(this).each(function(){
        var self = jQuery(this),
            input = self.find('input[type="text"]');
        self.find('.product-count-minus').on('click', function(){
            var value = parseInt(input.val());
            if(value > 1){
                input.val(value - 1);
            }
            else{
                input.val(1);
            }
            input.trigger('product-count.change');
        });
        self.find('.product-count-plus').on('click', function(){
            var value = parseInt(input.val());
            input.val(value + 1);
            input.trigger('product-count.change');
        });
    });
};


jQuery.fn.modalSlider = function(modal_selector, callback){
    var modal = jQuery(modal_selector),
        modal_body = modal.find('.modal-body'),
        items = jQuery(this),
        items_count = items.length,
        index = 0;

    var slide = function(){
        modal_body.fadeOut(300, function(){
            callback(items.eq(index));
            modal_body.fadeIn(300);
        });
    };

    items.on('click', function(){
        index = items.index(jQuery(this));
        metrikaReachGoal('product');
        slide();
    });

    jQuery('<a class="left carousel-control modal-slider-control" href="#"><span class="glyphicon glyphicon-chevron-left"></span></a>')
        .on('click', function(){
            index = index <= 0 ? items_count - 1 : index - 1;
            slide();
            return false;
        })
        .appendTo(modal_body);
    jQuery('<a class="right carousel-control modal-slider-control" href="#"><span class="glyphicon glyphicon-chevron-right"></span></a>')
        .on('click', function(){
            index = index >= (items_count - 1) ? 0 : index + 1;
            slide();
            return false;
        })
        .appendTo(modal_body);
};


jQuery.fn.stickyNavbar = function(conf){
    var self = jQuery(this),
        self_y_start_position = self.get(0).offsetTop;
    conf = jQuery.extend({
        fixedClass : 'navbar-fixed-top'
    }, conf);

    jQuery(window).on('scroll', function(){
        if(window.pageYOffset >= self_y_start_position){
            self.addClass(conf.fixedClass);
            if(conf.onFixed !== undefined){
                conf.onFixed();
            }
            if(conf.hiddenOnFixed !== undefined){
                jQuery(conf.hiddenOnFixed).hide();
            }
            if(conf.shownOnFixed !== undefined){
                jQuery(conf.shownOnFixed).show();
            }
        }
        else{
            self.removeClass('navbar-fixed-top');
            if(conf.onNotFixed !== undefined){
                conf.onNotFixed();
            }
            if(conf.hiddenOnFixed !== undefined){
                jQuery(conf.hiddenOnFixed).show();
            }
            if(conf.shownOnFixed !== undefined){
                jQuery(conf.shownOnFixed).hide();
            }
        }
    }).trigger('scroll');
};


/**
 * Determine the mobile operating system.
 * This function returns one of 'iOS', 'Android', 'Windows Phone', or 'unknown'.
 *
 * @returns {String}
 */
function getMobileOperatingSystem(){
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;
    if (/windows phone/i.test(userAgent)) {
        return 'Windows Phone';
    }
    if (/android/i.test(userAgent)) {
        return 'Android';
    }
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
        return 'iOS';
    }
    return 'unknown';
}


function scrollToAnchor(selector, offset){
    var block_position = jQuery(selector).position().top;
    if(offset === undefined){
        offset = 0;
    }
    $('body,html').animate({scrollTop: block_position + offset}, 700);
}


function refreshShopcartInfo(){
    jQuery.get('/shopcart-info', {}, function(response){
        document.getElementById('shopcart-info-count').innerHTML = response.count + ' РµРґ.';
        document.getElementById('shopcart-info-cost').innerHTML = response.cost + ' СЂСѓР±.';
        document.getElementById('shopcart-info-count-2').innerHTML = response.count + ' РµРґ.';
        document.getElementById('shopcart-info-cost-2').innerHTML = response.cost + ' СЂСѓР±.';
    });
}


function notify(title, message, type){
    title = title ? '<strong>' + title + '</strong><br>' : '';
    type = type || 'success';
    message = message || '';
    var html = '<div class="notify"><div class="alert alert-' + type + '">';
    html += title + message + '</div></div>';
    var html_obj = jQuery(html);

    html_obj.appendTo(document.body).fadeIn(500, function(){
        setTimeout(function(){
            html_obj.fadeOut(500, function(){
                html_obj.remove();
            });
        }, 4000);
    });
}


function countCost(){
    var total_cost = 0,
        delivery_price = 0,
        delivery_info;
    jQuery('.product-count-input').each(function(){
        var self = jQuery(this),
            good_price = (parseInt(self.data('price'))) * parseInt(self.val());
        self.parent().parent().parent().find('.shopcart-good-price').html(good_price + ' СЂСѓР±.');
        total_cost += good_price;
    });
    if(parseInt(jQuery('#shopcart-delivery').val()) === 1){
        delivery_info = jQuery('#shopcart-city_id').find(':checked').data();
        if(total_cost < APP.free_delivery_min_cost){
            delivery_price = delivery_info.delivery_price;
        }
        else{
            delivery_price = 0;
        }
    }
    else{
        delivery_price = 0;
    }

    document.getElementById('total-cost').innerHTML = total_cost + ' СЂСѓР±';
    document.getElementById('delivery-price').innerHTML = delivery_price === 0 ? 'Р‘РµСЃРїР»Р°С‚РЅРѕ' : delivery_price  + ' СЂСѓР±';
    document.getElementById('total-with-delivery-cost').innerHTML = total_cost + delivery_price  + ' СЂСѓР±';
}


 function showPriceInfo(info){
    document.getElementById('min').innerHTML = info.value[0];
    document.getElementById('max').innerHTML = info.value[1];
    document.getElementById('min_price').value = info.value[0];
    document.getElementById('max_price').value = info.value[1];
}


function counter(counters){
    counters.each(function(){
        var el = jQuery(this),
            seconds = parseInt(el.data('counterSeconds'));
        el.html(seconds);
        var id = setInterval(function(){
            if(seconds === 0){
                clearInterval(id);
                return;
            }
            seconds -= 1;
            el.html(seconds);
        }, 1000);
    });
}
