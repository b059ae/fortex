<?php

namespace app\components;

use app\models\interfaces\CallerInterface;
use yii\base\Component;
use yii\base\InvalidConfigException;

/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 11.08.17
 * Time: 14:16
 */
class Beeline extends Component
{
    private $url = 'http://vn.beeline.ru/com.broadsoft.xsi-actions/v2.0/user/{user}@mpbx.sip.beeline.ru/calls/CallMeNow?address={phone}';
    
    private $cloud_pbx_url = 'https://cloudpbx.beeline.ru/apis/portal/abonents/{manager_number}/call';
    
    /**
     * Пользователь из кода виджета для обратного звонка с сайта
     * Находится во вкладке Звонок с сайта на странице https://cloudpbx.beeline.ru/#Профиль
     * Формат MPBX_g_76328_hg_87307
     * @var string
     */
    public $user;
    
    public $manager_numbers;
    public $token;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        if (empty($this->user)){
            throw new InvalidConfigException('Требуется указать beeline.user в настройках');
        }
        $this->url = strtr($this->url,[
            '{user}' => $this->user
        ]);
        
        if (empty($this->manager_numbers)){
            throw new InvalidConfigException('Требуется указать в настройках beeline.manager_numbers в формате <id> => <number>');
        }
        
        if (empty($this->token)){
            throw new InvalidConfigException('Требуется указать в beeline.token настройках');
        }
    }

    /**
     * @inheritdoc
     */
    public function call($phoneNumber)
    {
        $s = curl_init();
        curl_setopt($s, CURLOPT_URL, strtr($this->url,[
            '{phone}' => urlencode($phoneNumber)
        ]));
        curl_setopt($s, CURLOPT_POST, true);
        curl_setopt($s, CURLOPT_RETURNTRANSFER, true);
        // Хак для работы с кривым php curl
        curl_setopt($s, CURLOPT_HTTPHEADER, array(
            "Content-Length: 0"
        ));
        curl_exec($s);
    }
    
    
    public function cloudPbxCall($customer_number, $manager_number){
        $s = curl_init();
        curl_setopt($s, CURLOPT_URL, strtr($this->cloud_pbx_url,[
            '{manager_number}' => urlencode($manager_number),
        ]));
        curl_setopt($s, CURLOPT_POST, true);
        curl_setopt($s, CURLOPT_POSTFIELDS, http_build_query(['phoneNumber' => $customer_number]));
        curl_setopt($s, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($s, CURLOPT_SSL_VERIFYPEER, false);
        // Хак для работы с кривым php curl
        curl_setopt($s, CURLOPT_HTTPHEADER, [
            "X-MPBX-API-AUTH-TOKEN: " . $this->token
        ]);
        curl_exec($s);
    }
    
    
    
    private function getManagerNumber($id){
        return $this->manager_numbers[$id];
    }
}