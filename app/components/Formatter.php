<?php
/**
 * User: Alexander Popov
 * Email: b059ae@gmail.com
 * Date: 03/12/2017 21:48
 */

namespace app\components;

class Formatter extends \yii\i18n\Formatter
{
    public function asPhone($value) {
        if ($value === null) {
            return $this->nullDisplay;
        }
        return  preg_replace("/([\d]{3})([\d]{3})([\d]{2})([\d]{2})/", "+7 ($1) $2-$3-$4", $value);
    }
}