<?php
/* @var $customer_data array */
/* @var $site_data array */

?>
<table class="bordered">
    <tr>
        <td colspan="2" class="w-60-p h-75 va-t">
            Банк получателя:<br><strong><?=$site_data['bank'];?></strong>
        </td>
        <td class="va-t">
            БИК: <strong><?=$site_data['bik'];?></strong>
            <br>
            Сч. №: <strong><?=$site_data['correspondent_account'];?></strong>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="w-60-p">
            Получатель:<br><strong><?=$site_data['company'];?></strong>
        </td>
        <td rowspan="3" class="va-t">
            Сч. № <strong><?=$site_data['checking_account'];?></strong>
        </td>
    </tr>
    <tr>
        <td>
            ИНН: <strong><?=$site_data['inn'];?></strong>
        </td>
        <td>
            КПП: <strong><?=$site_data['kpp'];?></strong>
        </td>
    </tr>
    <tr>
        <td>
            ОГРН: <strong><?=$site_data['ogrn'];?></strong>
        </td>
        <td>
            
        </td>
    </tr>
    
</table>
<h2>Счет на оплату №СП-<?=$order_info->order->id;?> от <?=date('d.m.Y');?></h2>
<hr>
<p>Поставщик<br>(Исполнитель): <strong><?=$site_data['company'];?>, <?=$site_data['address'];?>,<br>тел.: <?=$site_data['phone'];?></strong></p>
<p class="mb-30">
    Покупатель<br>(Заказчик):
    <strong>
        <?php if(!empty($customer_data['company'])): ?>
            <?=$customer_data['company'];?>,
        <?php else: ?>
            <?=$customer_data['name'];?>,
        <?php endif; ?>
        <?php if(!empty($customer_data['inn'])): ?>
            ИНН: <?=$customer_data['inn'];?>
        <?php endif; ?>
    </strong>
</p>
<table class="bordered">
    <tr>
        <th>№</th>
        <th>Товар</th>
        <th>Кол-во</th>
        <th>Ед.</th>
        <th>Цена</th>
        <th>Сумма</th>
    </tr>
    
    <?php $number = 1;?>
    <tr>
        <td>1</td>
        <td><?=$product_title;?></td>
        <td><?=$count;?></td>
        <td>шт.</td>
        <td><?=number_format($price_for_one);?></td>
        <td><?=number_format($price_for_one * $count);?></td>
    </tr>
        
    <?php if((int)$delivery === 2): ?>
        <?php $number++; ?>
        <tr>
            <td>2</td>
            <td>Доставка</td>
            <td></td>
            <td></td>
            <td></td>
            <td><?=number_format($delivery_price, 2, '.', '');?></td>
        </tr>
    <?php endif; ?>
</table>
<div class="total">
    <div class="left text-right w-200">
        <p><strong>Всего к оплате:</strong></p>
    </div>
    <div class="right text-right w-200">
        <p>Без НДС <?=number_format($cost, 2, '.', '');?> руб.</p>
    </div>
    <div class="clear"></div>
</div>
<div class="clear"></div>
<div class="left w-200">
    <p><strong>Всего наименований:</strong></p>
</div>
<div class="left w-200">
    <p><?=$number;?></p>
</div>
<div class="clear"></div>
<div class="warning">
    <p>Внимание!</p>
    <p>Оплата данного счета означает согласие с условиями поставки товара.</p>
    <p>Товар отпускается по факту прихода денег на р/с Поставщика, самовывозом, при наличии паспорта.</p>
</div>
<hr>
<div class="left">
    <table>
        <tr>
            <td>
                <strong>Директор:</strong> <?=$site_data['director'];?>
            </td>
        </tr>
        <tr>
            <td><img src="img/signature.jpg"></td>
        </tr>
    </table>
</div>
<div class="right">
    <img src="img/stamp.jpg">
</div>