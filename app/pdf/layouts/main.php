<html>
<head>
    <meta charset="utf-8">
    <style>
        body{
            font-family: DejaVu Sans;
            font-size: 12px !important;
        }
        h2 {  }
        table.bordered{
            border-collapse: collapse;
            width: 100%;
            font-size: 13px;
        }
        table.bordered td, table.bordered th{
            border: 1px solid #333;
        }
        table.bordered td{
            vertical-align: bottom;
        }
        hr{ margin: 20px 0; }
        .va-t{ vertical-align: top !important; }
        .h-50{ height: 50px; }
        .h-75{ height: 75px; }
        .h-100{ height: 100px; }
        .w-200{ width: 200px; }
        .w-45-p{ width: 45%; }
        .w-60-p{ width: 60%; }
        .w-30-p{ width: 30%; }
        .mb-30{ margin-bottom: 30px; }
        .mb-20{ margin-bottom: 20px; }
        .left{ float: left; }
        .right{ float: right; }
        .clear{ clear: both; }
        .text-right{ text-align: right; }
        .total{ margin: 10px 0 10px 300px; }
        .total p{ margin: 5px 0; }
        .warning{ margin: 20px 0; }
        .warning p{ margin: 0; }
    </style>
</head>
<body>
    <?=$content;?>
</body>
</html>