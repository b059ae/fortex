<?php

namespace app\helpers;

use Yii;

class WordHelper {
    
    public static function saveDocFile($report){
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        
        if(!empty($report['title'])){
            $section->addText($report['title'], ['size' => '16', 'bold' => true], ['alignment' => 'center']);
            $section->addTextBreak(2);
            unset($report['title']);
        }
        if(!empty($report['time_to_work'])){
            $section->addText($report['time_to_work'], ['size' => '16'], ['alignment' => 'center']);
            $section->addTextBreak(2);
            unset($report['time_to_work']);
        }
        if(!empty($report['delivery'])){
            $section->addText($report['delivery'], ['size' => '16'], ['alignment' => 'center']);
            $section->addTextBreak(2);
            unset($report['delivery']);
        }
        
        foreach($report as $good_report){
            if(!is_array($good_report)){
                continue;
            }
            $section->addText($good_report['title'], ['size' => '14', 'bold' => true], ['alignment' => 'center']);
            $section->addTextBreak(2);
            unset($good_report['title']);
            
            foreach($good_report as $text_section){
                $section->addText("\n\n" . $text_section, ['size' => '12']);
                $section->addTextBreak(1);
            }
            $section->addTextBreak(3);
        }
        
        if(!empty($report['total'])){
            $section->addText($report['total'], ['size' => '16'], ['alignment' => 'center']);
        }
        
        $file_name = time() . '.docx';
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save(Yii::getAlias('@webroot') . '/docs/' . $file_name);
        return Yii::$app->request->hostInfo . '/docs/' . $file_name;
    }
    
}
