<?php

namespace app\helpers;

use Yii;
use yii\easyii\modules\catalog\api\Catalog;
use yii\easyii\modules\catalog\models\Category;
use yii\easyii\modules\catalog\api\CategoryObject;
use yii\easyii\modules\catalog\models\Item;
use yii\easyii\models\Setting;
use yii\easyii\modules\shopcart\api\Shopcart;

class Product{
    
    public static $tools_config = [
        'ruchnye' => [
            'show_wrapper' => false,
            'col_class' => 'col-xs-6 col-sm-4 col-md-4 ',
        ],
        'karmannye' => [
            'show_wrapper' => false,
            'col_class' => 'col-xs-6 col-sm-4 col-md-3 col-lg-2',
        ],
        'automaticheskie' => [
            'show_wrapper' => false,
            'col_class' => 'col-xs-6 col-sm-4 col-md-3',
        ],
        'metallicheskie' => [
            'show_wrapper' => false,
            'col_class' => 'col-xs-6 col-sm-4 col-md-3 col-lg-2',
        ],
        'stampy-tools' => [
            
        ],
    ];
    public static $pecati_config = [
        'ip' => [
            
        ],
        'ooo' => [
            
        ],
        'doctor' => [
            
        ],
        'notarius' => [
            
        ],
        'extraordinary' => [
            
        ],
        'gerbovye' => [
            
        ],
        'stampy' => [
            
        ],
    ];
    public static $plombiry_config = [
        'prices' => [290, 245, 195, 190, 180, 170],
    ];
    public static $flazhok_config = [
        'prices' => [
            [280, 260, 240],
            [280, 260, 240],
            100,
        ]
    ];
    public static $shtok_config = [
        'prices' => [280, 260, 240],
    ];
    public static $datery_config = [
        'stampy-samonabornye' => [],
        'pecati-samonabornye' => [],
        'datery' => [],
        'numeratory' => [],
        'datery-so-svobodnym-polem' => [],
        'avtomaticeskie-numeratory' => [],
        'samonabornyj-dater' => [],
        'datery-different' => [],
    ];
    
    
    public $order;
    public $template;
    public $cliche_good;
    public $tool_good;
    public $good;
    
    public function __construct($order){        
        $this->order = $order;
        $goods_count = (int)count($order->goods);
        $pecati_slugs = array_keys(self::$pecati_config);
        $tools_slugs = array_keys(self::$tools_config);
        if($goods_count === 1){
            $good = $order->goods[0];
            if(in_array($good->item->cat->slug, array_keys(self::$datery_config))){
                $this->template = 'datery-i-numeratory';
                $this->good = $good;
            }
            elseif(
                $good->item->cat->slug === 'plombiry-i-plombiratory'
            ){
                $this->template = $good->item->slug;
                $this->good = $good;
            }
            elseif(
                $good->item->cat->slug === 'sp-grm'
                || $good->item->cat->slug === 'sp-colop'
                || $good->item->cat->slug === 'sp-trodat'
            ){
                $this->template = 'smennye-poduski';
                $this->good = $good;
            }
        }
        else{
            foreach($order->goods as $good){
                $cat_slug = $good->item->cat->slug;
                if(in_array($cat_slug, $pecati_slugs)){
                    $cliche_good = $good;
                }
                elseif(in_array($cat_slug, $tools_slugs)){
                    $tool_good = $good;
                }
            }
            if(isset($cliche_good) && isset($tool_good)){
                $this->cliche_good = $cliche_good;
                $this->tool_good = $tool_good;
                if(
                    $cliche_good->item->cat->slug === 'stampy'
                    && $tool_good->item->cat->slug === 'stampy-tools'
                ){
                    $this->template = 'stampy/' . $tool_good->item->slug;
                }
                else{
                    $this->template = 'pecati';
                }
            }
        }
    }
    
    public function getFullTitle(){
        if(!$this->template){
            return null;
        }
        if(is_object($this->cliche_good) && is_object($this->tool_good)){
            $cliche = $this->getClicheInfo('item');
            $tool = $this->getToolInfo('item');
            $cliche_title = !empty($cliche->data->fulltitle) ? $cliche->data->fulltitle : $cliche->title;
            $tool_title = !empty($tool->data->fulltitle) ? $tool->data->fulltitle : $tool->title;
            return $cliche_title . ' + ' . $tool_title;
        }
        return $this->order->goods[0]->item->title;
    }
    
    public function getPrice($options = []){
        if(!$this->template){
            return 0;
        }
        if(is_object($this->cliche_good) && is_object($this->tool_good)){
            $cliche = $this->cliche_good->item;
            $tool = $this->tool_good->item;
            
            if($tool->slug === 'st-tool-7'){
                if(isset($options['config_size'])){
                    $k = 'price' . $options['config_size'];
                    $tool_price = isset($tool->data->$k) ? $tool->data->$k : 0;
                    if(is_numeric($tool_price)){
                        return $cliche->price + $tool_price;
                    }
                    return $cliche->price;
                }
                return $cliche->price;
            }
            return $cliche->price + $tool->price;
        }
        return $this->order->goods[0]->item->price;
    }
    
    public function getCost(){
        return $this->getPrice() * $this->getCount();
    }
    
    public function getCount(){
        if(!$this->template){
            return null;
        }
        if(is_object($this->cliche_good) && is_object($this->tool_good)){
            return $this->cliche_good->model->count;
        }
        return $this->good->model->count;
    }
    
    public function getImages($w = null, $h = null){
        if(!$this->template){
            return null;
        }
        if($this->template === 'pecati'){
            return [
                $this->getClicheInfo('item')->thumb($w, $h),
                $this->getToolInfo('item')->thumb($w * 2, $h * 2),
            ];
        }
        else{
            return [$this->order->goods[0]->item->thumb($w, $h)];
        }
    }
    
    public function getClicheInfo($key = null){
        if($this->cliche_good){
            if($key === 'cat_slug'){
                return $this->cliche_good->item->cat->slug;
            }
            if($key === 'item'){
                return $this->cliche_good->item;
            }
            if($key === 'price'){
                return $this->cliche_good->item->price;
            }
            return $this->cliche_good;
        }
        return null;
    }
    
    public function getToolInfo($key = null, $options = []){
        if($this->tool_good){
            if($key === 'cat_slug'){
                return $this->tool_good->item->cat->slug;
            }
            if($key === 'item'){
                return $this->tool_good->item;
            }
            if($key === 'price'){
                return $this->tool_good->item->price;
            }
            if($key === 'sizes_and_prices'){
                $data = $this->tool_good->item->data;
                $result = [];
                for($i = 1; $i <= 25; $i++){
                    $size_dk = 'size' . $i;
                    $price_dk = 'price' . $i;
                    if(!isset($data->$size_dk) && !isset($data->$price_dk)){
                        break;
                    }
                    $result[$i] = [
                        'size' => $data->$size_dk,
                        'price' => $data->$price_dk,
                    ];
                }
                return $result;
            }
            return $this->tool_good;
        }
        return null;
    }
    
    public function getGood(){
        return $this->order->goods[0];
    }
    
    public static function getPlombiryPrice($count){
        if($count >= 1 && $count <= 10){
            return self::$plombiry_config['prices'][0];
        }
        if($count >= 11 && $count <= 50){
            return self::$plombiry_config['prices'][1];
        }
        if($count >= 51 && $count <= 100){
            return self::$plombiry_config['prices'][2];
        }
        if($count >= 101 && $count <= 500){
            return self::$plombiry_config['prices'][3];
        }
        if($count >= 501 && $count <= 1000){
            return self::$plombiry_config['prices'][4];
        }
        if($count >= 1000){
            return self::$plombiry_config['prices'][5];
        }
    }
    
    public static function getFlazhokPrice($count, $staff){
        if($staff === 'пластик d25 мм'){
            return self::$flazhok_config['prices'][2];
        }
        if($count >= 1 && $count <= 10){
            return self::$flazhok_config['prices'][0][0];
        }
        if($count >= 11 && $count <= 50){
            return self::$flazhok_config['prices'][0][1];
        }
        if($count >= 51){
            return self::$flazhok_config['prices'][0][2];
        }
    }
    public static function getShtokPrice($count){
        if($count >= 1 && $count <= 10){
            return self::$shtok_config['prices'][0];
        }
        if($count >= 11 && $count <= 50){
            return self::$shtok_config['prices'][1];
        }
        if($count >= 51){
            return self::$shtok_config['prices'][2];
        }
    }
}
