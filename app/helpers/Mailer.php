<?php
 
namespace app\helpers;
 
use yii\base\InvalidConfigException;
 
class Mailer extends \yii\swiftmailer\Mailer {
 
    private $sender;
    private $transports = [];
 
    public function setTransports($transports) {
        if(!is_array($transports)){
            throw new InvalidConfigException('"' . get_class($this) . '::transport" should be array, "' . gettype($transports) . '" given.');
        }
        $this->transports = $transports;
    }
 
    public function useTransport($name) {
        if(!array_key_exists($name, $this->transports)){
            throw new InvalidConfigException('"' . get_class($this) . '::useTransport" The requested transport '.$name.' could not be found');
        }
        $this->setTransport($this->transports[$name]);
        $this->sender = $this->transports[$name]['username'];
        return $this;
    }
 
    public function compose($view = null, array $params = []) {
        $message = parent::compose($view, $params);
        $message->setFrom($this->sender); // By default SMTP Username will be used for sender
        return $message;
    }
 
}