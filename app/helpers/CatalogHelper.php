<?php
namespace app\helpers;

use Yii;
use yii\easyii\modules\catalog\api\Catalog;
use yii\easyii\modules\catalog\models\Category;
use yii\easyii\modules\catalog\api\CategoryObject;
use yii\easyii\modules\catalog\models\Item;
use yii\easyii\models\Setting;
use yii\easyii\modules\shopcart\api\Shopcart;


class CatalogHelper{
    
    public static $tools_config = [
        'ruchnye' => [
            'show_wrapper' => false,
            'col_class' => 'col-xs-6 col-sm-4 col-md-4 ',
        ],
        'karmannye' => [
            'show_wrapper' => false,
            'col_class' => 'col-xs-6 col-sm-4 col-md-3 col-lg-2',
        ],
        'automaticheskie' => [
            'show_wrapper' => false,
            'col_class' => 'col-xs-6 col-sm-4 col-md-3',
        ],
        'metallicheskie' => [
            'show_wrapper' => false,
            'col_class' => 'col-xs-6 col-sm-4 col-md-3 col-lg-2',
        ],
    ];
    public static $pecati_config = [
        'ip' => [
            
        ],
        'ooo' => [
            
        ],
        'doctor' => [
            
        ],
        'notarius' => [
            
        ],
        'extraordinary' => [
            
        ],
        'gerbovye' => [
            
        ],
    ];
    public static $plombiratory_config = [
        
    ];
    
    /*public static $plombiratory_config = [
        'plombirator', 'plombiry', 'latun', 'plomby-svincovye',
        'flazhok', 'shtok', 'penaly', 'plashki', 'sejfovoe', 'sterzen'
    ];*/
    
    
    
    public static function getCats($where, $parent = null){
        $cat_objects = Category::find()
            ->where($where)
            ->orderBy(['order_num' => SORT_ASC])
            ->all();
        return array_map(function($cat) use ($parent){
            if($parent !== null){
                $cat->parent = $parent;
            }
            return new CategoryObject($cat);
        }, $cat_objects);
    }
    
    
    public static function getToolCats(){
        return self::getCats(['slug' => array_keys(self::$tools_config)], 'osnastki');
    }
    
    public static function getPecatiCats(){
        return self::getCats(['slug' => array_keys(self::$pecati_config)], 'pecati');
    }
    
    public static function getMainCats(){
        return self::getCats(['id' => [1,2,4,5]]);
    }
    
    public static function getOrderInfo(){
        $order = (new \yii\easyii\modules\shopcart\api\Shopcart())->order;
        return new \app\helpers\Product($order);
    }
    
    public static function getUrgencyInfo(){
        return [
            '1' => Setting::get('urgency_1_day'),
            '2' => Setting::get('urgency_6_hours'),
            '3' => Setting::get('urgency_1_hour'),
        ];
    }
    
    public static function getUrgencyPrice($id){
        $id = (int)$id;
        if($id === 1){
            return Setting::get('urgency_1_day');
        }
        elseif($id === 2){
            return Setting::get('urgency_6_hours');
        }
        elseif($id === 3){
            return Setting::get('urgency_1_hour');
        }
    }
    
    public static function getDeliveryPrice(){
        return Setting::get('delivery_price');
    }
    
    
    public static function buyHiddenItem($cat_slug){
        $cat = Catalog::cat($cat_slug);
        $item_id = $cat->items[0]->id;
        Shopcart::add($item_id, 1);
        self::removePreviousTo($item_id);
    }
    
    
    
    public static function getParentCat($cat){
        return Catalog::cat($cat->model->parent);
    }
    
    public static function removeActiveGoods(){
        $order = (new Shopcart())->order;
        foreach($order->goods as $good){
            Shopcart::remove($good->id);
        }
    }
    
    public static function removePreviousTo($id){
        $item = Catalog::get($id);
        if(in_array($item->cat->slug, array_keys(self::$pecati_config))){
            
            foreach(Shopcart::goods() as $good){
                if(
                    $good->item->id != $id
                    && (
                        $item->cat->model->parent === $good->item->cat->model->parent
                        || $good->item->cat->slug === 'stampy'
                    )
                ){
                    Shopcart::remove($good->id);
                }
            }
        }
        elseif(in_array($item->cat->slug, array_keys(self::$tools_config))){
            foreach(Shopcart::goods() as $good){
                if(
                    $good->item->id != $id
                    && (
                        $item->cat->model->parent === $good->item->cat->model->parent
                        || $good->item->cat->slug  === 'stampy-tools'
                    )
                ){
                    Shopcart::remove($good->id);
                }
            }
        }
        elseif(
            $item->cat->slug === 'plombiry-i-plombiratory'
            || $item->cat->slug === 'sp-grm'
            || $item->cat->slug === 'sp-colop'
            || $item->cat->slug === 'sp-trodat'
            || in_array($item->cat->slug, array_keys(Product::$datery_config))
        ){
            foreach(Shopcart::goods() as $good){
                if($good->item->id != $id){
                    Shopcart::remove($good->id);
                }
            }
        }
        
        elseif($item->cat->slug == 'stampy'){
            
            foreach(Shopcart::goods() as $good){
                if(
                    $good->item->id != $id
                    && (
                        $item->cat->slug === $good->item->cat->slug
                        || in_array($good->item->cat->slug, array_keys(self::$pecati_config))
                    )
                ){
                    Shopcart::remove($good->id);
                }
            }
        }
        elseif($item->cat->slug === 'stampy-tools'){
            foreach(Shopcart::goods() as $good){
                if(
                    $good->item->id != $id
                    && (
                        $item->cat->slug === $good->item->cat->slug
                        || in_array($good->item->cat->slug, array_keys(self::$tools_config))
                    )
                ){
                    Shopcart::remove($good->id);
                }
            }
        }
    }
    
    
    public static function getToolCatConfig($slug, $key = null){
        if(!isset(self::$tools_config[$slug])){
            return null;
        }
        if($key === null){
            return self::$tools_config[$slug];
        }
        return isset(self::$tools_config[$slug][$key]) ? self::$tools_config[$slug][$key] : null;
    }
    
    
    public static function getCheckboxListTemplator(){
        return function($index, $label, $name, $checked, $value){
            $output = '
            <div class="block-inline margin-b-20">
            <input type="checkbox" class="checkbox" id="'.$name.'-'.$index.'"'
            . ' name="'.$name.'" value="'.$value.'"'.($checked ? ' checked ' : '').'/>
            <label for="'.$name.'-'.$index.'">'.$label.'</label>
            </div>';
            return $output;
        };
    }
    
    
    public static function getRadioListTemplator(){
        return function ($index, $label, $name, $checked, $value) {
            $check = $checked ? ' checked="checked"' : '';
            return "<label class=\"radio-inline\"><input type=\"radio\" name=\"$name\" value=\"$value\"$check>$label</label>";
        };
    }
    
    
    // TO COMPLETE
    public static function getMinPrice($cat){
        return Item::find()->select('price')->where(['category_id' => $cat->id])->min('price');
    }
    
    
    // TO COMPLETE
    public static function getCategoryName($item){
        $names = [
            'pecati' => 'Печати',
            'stampy' => 'Штампы',
            'faximile' => 'Факсимиле',
            'ekslibris' => 'Экслибрис'
        ];
        $subcat = Catalog::cat($item->category_id);
        if(!empty($names[$subcat->slug])){
            return $names[$subcat->slug];
        }
        if(empty($subcat->model->parent)){
            return null;
        }
        $cat = Category::findOne($subcat->model->parent);
        if(!empty($names[$cat->slug])){
            return $names[$cat->slug];
        }
        return null;
    }
    
    
    /**
     * Проверка рабочее время сейчас или нет
     *
     * @return int 1 - рабочее, -1 - выходные, 0 - будни нерабочее
     */
    static public function isWorkingTime(){
        $week_day = date('D');
        if($week_day == 'Sat' || $week_day == 'Sun'){
            return -1;
        }
        $earliest = mktime(9, 0, 0);
        $latest = mktime(18, 0, 0);
        $now = time();
        if($now <= $earliest || $now >= $latest){
            return 0;
        }
        return 1;
    }
}