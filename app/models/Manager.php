<?php

namespace app\models;

use Yii;
use yii\easyii\models\Setting;

/**
 * This is the model class for table "managers".
 *
 * @property int $id
 * @property string $name
 * @property int $amocrm_id
 * @property string $email
 * @property string $phone
 * @property string $email_pwd
 * @property int $used
 */
class Manager extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'managers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['amocrm_id'], 'integer'],
            [['name', 'email', 'phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'amocrm_id' => 'Amocrm ID',
            'email' => 'E-mail',
            'phone' => 'Телефон',
        ];
    }
    
    /**
     * 
     * @return \app\models\Manager
     */
    public static function getActive(){
        $one = self::find()
            ->where(['used' => 0])
            ->one();
        if(!$one){
            self::updateAll(['used' => 0]);
            $one = self::find()
                ->where(['used' => 0])
                ->one();
        }
        $one->used = 1;
        $one->save();
        return $one;
    }
    
    
    public function sendMail($subject, $message, $files = [], $html = false){
        $robo_email = Setting::get('robot_email');
        $robo_email_pass = 'pass4fortex2';
        $m = Yii::$app->mailer->compose();
        $m->mailer->transport->setUsername($robo_email);
        $m->mailer->transport->setPassword($robo_email_pass);
        $m->setFrom([$robo_email => Yii::$app->name])
            ->setTo($this->email)
            ->setBcc('vk@telecombg.ru')
            ->setSubject($subject);
        if($html){
            $m->setHtmlBody($message);
        }
        else{
            $m->setTextBody($message);
        }
        
        if(count($files)){
            foreach($files as $file){
                $m->attach($file['location']);
            }
        }
        return $m->send();
    }
    
    
    public function sendMailTo($email, $form_data, $files = []){
        $template = (int)$form_data['delivery'] === 2 ? 'delivery_notification_' : 'notification_';
        $template .= $form_data['order_type'];
        $message = Yii::$app->view->renderFile(
            '@app/views/site/email/'.$template.'.php',
            array_merge($form_data, [
                'manager' => $this->name,
                'phone' => Setting::get('phone'),
                'address' => Setting::get('address'),
            ])
        );
        $mail = Yii::$app->mailerb->compose();
        $mail->mailer->transport->setUsername($this->email);
        $mail->mailer->transport->setPassword($this->email_pwd);
        $mail->setFrom([$this->email => Yii::$app->name])
            ->setTo($email)
            ->setSubject('Ваш заказ оформлен!')
            ->setHtmlBody($message);
        if(count($files)){
            foreach($files as $file){
                $mail->attach($file['location']);
            }
        }
        return $mail->send();
    }
    
    private function dataToFormat($key, $value){
        if($key === 'phone' || $key === 'email'){
            return [
                [$value, 'WORK']
            ];
        }
        return $value;
    }

    public function sendToAmo($subject, $data){
        if(empty($this->amocrm_id)){
            return false;
        }
        $amo = Yii::$app->amocrm->getClient();
        $lead_cfs = $amo->fields['lead_custom_fields'];
        $contact_cfs = $amo->fields['contact_custom_fields'];

        $lead = $amo->lead;
        $lead['name'] = $subject;
        $lead['responsible_user_id'] = $this->amocrm_id;
        $lead['pipeline_id'] = $amo->fields['pipeline_id'];
        foreach($lead_cfs as $key => $key_id){
            if(!isset($data[$key])){
                continue;
            }
            $lead->addCustomField(
                $key_id,
                $this->dataToFormat($key, $data[$key])
            );
        }
        $lead_id = $lead->apiAdd();

        $contact = $amo->contact;
        $contact['name'] = $data['name'];
        $contact['responsible_user_id'] = $this->amocrm_id;
        foreach($contact_cfs as $key => $key_id){
            if(!isset($data[$key])){
                continue;
            }
            $contact->addCustomField(
                $key_id,
                $this->dataToFormat($key, $data[$key])
            );
        }
        $contact_id = $contact->apiAdd();

        $contact = $amo->contact;
        $contact->setLinkedLeadsId([$lead_id]);
        return $contact->apiUpdate((int)$contact_id, 'now');
    }
}