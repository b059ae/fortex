<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\base\Security;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "customers".
 *
 * @property int $id
 * @property int $phone Телефон
 * @property string $email Email
 * @property string $name ФИО
 * @property string $auth_key Токен авторизации
 * @property string $password_hash Пароль
 * @property string $password_reset_token Токен для сброса пароля
 * @property int $created_at Дата создания
 * @property int $updated_at Дата обновления
 */
class Customers extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customers';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone', 'created_at', 'updated_at'], 'integer'],
            [['auth_key', 'password_hash'], 'required'],
            [['email', 'name', 'password_hash', 'password_reset_token'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'phone' => 'Телефон',
            'email' => 'Email',
            'name' => 'ФИО',
            'auth_key' => 'Токен авторизации',
            'password_hash' => 'Пароль',
            'password_reset_token' => 'Токен для сброса пароля',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Поиск пользователя по телефону
     *
     * @param string $phone
     * @return static|null
     */
    public static function findByPhone($phone)
    {
        return static::findOne(['phone' => $phone]);
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param $password
     * @throws \yii\base\Exception
     */
    public function setPassword($password)
    {
        /** @var Security $security */
        $security = Yii::$app->security;
        $this->password_hash = $security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     *
     * @throws \yii\base\Exception
     */
    public function generateAuthKey()
    {
        /** @var Security $security */
        $security = Yii::$app->security;
        $this->auth_key = $security->generateRandomString();
    }

    /**
     * Generates password
     *
     * @return string Password
     * @throws \yii\base\Exception
     */
    public function generatePassword()
    {
        /** @var Security $security */
        $security = Yii::$app->security;
        $generatedPassword = $security->generateRandomString(8);
        $this->password_hash = $security->generatePasswordHash($generatedPassword);
        return $generatedPassword;
    }

    /**
     * Generates new password reset token
     *
     * @throws \yii\base\Exception
     */
    public function generatePasswordResetToken()
    {
        /** @var Security $security */
        $security = Yii::$app->security;
        $this->password_reset_token = $security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Список заказов пользователя с фильтрацией по статусу
     *
     * @param int $status
     * @return ActiveDataProvider
     */
    public function getOrders($status)
    {
        return new ActiveDataProvider([
            'query' => Order::find()
                ->andWhere(['customer_id' => $this->id])
                ->status($status)
                ->desc(),
        ]);
    }
}
