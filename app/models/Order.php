<?php
/**
 * User: Alexander Popov
 * Email: b059ae@gmail.com
 * Date: 03/12/2017 22:49
 */

namespace app\models;

use Yii;
use app\models\Good;
use yii\easyii\models\Setting;


class Order extends \yii\easyii\modules\shopcart\models\Order
{
    const TIME_TO_WORK_ONE_HOUR = 0;
    const TIME_TO_WORK_THREE_HOURS = 1;
    const TIME_TO_WORK_ONE_DAY = 2;
    
    private $_goods;
    
    public function rules() {
        return array_merge(parent::rules(), [
            [['delivery', 'time_to_work', 'city_id', 'customer_id'], 'integer'],
            [['transaction_id'], 'safe']
        ]);
    }
    
    
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        // Фикс против ошибки неверной категории сообщений easyii/shopcart
        return [];

    }

    
    /**
     * @inheritdoc
     */
    public static function states()
    {
        return [
            self::STATUS_PENDING => 'Ожидает оплаты',
            self::STATUS_PROCESSED => 'В работе',
            self::STATUS_COMPLETED => 'Выполнен',
        ];
    }

    
    /**
     * @inheritdoc
     */
    public function getStatusName()
    {
        $states = static::states();
        return !empty($states[$this->status]) ? $states[$this->status] : $this->status;
    }
    
    
    /**
     * Варианты сроков изготовления
     * 
     * @return array
     */
    public static function timesToWork(){
        return [
            self::TIME_TO_WORK_ONE_HOUR => '1 час',
            self::TIME_TO_WORK_THREE_HOURS => '3 часа',
            self::TIME_TO_WORK_ONE_DAY => '1 сутки',
        ];
    }
    
    
    /**
     * Название наценки за сроки исполнения
     * 
     * @param int $key
     * @return string
     */
    public static function getTimeToWorkName($key){
        $times_to_work = self::timesToWork();
        return !empty($times_to_work[$key]) ? $times_to_work[$key] : $key;
    }
    
    
    /**
     * Возвращает наценку за срок изготовления
     * 
     * @param int $key
     * @return int
     */
    public static function getTimeToWorkPrice($key){
        $setting = Setting::find()->where(['name' => 'time_to_work_price_' . $key])->select('value')->one();
        return is_object($setting) ? $setting->value : 0;
    }
    
    
    /**
     * Возвращает "позиции заказа" с прикрепленными "товаром"->"категорией товара".
     * Можно использовать $order->goods.
     * 
     * @return Good[]
     */
    public function getGoods(){
        if($this->_goods === null){
            $this->_goods = Good::find()->where(['order_id' => $this->id])->with('item')->all();
        }
        return $this->_goods;
    }
    
    
    /**
     * Возвращает город доставки данного заказа
     * 
     * @return City|null
     */
    public function getCity(){
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }
    
    
    /**
     * Вывод подробного описания заказа построчно в виде массива или строки
     * 
     * @param boolean $to_string Если true, то вывод в строковом формате. По умолчанию массив.
     * @return array|string
     */
    public function report($to_string = false){
        $report = [
            'title' => 'Заказ №' . $this->id,
            'time_to_work' => 'Срок на изготовление: ' . $this->getTimeToWorkName($this->time_to_work),
        ];
        
        if($this->delivery){
            $report['delivery'] = 'Доставка: ' . $this->city->name . ' (' . $this->getDeliveryCost() . ' руб.)';
        }
        else{
            $report['delivery'] = 'Выдача заказа: самовывоз';
        }
        
        if(!is_array($this->goods)){
            return $report;
        }
        $goods_report = array_map(function($good) use ($to_string){
            return $good->report($to_string);
        }, $this->goods);
        $report = array_merge($report, $goods_report);
        $report['total'] = 'Итого позиций: ' . count($this->goods) . ' шт. - '
            . $this->getCost() . ' руб.'
            . ' [С учетом сроков на изготовление и доставки: '
            . $this->getCost(true, true) . ' руб.]';
        
        return $to_string ? implode("\n\n", $report) : $report;
    }
    
    
    /**
     * Возвращает полную стоймость заказа с учетом всех текущих цен
     * 
     * @param boolean $include_time_to_work
     * @param boolean $include_delivery
     * @return int
     */
    public function getCost($include_time_to_work = false, $include_delivery = false){
        $total_cost = 0;
        foreach($this->goods as $good){
            $total_cost += $good->getCost(true, $include_time_to_work, true);
        }
        return $include_delivery ? $total_cost + $this->getDeliveryCost() : $total_cost;
    }
    
    
    /**
     * Возвращает количество позиций в данном заказе
     * 
     * @return type
     */
    public function getGoodsCount(){
        return Good::find()->where(['order_id' => $this->id])->count();
    }
    
    
    /**
     * Возвращает текущий заказ
     * 
     * @return Order
     */
    public static function current(){
        $access_token = self::getToken();
        
        if(!$access_token || !($order = Order::find()->where(['access_token' => $access_token])->status(Order::STATUS_BLANK)->one())){
            $order = new Order();
        }
        return $order;
    }
    
    
    /**
     * Возвращает токен доступа из сессии
     * 
     * @return string
     */
    public static function getToken(){
        return Yii::$app->session->get(Order::SESSION_KEY);
    }
    
    
    public function getDeliveryCost(){
        if(!$this->delivery){
            return 0;
        }
        return $this->getCost() >= $this->city->free_delivery_min_cost ? 0 : $this->city->delivery_price;
    }
    
    
    public function saveCompletely($order_data, $goods_data, $status = self::STATUS_PENDING){
        foreach($this->goods as $good){
            $good->count = $goods_data[$good->id];
            $good->price = $good->getCost(true, true, true);
            $good->save();
        }
        
        
        $this->setAttributes($order_data);
        $this->status = $status;
        $this->save();
        $this->comment = $this->report(true);
        return $this->save();
    }
    
    /*
    public function beforeSave($insert) {
        $this->ip = Yii::$app->request->userIP;
        $this->access_token = Yii::$app->security->generateRandomString(32);
        $this->time = time();
        return true;
    }
     *
     */
}