<?php

namespace app\models;

use Yii;
use app\validators\PhoneMatchValidator;
use app\validators\PhoneFilterValidator;
use yii\web\UploadedFile;
use app\models\Manager;

class EkslibrisForm extends \app\models\Model{
    public $form_title;
    public $name;
    public $phone;
    public $email;
    public $file;
    public $message;
    
    public function rules(){
        return [
            [['name', 'phone', 'form_title', 'email'], 'required'],
            ['phone', PhoneMatchValidator::className()],
            ['phone', PhoneFilterValidator::className()],
            ['email', 'email'],
            [['name', 'message', 'email'], 'trim'],
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, pdf, tiff, gif'],
        ];
    }
    
    public function attributeLabels(){
        return [
            'name' => 'ФИО',
            'phone' => 'Ваш телефон',
            'email' => 'E-mail для согласования макета',
            'file' => 'Прикрепить фото или скан печати или штампа',
            'message' => 'Детали заявки',
            'form_title' => 'Заказ',
        ];
    }
    
    public function action(){
        $this->file = UploadedFile::getInstance($this, 'file');
        if(!$this->validate()){
            return false;
        }
        $files = [];
        if(!empty($this->file)){
            $file = 'uploads/' . $this->file->baseName . '.' . $this->file->extension;
            $this->file->saveAs($file);
            $files[] = ['location' => $file];
        }
        $manager = Manager::getActive();
        $subject = 'Заявка с сайта (' . $this->form_title . ')';
        $manager->sendMail($subject, $this->report(true), $files, true);
        
        $message = 'Ваша заявка отправлена. Наш менеджер свяжется с вами в ближайшее время.';
        Yii::$app->session->setFlash('title', 'Спасибо, что выбрали нашу компанию!');
        Yii::$app->session->setFlash('message', $message);
        return true;
    }
    
    public function report($html = false){
        if($html){
            return Yii::$app->view->renderFile('@app/views/site/report.php', [
                'form_info' => $this->labelsAndValues(),
            ]);
        }
        $form_info = $this->labelsAndValues(' : ', "\n\n");
        return $form_info;
    }
    
    public function formData() {
        $data = $this->toArray();
        unset($data['file']);
        return $data;
    }
}
