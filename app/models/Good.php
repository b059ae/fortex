<?php

namespace app\models;

use app\models\Order;
use yii\easyii\modules\catalog\models\Item;
use app\models\Master;
use app\helpers\CatalogHelper;
use yii\easyii\modules\catalog\api\ItemObject;
use yii\easyii\helpers\Image;


class Good extends \yii\easyii\modules\shopcart\models\Good{
    
    private $_master;
    private $_item;
    private $_tool;
    private $_time_to_work_price;
    private $_uses_tool;
    private $_uses_config;
    
    /**
     * Возвращает конфигурацию позиции - данные, прикрепленные к товару в мастере
     * 
     * @param null|string $key
     * @return array|string|null
     */
    public function getConfig($key = null){
        $config = json_decode($this->config, true);
        if(!is_array($config)){
            return [];
        }
        if($key !== null){
            return !empty($config[$key]) ? $config[$key] : null;
        }
        return $config;
    }
    
    
    /**
     * Проверяет, является ли товар пломбиратором или пломбиром
     * 
     * @return boolean
     */
    public function isPlombirOrPlombirator(){
        if(!is_object($this->master)){
            return false;
        }
        $master_name = $this->master->formName();
        return $master_name == 'PlombiryMaster' || $master_name == 'PlombiratoryMaster';
    }
    
    
    
    /**
     * Запись в поле config
     * 
     * @param array $data
     */
    public function setConfig($data){
        $config = array_merge($this->getConfig(), $data);
        $this->config = json_encode($config);
    }
    
    
    /**
     * Устанавливает взаимоотношение классов "позиции" и "заказа"
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getOrder(){
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
    
    
    /**
     * Связь с инструментом, прикрепленному к клише (оснасткой)
     * 
     * @return yii\easyii\modules\catalog\models\Item
     */
    public function getTool(){
        if($this->_tool == null){
            if($this->tool_id == 0){
                $this->_tool = CatalogHelper::getNoneOfToolItem($this->usesTool());
            }
            else{
                $this->_tool = Item::findOne($this->tool_id);
            }
        }
        return $this->_tool;
    }
    
    
    /**
     * Стоимость за один товар/набор в "позиции"
     * 
     * @param boolean $include_tools
     * @param boolean $include_time_to_work
     * @return int
     */
    public function getPriceForOne($include_tool = false, $include_time_to_work = false, $include_discount = false){
        if(!$this->master){
            $price = $this->item->price;
        }
        else{
            $price = $this->master->priceForOne($this, $include_tool, $include_time_to_work);
        }
        if($include_discount){
            $discount = $this->getConfig('discount');
            $discount = $discount ? $discount : 0;
            $price -= $price / 100 * (int)$discount;
        }
        return $price;
    }
    
    
    /**
     * Возвращает наценку за срочность изготовления
     * 
     * @return int
     */
    public function getTimeToWorkPrice($time_to_work_id = null){
        if($time_to_work_id !== null){
            if($this->usesConfig() || $this->item->category_id == 27){
                return Order::getTimeToWorkPrice($time_to_work_id);
            }
            return 0;
        }
        if($this->_time_to_work_price === null){
            if($this->usesConfig() || $this->item->category_id == 27){
                $this->_time_to_work_price = Order::getTimeToWorkPrice($this->order->time_to_work);
            }
            else{
                $this->_time_to_work_price = 0;
            }
        }
        return $this->_time_to_work_price;
    }
    
    
    /**
     * Общая стоимость всех товаров/наборов в "позиции"
     * 
     * @param boolean $include_tools
     * @param boolean $include_time_to_work
     * @return int
     */
    public function getCost($include_tools = false, $include_time_to_work = false, $include_discount = false){
        return $this->count * $this->getPriceForOne($include_tools, $include_time_to_work, $include_discount);
    }
    
    
    /**
     * Вывод информации о "позиции заказа" в виде массива или строки
     * 
     * @param boolean $to_string Если true, то вывод в строковом формате. По умолчанию массив.
     * @return array|string
     */
    public function report($to_string = false){
        $uses_config = $this->usesConfig();
        $uses_tool = $this->usesTool();
        
        $report['title'] = "\n" . $this->getTitle()
            . ' (' . $this->getPriceForOne(false, false, true) . ' руб.)';
        
        if($uses_config){
            foreach($this->getConfig() as $key => $value){
                /*if(!is_string($value) || !is_array($value) || $key == 'skip'){ // Временная мера
                    continue;
                }*/
                if($key == 'discount'){
                    $report[] = 'Скидка: ' . $value . '%';
                }
                elseif(is_string($value)){
                    $v = $this->master->getAttributeValueLabels($key, $value);
                    if($v == '0'){ // Временная мера
                        continue;
                    }
                    $k = $this->master->getAttributeLabel($key);
                    $report[] = $k . ': ' . $v;
                }
                elseif(is_array($value)){
                    $k = $this->master->getAttributeLabel($key);
                    $_rep = $k . ': ';
                    $_vals = [];
                    
                    foreach($value as $index => $_val){
                        $_vals[] = $this->master->getAttributeValueLabels($key, $_val);
                    }
                    
                    $report[] = $_rep . implode(', ', $_vals);
                }
            }
            
            if($uses_tool){
                $report[] = 'Оснастка: ' . $this->tool->title . ' (' . $this->tool->price . ' руб.)';
            }
        }
        $report['total'] = "\n" . 'Итого: '. $this->count . ' шт. x '
                . $this->getPriceForOne(true, false, true) . ' руб. = '
                . $this->getCost(true, false, true) . ' руб.'
                . ' [С учетом сроков на изготовление: '
                . $this->getCost(true, true, true) . ' руб.]' . "\n";
        
        return $to_string ? implode("\n", $report) : $report;
    }
    
    
    /**
     * Используют ли позиция и тип товара конфигурацию.
     * Проходит ли товар через мастер.
     * 
     * @return boolean
     */
    public function usesConfig(){
        if($this->_uses_config === null){
            $this->_uses_config = is_object($this->master);
        }
        return $this->_uses_config;
    }
    
    
    /**
     * Используют ли позиция и тип товара прикрепляемые инструменты.
     * Например: печати используют оснастки.
     * 
     * @return boolean
     */
    public function usesTool(){
        if($this->_uses_tool === null){
            $this->_uses_tool = is_object($this->master) ? $this->master->usesTool() : false;
        }
        return $this->_uses_tool;
    }
    
    
    public function item(){
        if($this->_item === null){
            $this->_item = new ItemObject($this->item);
        }
        return $this->_item;
    }
    
    
    public function getTitle($include_tool = false){
        if(!$this->master){
            return $this->item->title;
        }
        return $this->master->title($this, $include_tool);
    }
    
    
    public function getMasterUrl(){
        if(!is_object($this->master)){
            return null;
        }
        $master_url_data = $this->master->getUrl($this->item);
        return array_merge($master_url_data, ['good_id' => $this->id]);
    }
    
    
    
    /**
     * Возвращает модель типа товара данной позиции, которая используется в "мастере"
     * 
     * @return mixed
     */
    public function getMaster($load_good = false){
        if($this->_master === null){
            $this->_master = Master::get($this->item);
            if($load_good){
                $this->_master->loadGood($this);
            }
        }
        return $this->_master;
    }
    
    
    public function getToolThumb($width, $height = null){
        if(!$this->usesTool()){
            return $this->item()->thumb($width, $height);
        }
        if($this->tool_id == 0){
            return Image::thumb('..' . $this->tool->image_file, $width, $height);
        }
        return (new ItemObject($this->tool))->thumb($width, $height);
    }
    
}
