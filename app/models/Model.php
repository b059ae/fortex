<?php

namespace app\models;

use Yii;

class Model extends \yii\base\Model{
    
    public function valueLabels(){
        return [];
    }
    
    public function getValueLabels($attribute, $value = null){
        $labels = $this->valueLabels();
        return $value !== null ? $labels[$attribute][$value] : $labels[$attribute];
    }
    
    public function anotherLabels(){
        return [];
    }
    
    public function labelsAndValues($pair_glue = null, $glue = null){
        $labels = array_merge($this->attributeLabels(), $this->anotherLabels());
        $attrs = $this->attributesAndValues();
        $result = [];
        $pair_glue_type = gettype($pair_glue);
        foreach($attrs as $key => $value){
            $new_key = isset($labels[$key]) ? $labels[$key] : $key;
            if($pair_glue_type === 'string'){
                $result[] = $new_key . $pair_glue . $value;
            }
            elseif($pair_glue_type === 'object'){
                $result[] = $pair_glue($new_key, $value);
            }
            else{
                $result[$new_key] = $value;
            }
        }
        if($glue !== null){
            $result = implode($glue, $result);
        }
        return $result;
    }
    
    public function attributesAndValues(){
        $form_data = $this->formData();
        $value_labels = $this->valueLabels();
        $result = [];
        foreach($form_data as $key => $value){
            if(array_key_exists($key, $value_labels)){
                if(is_string($value) && array_key_exists($value, $value_labels[$key])){
                    $value_label = $value_labels[$key][$value];
                }
                elseif(is_array($value)){
                    $v = [];
                    foreach($value as $subvalue){
                        $v[] = $value_labels[$key][$subvalue];
                    }
                    $value_label = implode(', ', $v);
                }
                else{
                    $value_label = $value;
                }
            }
            else{
                $value_label = $value;
            }
            $result[$key] = $value_label;
        }
        return $result;
    }
    
    public function formData(){
        return $this->toArray();
    }
    
    public function sendEmailToManager($subject, $message, $files = []){
        $from_email = Yii::$app->mailerb->transport->getUsername();
        $to_email = Yii::$app->mailer->transport->getUsername();
        $m = Yii::$app->mailerb->compose();
        $m->setFrom([$from_email => Yii::$app->name])
            ->setTo($to_email)
            ->setSubject($subject)
            ->setHtmlBody($message);
        
        foreach($files as $file){
            $m->attach($file);
        }
        return $m->send();
    }
    
    public function sendEmailTo($to_email, $subject, $message, $files = []){
        $from_email = Yii::$app->mailer->transport->getUsername();
        $m = \Yii::$app->mailer->compose();
        $m->setFrom([$from_email => Yii::$app->name])
            ->setTo($to_email)
            ->setSubject($subject)
            ->setHtmlBody($message);
        
        foreach($files as $file){
            $m->attach($file);
        }
        return $m->send();
    }
}
