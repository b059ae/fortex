<?php
namespace app\models\forms;

use Yii;
use app\models\Model;
use app\models\Manager;
use app\validators\PhoneFilterValidator;
use app\validators\PhoneMatchValidator;

class MainContactForm extends Model{
    private $_amocrm;
    
    public $name;
    public $email;
    public $phone;
    public $message;

    
    public function rules(){
        return [
            [['name', 'email', 'phone'], 'required'],
            [['name', 'email'], 'trim'],
            ['name', 'string', 'min' => '4'],
            ['name', 'safe'],
            ['email', 'email'],
            ['phone', PhoneFilterValidator::className()],
            ['phone', PhoneMatchValidator::class],
            ['message', 'safe'],
        ];
    }
    
    public function action(){
        if(!$this->validate()){
            return false;
        }
        $manager = Manager::getActive();
        $subject = $this->formTitle();
        $report = $this->report(true);
        $manager->sendMail($subject, $report, [], true);
        $manager->sendToAmo($subject, $this->attributesAndValues());
        Yii::$app->session->setFlash('title', 'Спасибо, что выбрали нашу компанию!');
        Yii::$app->session->setFlash('message', 'Наш менеджер свяжется в ближайшее время.');
        return true;
    }
    
    public function attributeLabels(){
        return [
            'name' => 'Ваше имя',
            'email' => 'E-mail',
            'phone' => 'Контактный телефон',
            'message' => 'Текст сообщения',
        ];
    }
    
    public function formTitle(){
        return 'Форма обр.связи (подвал). ' . $this->name;
    }
    
    public function setMessage(){
        Yii::$app->session->setFlash('title', 'Заявка отправлена!');
        Yii::$app->session->setFlash('message', 'Наш менеджер свяжется с вами в ближайшее время.');
    }
    
    public function report($html = false){
        if($html){
            return \Yii::$app->view->renderFile('@app/views/site/report.php', [
                'form_info' => $this->labelsAndValues(),
            ]);
        }
        $form_info = $this->labelsAndValues(' : ', "\n\n");
        return $form_info;
    }
}