<?php
namespace app\models\forms;

use yii\base\Model;
use yii\easyii\modules\shopcart\api\Shopcart;
use yii\easyii\modules\shopcart\models\Good;

class CatalogForm extends Model{
    
    public $count = '1';
    public $good_id;
    public $item_id;
    
    
    public function rules(){
        return [
            [['count', 'item_id'], 'required'],
            
            [['count', 'good_id', 'item_id'], 'integer'],
        ];
    }
    
    
    public function attributeLabels(){
        return [
            'count' => 'Количество',
        ];
    }
    
    
    
    
    public function save(){
        if(empty($this->item_id)){
            return false;
        }
        
        $config = $this->toArray();
        unset($config['item_id'], $config['good_id'], $config['count']);
        $config = array_filter($config);
        //var_dump($config);exit();
        
        $order_info = Shopcart::add($this->item_id, $this->count);
        //$good = Good::findOne($order_info['good_id']);
        //$good->config = json_encode($config);
        //$good->save();
        $this->good_id = $order_info['good_id'];
        
        return $this->good_id;
    }
}