<?php
namespace app\models\forms;

use Yii;
use yii\base\Model;
use app\validators\PhoneFilterValidator;
use app\validators\PhoneMatchValidator;
use app\helpers\AmoCrmHelper;
use app\helpers\CatalogHelper;
use yii\easyii\models\Setting;
use app\models\Manager;

/**
 * Форма обратного звонка
 */
class CallbackForm extends Model{
    
    private $_amocrm;
    
    public $phone;

    
    public function rules(){
        return [
            ['phone', 'required'],
            ['phone', 'filter', 'filter' => function($value){return preg_replace('/[^+\d]/', '', $value);}],
            ['phone', PhoneMatchValidator::class],
        ];
    }

    
    public function attributeLabels(){
        return [
            'phone' => 'Контактный телефон'
        ];
    }

    
    public function action(){
        if(!$this->validate()){
            return false;
        }
        $working_time_key = CatalogHelper::isWorkingTime();
        $title = 'Запрос на звонок принят!';
        $message = 'Благодарим за заявку! Мы позвоним вам в рабочее время.';
        if((int)$working_time_key === 1){
            Yii::$app->beeline->call($this->phone);
            $message = 'В течение нескольких секунд установится соединение с менеджером...'
                . Yii::$app->view->renderFile('@app/views/layouts/chunks/counter.php', [
                    'seconds' => 8,
                ]);
        }
        
        $manager = Manager::getActive();
        $subject = 'Обратный звонок';
        $manager->sendMail($subject, Yii::$app->view->renderFile(
            '@app/views/site/email/callback.php',
            [
                'subject' => $subject,
                'datetime' => date('d.m.Y H:i'),
                'phone' => $this->phone,
            ]
        ), [], true);
        
        Yii::$app->session->setFlash('title', $title);
        Yii::$app->session->setFlash('message', $message);
        return true;
    }
    
    
    public function getAmocrm(){
        if($this->_amocrm === null){
            $form_title = $this->formTitle();
            $lead_data = ['message' => $form_title];
            $contact_data = $this->toArray(['phone']);
            
            $this->_amocrm = new AmoCrmHelper(
                $form_title,
                $form_title,
                $lead_data,
                $contact_data
            );
        }
        return $this->_amocrm;
    }
    
    
    public function formTitle(){
        return 'Обратный звонок: ' . $this->phone;
    }
}