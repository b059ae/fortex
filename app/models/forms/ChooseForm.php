<?php
namespace app\models\forms;

use Yii;
use yii\base\Model;
use app\validators\PhoneMatchValidator;
use app\helpers\AmoCrmHelper;
use app\models\Manager;

class ChooseForm extends Model{
    
    private $_amocrm;
    public $phone;
    public $a;

    
    public function rules(){
        return [
            [['phone', 'a'], 'required'],
            ['phone', PhoneMatchValidator::class],
            ['a', 'string', 'min' => 1],
        ];
    }

    
    public function attributeLabels(){
        return [
            'phone' => 'Контактный телефон'
        ];
    }

    
    public function action(){
        if(!$this->validate()){
            return false;
        }
        $manager = Manager::getActive();
        $subject = 'Продолжение заказа';
        $manager->sendMail($subject, Yii::$app->view->renderFile(
            '@app/views/site/email/callback.php',
            [
                'subject' => $subject,
                'datetime' => date('d.m.Y H:i'),
                'phone' => $this->phone,
            ]
        ), [], true);
        return true;
    }
    
    
    public function getAmocrm(){
        if($this->_amocrm === null){
            $form_title = $this->formTitle();
            $lead_data = ['message' => $form_title];
            $contact_data = $this->toArray(['phone']);
            
            $this->_amocrm = new AmoCrmHelper(
                $form_title,
                $form_title,
                $lead_data,
                $contact_data
            );
        }
        return $this->_amocrm;
    }
    
    
    public function formTitle(){
        return 'Обратный звонок: ' . $this->phone;
    }
}