<?php
namespace app\models\forms;

use Yii;
use yii\base\Model;
use app\validators\PhoneMatchValidator;
use app\helpers\AmoCrmHelper;
use yii\easyii\models\Setting;


class OfferContactForm extends Model{
    private $_amocrm;
    
    public $name;
    public $phone;
    public $offer;

    public function rules(){
        return [
            [['phone', 'offer'], 'required'],
            [['name', 'offer'], 'trim'],
            [['name', 'offer'], 'string', 'min' => '4'],
            [['name', 'offer'], 'safe'],
            ['phone', 'filter', 'filter' => function($value){return preg_replace('/[^+\d]/', '', $value);}],
            ['phone', PhoneMatchValidator::class],
        ];
    }
    
    public function attributeLabels(){
        return [
            'name' => 'Ваше имя',
            'phone' => 'Контактный телефон',
        ];
    }
    
    public function action(){
        if(!$this->validate()){
            return false;
        }
        $this->amocrm->send();
        $this->sendEmail();
        $callback_form = new CallbackForm(['phone' => $this->phone]);
        $callback_form->call();
        Yii::$app->session->setFlash('title', 'Благодарим вас!');
        Yii::$app->session->setFlash('message', 'Наш менеджер скоро с вами свяжется.');
        return true;
    }
    
    public function getAmocrm(){
        if($this->_amocrm === null){
            $form_title = $this->formTitle();
            $lead_data = ['message' => $this->offer];
            $contact_data = $this->toArray(['phone']);
            
            $this->_amocrm = new AmoCrmHelper(
                $form_title,
                $this->name,
                $lead_data,
                $contact_data
            );
        }
        return $this->_amocrm;
    }
    
    public function formTitle(){
        return 'Форма обр.связи (оффер, всплывашка). ' . $this->name;
    }
    
    public function sendEmail(){
        $to      = Setting::get('admin_email');
        $subject = $this->formTitle();
        $message = 'По запросу: ' . $this->offer
        . "\n\n" . 'Имя покупателя: ' . $this->name
        . "\n\n" . 'Телефон: ' . $this->phone;
        
        $headers = 'Content-type: text/html; charset=UTF-8' . "\r\n";
        $headers .= 'From: ' . Setting::get('robot_email');
        return mail($to, $subject, $message, $headers);
    }
}