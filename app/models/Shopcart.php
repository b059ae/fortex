<?php

namespace app\models;

use Yii;
use app\validators\PhoneMatchValidator;
use app\validators\PhoneFilterValidator;
use app\validators\InnValidator;
use yii\easyii\models\Setting;
use app\helpers\CatalogHelper;
use yii\easyii\modules\shopcart\models\Order;
use yii\helpers\ArrayHelper;
use app\models\Manager;
use app\helpers\Product;
use yii\web\UploadedFile;

class Shopcart extends \app\models\Model{
    
    private $_order_info;
    
    public $count = 1;
    public $order_type;
    public $phone;
    public $email;
    public $inn;
    public $name;
    public $ogrnip;
    public $country;
    public $address;
    public $delivery;
    public $delivery_address;
    public $urgency;
    public $agree;
    public $company;
    public $file;
    public $config_center_text;
    public $config_info;
    public $config_font;
    public $config_staff;
    public $config_size;
    public $config_color;
    public $config_frame;
    public $config_brand;
    public $config_engraving;
    public $config_protection;
    
    
    
    
    
    public function rules(){
        //$cliche_subcat = $this->order_info['pecati_good']->item->cat->slug;
        //$inn_name_req = $cliche_subcat !== 'doctor' && $cliche_subcat !== 'extraordinary';
        return [
            [['phone', 'email', 'agree', 'order_type'], 'required'],
            ['phone', PhoneMatchValidator::className()],
            ['phone', PhoneFilterValidator::className()],
            ['email', 'email'],
            ['ogrnip', 'match', 'pattern' => '/^[0-9]{12,15}$/'],
            [['agree', 'config_engraving'], 'boolean'],
            [['count', 'delivery', 'urgency'], 'integer'],
            
            ['agree', 'compare', 'compareValue' => true,
                'message' => 'Необходимо подтвердить соглашение'],
            
            [[
                'country', 'address', 'delivery_address', 'config_center_text',
                'config_font', 'config_info', 'company',
                'name', 'config_staff', 'config_size', 'config_frame', 'config_brand', 'config_color',
            ], 'trim'],
            [[
                'country', 'address', 'delivery_address', 'config_center_text',
                'config_font', 'config_info', 'company',
                'name', 'config_staff', 'config_size', 'config_frame', 'config_brand', 'config_color',
            ], 'safe'],
            
            ['config_protection', 'each', 'rule' => ['integer']],
            
            /*[['inn', 'name'], 'required', 'when' => function($model) use ($inn_name_req){
                return $inn_name_req;
            }, 'whenClient' => 'function(a, v){return '.($inn_name_req ? 'true' : 'false').';}'],
            */
            
            ['inn', InnValidator::className(), 'skipOnEmpty' => true],
            
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, pdf, tiff, gif'],
        ];
    }

    public function attributeLabels(){
        return [
            'phone' => 'Тел.',
            'email' => 'E-mail',
            'inn' => 'ИНН',
            'name' => 'ФИО',
            'ogrnip' => 'ОГРНИП',
            'country' => 'Страна',
            'address' => 'Город, адрес',
            'count' => 'Количество',
            'delivery' => 'Доставка',
            'delivery_address' => 'Адрес доставки',
            'urgency' => 'Срочность заказа',
            'agree' => 'Согласен на обработку персональных данных',
            'company' => 'Название организации',
            'file' => 'Файл',
            'config_protection' => 'Защита от подделки',
            'order_type' => 'Тип заказа',
            'config_center_text' => 'Текст по центру',
            'config_info' => 'Доп. информация',
            'config_font' => 'Шрифт',
            'config_staff' => 'Материал',
            'config_size' => 'Размер',
            'config_color' => 'Цвет',
            'config_frame' => 'С рамкой',
            'config_brand' => 'Производитель',
            'config_engraving' => 'С гравировкой',
        ];
    }
    
    public function valueLabels(){
        return [
            'order_type' => [
                '1' => 'Оплата онлайн',
                '2' => 'Получить счет',
                '3' => 'Оплата при получении',
            ],
            'urgency' => [
                '1' => '1 рабочий день',
                '2' => '6 часов (+' . Setting::get('urgency_6_hours') . ' руб.)',
                '3' => '1 час (+' . Setting::get('urgency_1_hour') . ' руб.)',
            ],
            'delivery' => [
                '1' => 'Самовывоз',
                '2' => 'Доставка водитель (' . Setting::get('delivery_price') . ' руб.)',
            ],
            'config_protection' => [
                '1' => 'Микрошрифт',
                '2' => 'Растр',
            ],
            'agree' => [
                '1' => 'Да',
            ],
            'config_engraving' => [
                '1' => 'Да',
                '0' => 'Нет',
            ],
        ];
    }
    
    public function anotherLabels(){
        return [
            'order_id' => 'Номер заказа',
            'ready_date' => 'Дата изготовления',
            'product_title' => 'Заказ',
            'cost_info' => 'Сумма заказа',
            'price_for_one_info' => 'За шт.',
        ];
    }
    
    public function formName(){
        return '';
    }
    
    public function saveOrder($status = Order::STATUS_PENDING){
        $order_info =& $this->order_info;
        $order = $order_info->order->model;
        $tool_good = $order_info->getToolInfo();
        $cliche_good = $order_info->getClicheInfo();
        
        if($tool_good){
            $tool_good = $order_info->getToolInfo()->model;
            $cliche_good = $order_info->getClicheInfo()->model;
            $tool_good->count = $this->count;
            $tool_good->save();
            $cliche_good->count = $this->count;
            $cliche_good->save();
        }
        else{
            $order_info->good->model->count = $this->count;
            $order_info->good->model->save();
        }
        
        $form_data = $this->formData();
        $form_data['inventory'] = $this->inventory();
        $order->status = $status;
        $order->save();
        $order->config = json_encode($form_data);
        $order->comment = 'Номер заказа: ' . $order_info->order->id . "\n\n" . $this->report();
        return $order->save();
    }
    
    public function action(){
        if(!$this->validate()){
            return false;
        }
        $files = [];
        $manager_files = [];
        $this->file = UploadedFile::getInstance($this, 'file');
        if(!empty($this->file)){
            $file = $this->getFilename();
            $this->file->saveAs($file);
            $manager_files[] = ['location' => $file];
        }
        if((int)$this->order_type === 1){
            $this->saveOrder(Order::STATUS_BLANK);
            return $this->payment();
        }
        elseif((int)$this->order_type === 2){
            $this->saveOrder();
            $invoice = $this->generateInvoice();
            $files[] = ['location' => $invoice];
            $manager_files[] = ['location' => $invoice];
            $message = 'Номер вашего заказа: ' . $this->order_info->order->id.
                '.<br> На ваш e-mail отправлено сопроводительное письмо и счёт для оплаты заказа.';
        }
        elseif((int)$this->order_type === 3){
            $this->saveOrder();
            $message = 'Номер вашего заказа: ' . $this->order_info->order->id.
                '.<br> На ваш e-mail отправлено сопроводительное письмо.';
        }
        
        $subject = "Заказ с сайта печатей №{$this->order_info->order->id} ({$this->getValueLabels('order_type', $this->order_type)})";
        $report = $this->report();
        $form_data = $this->formData();
        $manager = Manager::getActive();
        $manager->sendMail($subject, $report, $manager_files);
        $manager->sendToAmo($subject, $this->attributesAndValues());
        if(!empty($this->email)){
            $manager->sendMailTo($this->email, $form_data, $files);
        }
        Yii::$app->session->setFlash('title', 'Спасибо, что выбрали нашу компанию!');
        Yii::$app->session->setFlash('message', $message);
        return Yii::$app->response->redirect(['/success']);
    }
    
    
    
    
    public function getCost(){
        $result = $this->getPriceForOne() * $this->count;
        return $result + $this->getDeliveryPrice();
    }
    
    public function getPriceForOne(){
        $item_price = $this->order_info->getPrice(['config_size' => $this->config_size]);
        $result = 0;
        $good = $this->order_info->getGood();
        if((int)$this->urgency === 2){
            $result += Setting::get('urgency_6_hours');
        }
        elseif((int)$this->urgency === 3){
            $result += Setting::get('urgency_1_hour');
        }
        if(is_object($good)){
            $item = $good->item;
            if($this->config_engraving){
                $result += isset($item->data->engravingprice) ? $item->data->engravingprice : 0;
            }
            if($item->slug === 'plombiry'){
                $item_price = Product::getPlombiryPrice($this->count);
            }
            elseif($item->slug === 'flazhok'){
                $item_price = Product::getFlazhokPrice($this->count, $this->config_staff);
            }
            elseif($item->slug === 'shtok'){
                $item_price = Product::getShtokPrice($this->count);
            }
        }
        return $result + $item_price;
    }
    
    
    public function report($html = false){
        if($html){
            return \Yii::$app->view->renderFile('@app/views/site/report.php', [
                'form_info' => $this->labelsAndValues(),
            ]);
        }
        $form_info = $this->labelsAndValues(' : ', "\n\n");
        return $form_info;
    }
    
    
    public function getOrder_info(){
        if($this->_order_info === null){
            $this->_order_info = CatalogHelper::getOrderInfo();
        }
        return $this->_order_info;
    }
    
    public function generateInvoice(){
        $site_data = Setting::findAll(['name' => [
            'inn', 'kpp', 'company', 'bank',
            'bik', 'ogrn', 'checking_account', 'correspondent_account',
            'address', 'phone', 'director',
        ]]);
        
        $file_to_download = Yii::getAlias('@runtime') . '/downloads/order-' . $this->order_info->order->id . '.pdf';
        Yii::$app->html2pdf
            ->render('invoice', [
                'product_title' => $this->order_info->getFullTitle(),
                'order_info' => $this->order_info,
                'customer_data' => $this->formData(),
                'site_data' => ArrayHelper::map($site_data, 'name', 'value'),
                'nds' => 0,
                'delivery' => $this->delivery,
                'delivery_price' => $this->getDeliveryPrice(),
                'cost' => $this->getCost(),
                'price_for_one' => $this->getPriceForOne(),
                'count' => $this->count,
            ])
            ->saveAs($file_to_download);
        
        return $file_to_download;
    }
    
    public function payment(){
        $transaction_id = Yii::$app->security->generateRandomString(32);
        $order = $this->order_info->order->model;
        $order->transaction_id = $transaction_id;
        $order->save();
        $order_name = $this->order_info->getFullTitle();
        $inventory = $this->inventory();
        
        $params = [
            'MNT_ID' => (int)Setting::get('mnt_id'),
            'MNT_TRANSACTION_ID' => $transaction_id,
            'MNT_AMOUNT' => $this->getCost(),
            'MNT_DESCRIPTION' => Setting::get('company') . ' : ' . $order_name,
            'MNT_CURRENCY_CODE' => 'RUB',
            'paymentSystem.unitId' => 2243990,
            'MNT_CUSTOM1' => '1',
            'MNT_CUSTOM2' => json_encode($inventory),
        ];
        
        if(!empty($this->email)){
            $params['MNT_CUSTOM3'] = $this->email;
        }
        
        $url = 'https://www.payanyway.ru/assistant.htm';
        return Yii::$app->response->redirect($url . '?' . http_build_query($params));
    }
    
    public function formData(){
        return array_filter($this->getAttributes(null, ['file']) + [
            'order_id' => $this->order_info->order->id,
            'product_title' => $this->order_info->getFullTitle(),
            'ready_date' => $this->readyDate(),
            'cost_info' => $this->getCost() . ' руб.',
            'price_for_one_info' => $this->getPriceForOne() . ' руб.',
            'file' => $this->getFilename(),
        ]);
    }
    
    public function stampySizeNotice(){
        return 'Выберите ближайший и удобный для вас размер штампа.
            Если в данном списке вы не находите нужного размера,
            подберите себе другую оснастку в разделе
            <a href="/catalog/stampy-tools?a=buy">"образцы оснастки"</a>.';
    }
    
    public function readyDate(){
        switch((int)$this->urgency){
            case 1 : return date('d-m-Y H:i', time() + 60 * 60 * 24);
                break;
            case 2 : return date('d-m-Y H:i', time() + 60 * 60 * 6);
                break;
            case 3 : return date('d-m-Y H:i', time() + 60 * 60 * 1);
                break;
        }
        return date('d-m-Y H:i', time() + 60 * 60 * 24);
    }
    
    public function inventory(){
        $product_info = [
            "items" =>
            [
                [
                    "n" => $this->order_info->getFullTitle(),
                    "p" => $this->getPriceForOne() * $this->count,
                    "q" => $this->count,
                    "t" => 1104,
                ],
            ],
        ];
        if((int)$this->delivery === 2){
            $product_info['items'][] = [
                'n' => 'Доставка',
                'p' => Setting::get('delivery_price'),
                'q' => '1',
                't' => 1104,
            ];
        }
        return $product_info;
    }
    
    
    public function getDeliveryPrice(){
        $total = $this->getPriceForOne() * $this->count;
        $delivery_price = Setting::get('delivery_price');
        $mpfd = Setting::get('min_price_free_delivery');
        if((int)$this->delivery === 2 && $total < $mpfd){
            return $delivery_price;
        }
        return 0;
    }
    
    
    public function getFilename(){
        if(!empty($this->file)){
            return 'uploads/' . $this->file->baseName . '.' . $this->file->extension;
        }
        return null;
    }
}
