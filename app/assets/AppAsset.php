<?php
namespace app\assets;

class AppAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@app/media';
    public $css = [
        'https://fonts.googleapis.com/css?family=Hind:300,400,500,600,700',
        'chosen-js/chosen.min.css',
        'swiper/css/swiper.min.css',
        'css/fonts.css',
        'css/theme.css',
        'css/master.css',
    ];
    public $js = [
        'wowjs/wow.min.js',
        'chosen-js/chosen.jquery.min.js',
        'swiper/js/swiper.min.js',
        'js/functions.js',
        'js/scripts.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
