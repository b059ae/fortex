<?php

use yii\db\Migration;

/**
 * Class m171203_120154_create_customers
 */
class m171203_120154_create_customers extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('customers', [
            'id' => $this->primaryKey(),
            'phone' => $this->string()->comment('Телефон'),
            'email' => $this->string()->comment('Email'),
            'name' => $this->string()->comment('ФИО'),
            'auth_key' => $this->string(32)->notNull()->comment('Токен авторизации'),
            'password_hash' => $this->string()->notNull()->comment('Пароль'),
            'password_reset_token' => $this->string()->unique()->comment('Токен для сброса пароля'),
            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата обновления'),
        ],'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('customers');
    }
}
