<?php

use yii\db\Migration;

/**
 * Class m171224_152201_add_transaction_id_to_order
 */
class m171224_152201_add_transaction_id_to_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(
            'easyii_shopcart_orders',
            'transaction_id',
            $this->string(40)->null()->comment('Идентификатор платежа')
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('easyii_shopcart_orders', 'transaction_id');
    }
}
