<?php

use yii\db\Migration;

/**
 * Class m171213_144800_add_columns_to_goods
 */
class m171213_144800_add_columns_to_goods extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(
            'easyii_shopcart_goods',
            'config',
            $this->text()->comment('Конфигурация (данные, внесенные в мастере)')
        );
        
        $this->addColumn(
            'easyii_shopcart_goods',
            'tool_id',
            $this->integer()->comment('ID оснастки')
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('easyii_shopcart_goods', 'config');
        $this->dropColumn('easyii_shopcart_goods', 'tool_id');
    }
}
