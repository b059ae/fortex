<?php

use yii\db\Migration;

/**
 * Class m171213_142448_create_cities
 */
class m171213_142448_create_cities extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('cities', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название города'),
            'delivery_price' => $this->integer()->defaultValue(0)->comment('Стоимость доставки в этот город'),
            'free_delivery_min_cost' => $this->integer()->defaultValue(0)
                ->comment('Минимальная цена заказа, при которой доставка бесплатная'),
        ],'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('cities');
    }

}
