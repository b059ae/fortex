<?php

use yii\db\Migration;

/**
 * Class m171213_143558_add_columns_to_orders
 */
class m171213_143558_add_columns_to_orders extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(
            'easyii_shopcart_orders',
            'time_to_work',
            $this->integer(1)->comment('Идентификатор срока изготовления (от 0 до 2)')
        );
        
        $this->addColumn(
            'easyii_shopcart_orders',
            'delivery',
            $this->integer(1)->comment('Включение доставки (0 - нет, 1 - да)')
        );
        
        $this->addColumn(
            'easyii_shopcart_orders',
            'city_id',
            $this->integer()->comment('ID города')
        );
        
        $this->addColumn(
            'easyii_shopcart_orders',
            'customer_id',
            $this->integer()->comment('ID пользователя')
        );
        
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('easyii_shopcart_orders', 'time_to_work');
        $this->dropColumn('easyii_shopcart_orders', 'delivery');
        $this->dropColumn('easyii_shopcart_orders', 'city_id');
        $this->dropColumn('easyii_shopcart_orders', 'customer_id');
    }
}
