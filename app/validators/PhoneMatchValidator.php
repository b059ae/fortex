<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 28.06.17
 * Time: 14:07
 */

namespace app\validators;


use yii\validators\RegularExpressionValidator;

class PhoneMatchValidator extends RegularExpressionValidator
{

    public $pattern = '/[0-9]+$/s';
    public $message = '{attribute} необходимо вводить в формате +7 (900) 333-2211';
}