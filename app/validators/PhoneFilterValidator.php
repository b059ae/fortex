<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 28.06.17
 * Time: 14:07
 */

namespace app\validators;


use yii\validators\FilterValidator;

class PhoneFilterValidator extends FilterValidator
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->filter = function ($v) {
            return preg_replace('/[^+\d]/', '', $v);
        };
        parent::init();
    }
}