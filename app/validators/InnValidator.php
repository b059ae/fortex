<?php
namespace app\validators;

use yii\validators\Validator;

class InnValidator extends Validator{
    
    public function init(){
        parent::init();
        $this->message = 'Неправильный ИНН.';
    }

    
    public function validateAttribute($model, $attribute){
        $value = $model->$attribute;
        if (!$this->validateInn($value)) {
            $model->addError($attribute, $this->message);
        }
    }

    
    public function clientValidateAttribute($model, $attribute, $view){
        $message = json_encode($this->message, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        return <<<JS

var validateInn = function(inputNumber){
    //преобразуем в строку
    inputNumber = "" + inputNumber;
    //преобразуем в массив
    inputNumber = inputNumber.split('');
    //для ИНН в 10 знаков
    if((inputNumber.length == 10) && (inputNumber[9] == ((2 * inputNumber[  0] + 4 * inputNumber[1] + 10 * inputNumber[2] + 3 * inputNumber[3] + 5 * inputNumber[4] + 9 * inputNumber[5] + 4 * inputNumber[6] + 6 * inputNumber[7] + 8 * inputNumber[8]) % 11) % 10)){
        return true;
    //для ИНН в 12 знаков
    }else if((inputNumber.length == 12) && ((inputNumber[10] == ((7 * inputNumber[ 0] + 2 * inputNumber[1] + 4 * inputNumber[2] + 10 * inputNumber[3] + 3 * inputNumber[4] + 5 * inputNumber[5] + 9 * inputNumber[6] + 4 * inputNumber[7] + 6 * inputNumber[8] + 8 * inputNumber[9]) % 11) % 10) && (inputNumber[11] == ((3 * inputNumber[ 0] + 7 * inputNumber[1] + 2 * inputNumber[2] + 4 * inputNumber[3] + 10 * inputNumber[4] + 3 * inputNumber[5] + 5 * inputNumber[6] + 9 * inputNumber[7] + 4 * inputNumber[8] + 6 * inputNumber[9] + 8 * inputNumber[10]) % 11) % 10))){
        return true;
    }else{
        return false;
    }
}         

if (!validateInn(value)) {
    messages.push($message);
    return false;
}
else{
    return true;
}


JS;
    }
    
    
    /**
    * Функция проверяет правильность инн
    *
    * @param string $inn
    * @return bool
    */
    protected function validateInn($inn) {
        $digits_count = strlen($inn);
        if ($digits_count == 10) {
            //для юр лица код 10 знаков
            if (preg_match('#([\d]{10})#', $inn, $m)) {
                $inn = $m[0];
                $code10 = (($inn[0] * 2 + $inn[1] * 4 + $inn[2] *10 + $inn[3] * 3 +
                            $inn[4] * 5 + $inn[5] * 9 + $inn[6] * 4 + $inn[7] * 6 +
                            $inn[8] * 8) % 11 ) % 10;
                if ($code10 == $inn[9]){
                    return $inn;
                }
            }
        } else {
            //для физ.лиц и ИП - 12 знаков
            if (preg_match('#([\d]{12})#', $inn, $m)) {
                $inn = $m[0];
                $code11 = (($inn[0] * 7 + $inn[1] * 2 + $inn[2] * 4 + $inn[3] *10 +
                            $inn[4] * 3 + $inn[5] * 5 + $inn[6] * 9 + $inn[7] * 4 +
                            $inn[8] * 6 + $inn[9] * 8) % 11 ) % 10;
                $code12 = (($inn[0] * 3 + $inn[1] * 7 + $inn[2] * 2 + $inn[3] * 4 +
                            $inn[4] *10 + $inn[5] * 3 + $inn[6] * 5 + $inn[7] * 9 +
                            $inn[8] * 4 + $inn[9] * 6 + $inn[10]* 8) % 11 ) % 10;

                if ($code11 == $inn[10] && $code12 == $inn[11]) return $inn;
            }
        }
        return false;
    }
}